#!/usr/bin/python

"""
ZetCode PyQt5 tutorial 

In this example, we create a simple
window in PyQt5.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""

import sys, time
from PyQt5           import QtCore, QtGui, QtWidgets
from PyQt5.QtCore    import QSize, QTime, QTimer, QThread
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QCheckBox, QPushButton, QAction, QLineEdit, QMessageBox, QLCDNumber

#from PyQt5 import *
#from PyQt5.QtGui import *
#from PyQt5.QtCore import *
#from PyQt5.QtWidgets import *


class TimerThread(QThread):

    def __init__(self, window):
        QThread.__init__(self)
        self.window = window

    def __del__(self):
        self.wait()

    def run(self):
        while True:    
            time.sleep(1)
            # TODO Here you should just signal the clocks not updating directly
            self.window.updateTimers(timers)
            self.window.showUpdatedTimers(timers)
            #self.window.show()
            
        




class MainWindow(QtWidgets.QMainWindow):

    # here in the init you should pass an object for the config
    # some part of it will be read, some other will be written
    def __init__(self):

        leftAlign = 20
        
        super(MainWindow, self).__init__()
        self.setGeometry( 100, 100, 1000, 500)
        self.setWindowTitle("The Walking Dead (SuperBotPlayer)")        
        self.setWindowIcon(QtGui.QIcon("wdicon.png"))

        self.faction_supporter_check = QCheckBox("Faction Supporter",self)
        self.faction_supporter_check.stateChanged.connect(self.clickBox)
        self.faction_supporter_check.move(leftAlign,20)
        self.faction_supporter_check.resize(320,40)

        self.take_screenshot_check = QCheckBox("Take Screenshot",self)
        self.take_screenshot_check.stateChanged.connect(self.clickBox)
        self.take_screenshot_check.move(leftAlign,60)
        self.take_screenshot_check.resize(320,40)

        self.send_screenshot_check = QCheckBox("Send Screenshot",self)
        self.send_screenshot_check.stateChanged.connect(self.clickBox)
        self.send_screenshot_check.move(leftAlign,100)
        self.send_screenshot_check.resize(320,40)

        self.incremental_check = QCheckBox("Incremental",self)
        self.incremental_check.stateChanged.connect(self.clickBox)
        self.incremental_check.move(leftAlign,140)
        self.incremental_check.resize(320,40)

        self.start_button = QPushButton('Start', self)
        self.start_button.clicked.connect(self.clickButton)
        self.start_button.setToolTip('Start')
        self.start_button.move(leftAlign,190)

        self.pause_button = QPushButton('Pause', self)
        self.pause_button.setToolTip('Pause')
        self.pause_button.clicked.connect(self.clickButton)
        self.pause_button.move(120,190)

        self.stop_button = QPushButton('Stop', self)
        self.stop_button.setToolTip('Stop')
        self.stop_button.clicked.connect(self.clickButton)
        self.stop_button.move(220,190)

        # All energy sections
        labelEnergyWidth = 240
        currRow          = 230
        energyColumns    = [150, 210, 270, 325, 370, 410, 440, 445, 500]
        textBoxWidth     = 30
        textBoxHeight    = 30

        # Label for energy name
        self.worldEnergylabel = QLabel("WorldEnergy", self)
        self.worldEnergylabel.move(leftAlign, currRow)
        self.worldEnergylabel.resize(120, 30)

        # Current energy values
        energyColumnsCnt = 0
        self.worldEnergyValueLabel = QLabel("78/78", self)
        self.worldEnergyValueLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergyValueLabel.resize(120, 30)

        # Current energy time
        energyColumnsCnt = 1
        self.worldEnergyTimeLabel = QLabel("05:35", self)
        self.worldEnergyTimeLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergyTimeLabel.resize(120, 30)

        # Simple label
        energyColumnsCnt = 2
        self.worldEnergylabel = QLabel("energy", self)
        self.worldEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergylabel.resize(120, 30)

        # Text input for energy
        energyColumnsCnt = 3
        self.worldEnergyTextbox = QLineEdit(self)
        self.worldEnergyTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergyTextbox.resize(textBoxWidth, textBoxHeight)
        self.worldEnergyTextbox.setText(str(78))
        
        energyColumnsCnt = 4
        self.worldEnergylabel = QLabel("time", self)
        self.worldEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergylabel.resize(120, 30)

        # Text imput for time minutes
        energyColumnsCnt = 5
        self.worldEnergyTimerMinTextbox = QLineEdit(self)
        self.worldEnergyTimerMinTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergyTimerMinTextbox.resize(textBoxWidth, textBoxHeight)
        self.worldEnergyTimerMinTextbox.setText("05")

        # Label for timer
        energyColumnsCnt = 6
        self.worldEnergylabel2 = QLabel(":", self)
        self.worldEnergylabel2.move(energyColumns[energyColumnsCnt], currRow)        

        # Text imput for time seconds
        energyColumnsCnt = 7
        self.worldEnergyTimerSecTextbox = QLineEdit(self)
        self.worldEnergyTimerSecTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.worldEnergyTimerSecTextbox.resize(textBoxWidth, textBoxHeight)
        self.worldEnergyTimerSecTextbox.setText("12")
  
        # Update values button
        energyColumnsCnt = 8
        self.worldEnergyUpdatebutton = QPushButton('Update', self)
        self.worldEnergyUpdatebutton.setToolTip('Update world energy and time values')
        self.worldEnergyUpdatebutton.clicked.connect(self.clickButton)
        self.worldEnergyUpdatebutton.move(energyColumns[energyColumnsCnt], currRow)
        
        # Label for energy name
        currRow = currRow + 40
        self.raidEnergylabel = QLabel("RaidEnergy", self)
        self.raidEnergylabel.move(leftAlign, currRow)
        self.raidEnergylabel.resize(120, 30)

        # Current energy values
        energyColumnsCnt = 0
        self.raidEnergyValueLabel = QLabel("6/6", self)
        self.raidEnergyValueLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergyValueLabel.resize(120, 30)

        # Current energy time
        energyColumnsCnt = 1
        self.raidEnergyTimeLabel = QLabel("45:00", self)
        self.raidEnergyTimeLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergyTimeLabel.resize(120, 30)

        # Simple label
        energyColumnsCnt = 2
        self.raidEnergylabel = QLabel("energy", self)
        self.raidEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergylabel.resize(120, 30)

        # Text input for energy
        energyColumnsCnt = 3
        self.raidEnergyTextbox = QLineEdit(self)
        self.raidEnergyTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergyTextbox.resize(textBoxWidth, textBoxHeight)
        self.raidEnergyTextbox.setText(str(6))
        
        energyColumnsCnt = 4
        self.raidEnergylabel = QLabel("time", self)
        self.raidEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergylabel.resize(120, 30)

        # Text imput for time minutes
        energyColumnsCnt = 5
        self.raidEnergyTimerMinTextbox = QLineEdit(self)
        self.raidEnergyTimerMinTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergyTimerMinTextbox.resize(textBoxWidth, textBoxHeight)
        self.raidEnergyTimerMinTextbox.setText("45")

        # Label for timer
        energyColumnsCnt = 6
        self.raidEnergylabel2 = QLabel(":", self)
        self.raidEnergylabel2.move(energyColumns[energyColumnsCnt], currRow)        

        # Text imput for time seconds
        energyColumnsCnt = 7
        self.raidEnergyTimerSecTextbox = QLineEdit(self)
        self.raidEnergyTimerSecTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.raidEnergyTimerSecTextbox.resize(textBoxWidth, textBoxHeight)
        self.raidEnergyTimerSecTextbox.setText("00")
  
        # Update values button
        energyColumnsCnt = 8
        self.raidEnergyUpdatebutton = QPushButton('Update', self)
        self.raidEnergyUpdatebutton.setToolTip('Update raid energy and time values')
        self.raidEnergyUpdatebutton.clicked.connect(self.clickButton)
        self.raidEnergyUpdatebutton.move(energyColumns[energyColumnsCnt], currRow)

        # Label for energy name
        currRow = currRow + 40
        self.territoryEnergylabel = QLabel("TerritoryEnergy", self)
        self.territoryEnergylabel.move(leftAlign, currRow)
        self.territoryEnergylabel.resize(120, 30)

        # Current energy values
        energyColumnsCnt = 0
        self.territoryEnergyValueLabel = QLabel("10/10", self)
        self.territoryEnergyValueLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergyValueLabel.resize(120, 30)

        # Current energy time
        energyColumnsCnt = 1
        self.territoryEnergyTimeLabel = QLabel("60:00", self)
        self.territoryEnergyTimeLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergyTimeLabel.resize(120, 30)

        # Simple label
        energyColumnsCnt = 2
        self.territoryEnergylabel = QLabel("energy", self)
        self.territoryEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergylabel.resize(120, 30)

        # Text input for energy
        energyColumnsCnt = 3
        self.territoryEnergyTextbox = QLineEdit(self)
        self.territoryEnergyTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergyTextbox.resize(textBoxWidth, textBoxHeight)
        self.territoryEnergyTextbox.setText("10")
        
        energyColumnsCnt = 4
        self.territoryEnergylabel = QLabel("time", self)
        self.territoryEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergylabel.resize(120, 30)

        # Text imput for time minutes
        energyColumnsCnt = 5
        self.territoryEnergyTimerMinTextbox = QLineEdit(self)
        self.territoryEnergyTimerMinTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergyTimerMinTextbox.resize(textBoxWidth, textBoxHeight)
        self.territoryEnergyTimerMinTextbox.setText("60")

        # Label for timer
        energyColumnsCnt = 6
        self.territoryEnergylabel2 = QLabel(":", self)
        self.territoryEnergylabel2.move(energyColumns[energyColumnsCnt], currRow)        

        # Text imput for time seconds
        energyColumnsCnt = 7
        self.territoryEnergyTimerSecTextbox = QLineEdit(self)
        self.territoryEnergyTimerSecTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.territoryEnergyTimerSecTextbox.resize(textBoxWidth, textBoxHeight)
        self.territoryEnergyTimerSecTextbox.setText("00")
  
        # Update values button
        energyColumnsCnt = 8
        self.territoryEnergyUpdatebutton = QPushButton('Update', self)
        self.territoryEnergyUpdatebutton.setToolTip('Update territory energy and time values')
        self.territoryEnergyUpdatebutton.clicked.connect(self.clickButton)
        self.territoryEnergyUpdatebutton.move(energyColumns[energyColumnsCnt], currRow)

        # Label for energy name
        currRow = currRow + 40
        self.survivalRdEnergylabel = QLabel("SurvivalEnergy", self)
        self.survivalRdEnergylabel.move(leftAlign, currRow)
        self.survivalRdEnergylabel.resize(120, 30)

        # Current energy values
        energyColumnsCnt = 0
        self.survivalRdEnergyValueLabel = QLabel("10/10", self)
        self.survivalRdEnergyValueLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergyValueLabel.resize(120, 30)

        # Current energy time
        energyColumnsCnt = 1
        self.survivalRdEnergyTimeLabel = QLabel("15:00", self)
        self.survivalRdEnergyTimeLabel.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergyTimeLabel.resize(120, 30)

        # Simple label
        energyColumnsCnt = 2
        self.survivalRdEnergylabel = QLabel("energy", self)
        self.survivalRdEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergylabel.resize(120, 30)

        # Text input for energy
        energyColumnsCnt = 3
        self.survivalRdEnergyValueTextbox = QLineEdit(self)
        self.survivalRdEnergyValueTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergyValueTextbox.resize(textBoxWidth, textBoxHeight)
        self.survivalRdEnergyValueTextbox.setText("10")
        
        energyColumnsCnt = 4
        self.survivalRdEnergylabel = QLabel("time", self)
        self.survivalRdEnergylabel.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergylabel.resize(120, 30)

        # Text imput for time minutes
        energyColumnsCnt = 5
        self.survivalRdEnergyTimerMinTextbox = QLineEdit(self)
        self.survivalRdEnergyTimerMinTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergyTimerMinTextbox.resize(textBoxWidth, textBoxHeight)
        self.survivalRdEnergyTimerMinTextbox.setText("60")

        # Label for timer
        energyColumnsCnt = 6
        self.survivalRdEnergylabel2 = QLabel(":", self)
        self.survivalRdEnergylabel2.move(energyColumns[energyColumnsCnt], currRow)        

        # Text imput for time seconds
        energyColumnsCnt = 7
        self.survivalRdEnergyTimerSecTextbox = QLineEdit(self)
        self.survivalRdEnergyTimerSecTextbox.move(energyColumns[energyColumnsCnt], currRow)
        self.survivalRdEnergyTimerSecTextbox.resize(textBoxWidth, textBoxHeight)
        self.survivalRdEnergyTimerSecTextbox.setText("00")
  
        # Update values button
        energyColumnsCnt = 8
        self.survivalRdEnergyUpdatebutton = QPushButton('Update', self)
        self.survivalRdEnergyUpdatebutton.setToolTip('Update survivalRd energy and time values')
        self.survivalRdEnergyUpdatebutton.clicked.connect(self.survivalRdEnergyUpdateHandler)
        self.survivalRdEnergyUpdatebutton.move(energyColumns[energyColumnsCnt], currRow)

        self.timerThread = TimerThread(self)
        self.timerThread.start()
                
    def clickBox(self, state):
      
        if state == QtCore.Qt.Checked:
            print('Checked')
        else:
            print('Unchecked')

    def setTimers(self, timers):
        self.timers = timers

    def survivalRdEnergyUpdateHandler(self):

        # Setting the energy in case it is populated
        energy = self.survivalRdEnergyValueTextbox.text().strip()
        if energy != "":            
            self.survivalRdEnergyValueLabel.setText(energy + "/10")

        # Setting the time in case it is populated
        minutes = self.survivalRdEnergyTimerMinTextbox.text().strip()
        seconds = self.survivalRdEnergyTimerSecTextbox.text().strip()
        if minutes != "":
            self.timers["survival"][0] = int(minutes)
        if seconds != "":           
            self.timers["survival"][1] = int(seconds)            

    def clickButton(self):
        stringValues = "Raid Energy: " + self.raidEnergyTextbox.text() + "\n"
        stringValues +=  "World Energy: " + self.worldEnergyTextbox.text() + "\n"
        stringValues += "Survival Energy: " + self.survivalEnergyTextbox.text() + "\n"
        stringValues += "Territory Energy: " + self.territoryEnergyTextbox.text() + "\n"
        QMessageBox.question(self, 'Message - pythonspot.com', stringValues, QMessageBox.Ok, QMessageBox.Ok)
        print('Button pressed')

    def showUpdatedTimers(self, timers):
        self.worldEnergyTimeLabel.setText(     str(timers["world"][0]).zfill(2) +       ":" + str(timers["world"][1]).zfill(2))
        self.raidEnergyTimeLabel.setText(      str(timers["raid"][0]).zfill(2) +        ":" + str(timers["raid"][1]).zfill(2))
        self.territoryEnergyTimeLabel.setText( str(timers["territory"][0]).zfill(2) +   ":" + str(timers["territory"][1]).zfill(2))
        self.survivalRdEnergyTimeLabel.setText(str(timers["survival"][0]).zfill(2) +    ":" + str(timers["survival"][1]).zfill(2) )
        
    def updateTimers(self, timers):
        timers["world"][1] -= 1
        timers["raid"][1] -= 1
        timers["territory"][1] -= 1
        timers["survival"][1] -= 1
        
if __name__ == '__main__':

    timers = { "world": [], "raid": [], "territory": [], "survival": []}
    timers["world"].append(12)
    timers["world"].append(12)
    timers["raid"].append(39)
    timers["raid"].append(39)
    timers["territory"].append(40)
    timers["territory"].append(40)
    timers["survival"].append(55)
    timers["survival"].append(55)
    
    app = QApplication(sys.argv)
    window = MainWindow()
    window.setTimers(timers)
    window.show()
       
    # To check current platform running on there are several ways
    # https://stackoverflow.com/questions/1325581/how-do-i-check-if-im-running-on-windows-in-python
    if sys.platform.startswith("linux"):
        sys.exit(app.exec_())





