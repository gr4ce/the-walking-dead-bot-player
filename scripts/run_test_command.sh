#python -m unittest test.activity_test.ActivityWorldAreaTest
#python -m unittest test.activity_scheduler_test
#python3.7 -m xmlrunner test.activity_scheduler_test --output-file test-result.xml
python3.8 -m xmlrunner test.activity_scheduler_test --output test-reports
python3.8 -m xmlrunner test.image_recon_test --output test-reports
