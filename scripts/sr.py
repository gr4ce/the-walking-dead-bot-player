import os
import sys
import time
import thread
import threading
from multiprocessing.synchronize import Event


INFIGHT_AUTOPLAY        = [60,   1000]
ENDFIGHT_NEXT           = [1000, 1000]
ENDFIGHT_CLAIM_REWARDS  = [1050, 945 ]
IGNORE_RAIDED           = [1840, 80  ]
ROASTER_FULL            = [1438, 300 ]
# Presses "Fight Anyway" button if roster or weapon invertory is full (note that the 2 buttons are in different positions but they overlap)
FIGHT_ANYWAY            = [720,  790 ]
FIGHT                   = [1500, 1000]
GAME_ICON               = [670,  192 ]
GAME_ICON_TABLET        = [60,   110 ]
SWITCH_ACCOUNT          = [1200, 730 ]
CONNECT_WITH_FB         = [750,  900 ]
CONNECT_WITH_FB_CONFIRM = [950,  900 ]
TRANSFER_COMPLETE       = [950,  700 ]
GO_TO_WOODBURY          = [400,  760 ]
AREA_13_POSITION        = [1100, 450 ]
BATTLE_BUTTON           = [100,  1000]
WORLD_BUTTON            = [300,  1000]
QUIT_GAME               = [750,  830 ]
TEAM_SELECT             = [1250, 900 ]
CLOSE_AD                = [1880, 40  ]
CLOSE_OFFER             = [1520, 125 ]

ROAD_TO_SURVIVAL        = [   70,  560]
RTS_TOURNAMENT          = [ 1400,  350]
RTS_MAP                 = [ 1000, 1000]
RTS_MAP_GO_BACKWARD     = [  800,   50]
RTS_MAP_GO_FORWARD      = [ 1150,   50]
RTS_CLOSE_MAP_MENU      = [ 1500,  230]

RTS_SWIPE_BACK_START    = [  100,  600]
RTS_SWIPE_BACK_STOP     = [ 1700,  600]

RTS_SWIPE_MAP_14_START  = [ 1700,  600]
RTS_SWIPE_MAP_14_STOP   = [  100,  600]
RTS_MAP_14              = [ 1650,  500]
RTS_MAP_3               = [ 1050,  460]
RTS_MAP_25              = [ 1700,  500]
RTS_ATTACK              = [ 1300,  700]
RTS_AUTOPLAY            = [   60, 1000]
NETWORK_ERROR           = [  960,  800]
ENDFIGHT_NEXT           = [ 1000, 1000]

DEVICE_PWR_BUTTON  = "26"

RTS_ENERGY_CAP        = 8
RECHARGE_TIME_MINUTES = 15
RECHARGE_TIME_SECONDS = 0
ACTIVE                = 1
SLEEPING              = 0
START_PLAY_AT         = 8
MATCH_TO_WAIT         = 2
TIME_TO_REFILL_ONE_MATCH = 60 * 15

def log_message(comp, message):
    print(message)

class RTSEnergy:
    def __init__(self, curr_refils, minutes, seconds):
        self.name           = "Road To Survival Energy"
        self.max_energy     = RTS_ENERGY_CAP
        self.minutes_energy = RECHARGE_TIME_MINUTES
        self.seconds_energy = RECHARGE_TIME_SECONDS
        self.curr_energy    = curr_refils
        self.minutes        = minutes
        self.seconds        = seconds
        self.timer_status   = ACTIVE
        self.critical       = threading.Lock()

        self.world_energy_full = Event()

        self.world_energy_increased = Event()
        self.world_energy_increased.clear()

    def energy_left(self):
        return self.curr_energy

    def increment_energy(self):
        self.critical.acquire()

        if self.timer_status == ACTIVE and self.curr_energy < RTS_ENERGY_CAP:
            self.minutes = self.minutes_energy
            self.seconds = self.seconds_energy
            self.curr_energy += 1
            message = "Energy incremented by 1, left " + str(self.energy_left())
            log_message("World Energy", message)

        elif self.timer_status == SLEEPING and self.curr_energy == RTS_ENERGY_CAP:
            message = "WARNING: Requested increment energy when time_status = SLEEPING"
            log_message("World Energy", message)

        else:
            message = "FATAL: Something about the status is fundamentally wrong"
            log_message("World Energy", message)

        self.world_energy_increased.set()
        self.critical.release()

    def get_timer_value(self):
        return [self.minutes, self.seconds]

    def set_timer_value(self, minutes, seconds):
        self.critical.acquire()
        self.minutes = minutes
        self.seconds = seconds
        self.critical.release()

    def sync_wait(self, minutes, seconds):

        #sys.stdout.write("\nWorld Energy: ")
        #os.system('setterm -cursor off')

        self.critical.acquire()

        self.minutes = minutes
        self.seconds = seconds

        while not (self.minutes == 0 and self.seconds == 0):
            #sys.stdout.write("%02d:%02d" % (self.minutes, self.seconds))
            #sys.stdout.flush()
            if self.seconds == 0:
                self.seconds = 59
                self.minutes -= 1
            else:
                self.seconds -= 1
            self.critical.release()
            time.sleep(1)
            self.critical.acquire()
            #sys.stdout.write("\b\b\b\b\b")

        #sys.stdout.write("%02d:%02d" % (self.minutes, self.seconds))
        sys.stdout.flush()
        self.critical.release()
        time.sleep(1)
        #sys.stdout.write("\n")
        #os.system('setterm -cursor on')


    def set_energy(self, energy):

        self.critical.acquire()

        # A few checks is better to do
        if energy > RTS_ENERGY_CAP:
            raise Exception("energy value " + str(energy) + " is greater than RTS_ENERGY_CAP = " + RTS_ENERGY_CAP)
        if energy < 0:
            raise Exception("energy value " + str(energy) + " can't be less than 0")

        # We are increasing the energy so better notify who is waiting for this
        if self.curr_energy < energy:
            self.world_energy_increased.set()

        # We set the value
        self.curr_energy = energy

        # If the timer is sleeping we need to wake it up
        if self.timer_status == SLEEPING:
            self.timer_status == ACTIVE
            self.world_energy_full.set()

        self.critical.release()


    def use_refill(self):

        self.critical.acquire()

        self.minutes = RECHARGE_TIME_MINUTES
        self.seconds = RECHARGE_TIME_SECONDS
        self.curr_energy = RTS_ENERGY_CAP

        if self.timer_status == ACTIVE:
            self.timer_status = SLEEPING
            self.world_energy_increased.set()

        self.critical.release()

    def use_energy(self, energy):
        self.critical.acquire()
        self.curr_energy -= energy
        message = "Energy used " + str(energy) + " left " + str(self.energy_left())
        log_message("World Energy", message)
        self.critical.release()

# Creating an instance of the WorldEnergy object that will be used as global reference
rtsEnergy = RTSEnergy(0, 0, 0)

STATUS_ACTIVE   = 1
STATUS_SLEEPING = 0

def rts_energy_handler(we):
    log_message("RTSEnergyHandler", "RTS Energy handler is starting")

    handler_staus = STATUS_ACTIVE
    if we.curr_energy > 0:
            we.world_energy_increased.set()
    while True:

        # TODO: This should be moved in a while condition in the player code
        if we.curr_energy >= START_PLAY_AT:
            we.world_energy_increased.set()

        if we.curr_energy == RTS_ENERGY_CAP:
            we.timer_status = SLEEPING
            log_message("RTSEnergyHandler", "status = SLEEPING")
            we.world_energy_full.clear()

            # Wait until someone start a match and exactly when it presses fight button
            log_message("RTSEnergyHandler", "Max energy cap, can't refill anymore")
            we.world_energy_full.wait()
            log_message("RTSEnergyHandler", "Someone's playing, start refilling")
            we.timer_status = ACTIVE
        else:
            we.timer_status = ACTIVE
            log_message("RTSEnergyHandler", "status = ACTIVE")
            we.sync_wait(we.minutes_energy, we.seconds_energy)

            if we.timer_status == ACTIVE:
                energy_before = we.energy_left()
                we.increment_energy()
                energy_after  = we.energy_left()
                log_message("RTSEnergyHandler", "Energy refilled (" + str(energy_before) + " -> " + str(energy_after) + ")")

        we.minutes_energy = RECHARGE_TIME_MINUTES
        we.seconds_energy = RECHARGE_TIME_SECONDS

    log_message("WorldEnergyHandler", "Job done here I'm going to die")


ASYNC = 1

def press_button(button, async=0):
    command = "adb shell input tap " + str(button[0]) + " " + str(button[1]) 
    if async == ASYNC:
        command += " &"
    os.system(command)
    if async != ASYNC:
        time.sleep(2)

def swipe(start, stop, speed=200):
    command = "adb shell input swipe %d %d %d %d %d" % (start[0], start[1], stop[0], stop[1], speed) 
    os.system(command)
    time.sleep(2)

def go_to_road_to_survival():
    print("Going to road to survival")
    press_button(ROAD_TO_SURVIVAL)

def go_to_rts_tournament():
    print("Going to road to survival tournament")
    press_button(RTS_TOURNAMENT)

def go_to_rts_map():
    print("Going to road to survival map")
    press_button(RTS_MAP)

def close_map_menu():
    print("Close map menu")
    press_button(RTS_CLOSE_MAP_MENU)

def go_to_map_1():
    print("Going back to stage 1")
    close_map_menu()
    press_button(RTS_MAP_GO_BACKWARD)
    close_map_menu()
    press_button(RTS_MAP_GO_BACKWARD)
    close_map_menu()
    press_button(RTS_MAP_GO_BACKWARD)

def go_to_map_2():
    print("Going to stage 2")
    close_map_menu()
    press_button(RTS_MAP_GO_FORWARD)
    close_map_menu()

def go_to_map_3():
    print("Going to stage 3")
    close_map_menu()
    press_button(RTS_MAP_GO_FORWARD)
    close_map_menu()

def go_to_begin():
    close_map_menu()
    swipe(RTS_SWIPE_BACK_START, RTS_SWIPE_BACK_STOP)
    swipe(RTS_SWIPE_BACK_START, RTS_SWIPE_BACK_STOP)
    swipe(RTS_SWIPE_BACK_START, RTS_SWIPE_BACK_STOP)

def go_to_stage_14():
    # Gives 390 base points
    swipe(RTS_SWIPE_MAP_14_START, RTS_SWIPE_MAP_14_STOP, 400)
    swipe(RTS_SWIPE_MAP_14_START, RTS_SWIPE_MAP_14_STOP, 400)
    swipe(RTS_SWIPE_MAP_14_START, RTS_SWIPE_MAP_14_STOP, 400)

def play_map_14():
    print("Playing map 14")
    press_button(RTS_MAP_14)

def play_map_3():
    print("Playing map 3")
    press_button(RTS_MAP_3)

def play_map_25():
    print("Playing map 25")
    press_button(RTS_MAP_25)

def attack():
    print("Attacking now!")
    press_button(RTS_ATTACK)

def autoplay():
    print("Pressing autoplay")
    press_button(RTS_AUTOPLAY)

def next():
    print("Pressing next")
    press_button(ENDFIGHT_NEXT)

def actively_play(minutes, seconds):

    while not (minutes == 0 and seconds == 0):
        if seconds == 0:
            seconds = 59
            print("%d minute(s) remaining" % minutes)
            minutes -= 1            
        else:
            seconds -= 1
        time.sleep(1)
        press_button(NETWORK_ERROR, ASYNC)

def turn_screen_off():
    command = "adb shell input keyevent " + DEVICE_PWR_BUTTON
    os.system(command)
    log_message("Player", "Pressing Power button")
    time.sleep(2)

def turn_screen_on():
    command = "adb shell input keyevent " + DEVICE_PWR_BUTTON
    os.system(command)
    log_message("Player", "Pressing Power button")
    time.sleep(2)

def unlock():
    swipe([90, 400], [1000, 400], 100)

def wait_for_refill(matches, waittime):
    #time.sleep(10)
    total_refill_time = TIME_TO_REFILL_ONE_MATCH * MATCH_TO_WAIT
    time_spent_playing = waittime * matches
    log_message("Player", "Now waiting for: " + str(total_refill_time - time_spent_playing) + " secs"  )
    time.sleep(total_refill_time - time_spent_playing)

def main():
    #go_to_road_to_survival()
    #go_to_rts_tournament()
    #go_to_rts_map()
    #go_to_map_1()
    #go_to_map_2()
    #go_to_map_3()
    #go_to_begin()
    #go_to_stage_14()

    thread.start_new_thread(rts_energy_handler, (rtsEnergy,))
    intial_match_count = 1
    match_count = intial_match_count
    match_played = 0
    play_min = 1
    play_sec = 30
    turn_screen_on()
    unlock()
    time.sleep(3)
    press_button(CLOSE_OFFER)
    press_button(IGNORE_RAIDED)
    press_button(CLOSE_OFFER)

    while True:
        while match_count > 0:
            play_map_25()
            attack()
            autoplay()
            actively_play(play_min, play_sec)
            next()
            match_played += 1 
            match_count -= 1
        turn_screen_off()
        wait_for_refill(match_played, 60 * play_min + play_sec)
        match_played = 0
        match_count = MATCH_TO_WAIT
        turn_screen_on()
        unlock()
        time.sleep(3)
        press_button(CLOSE_OFFER)
        press_button(IGNORE_RAIDED)
        press_button(CLOSE_OFFER)

if __name__ == "__main__":
    main()


