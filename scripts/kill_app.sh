#!/bin/bash

RUNNING_PYTHON_INSTANCES=$(ps aux|grep -e "python"|grep -e "play"|grep  -v "grep"|awk -F" " '{print $2}'| sed 's/\n//g')

if [ -z "$RUNNING_PYTHON_INSTANCES" ];
then
	echo "No python instances of the application found"
else
        echo "Found instance of application... terminating"
        echo $RUNNING_PYTHON_INSTANCES
        kill -9 $RUNNING_PYTHON_INSTANCES
fi

RUNNING_BASH_INSTANCES=$(ps aux|grep "bash scripts"|grep -v "grep"|awk -F" " '{print $2}'| sed 's/\n//g')

if [ -z "$RUNNING_BASH_INSTANCES" ];
then
	echo "No bash instances of the application found"
else
	echo "Found instances of application... terminating"
        echo $RUNNING_BASH_INSTANCES
        kill -9 $RUNNING_BASH_INSTANCES
fi
