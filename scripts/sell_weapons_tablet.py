import os
import sys
import time

verbosity = "HIGH"

tablet = True

def touch_pure(x, y):
    os.system('adb shell input tap %d %d &' % (x,y))
    
def select_all():
    delay = 0.2
    y = 300
    x = 1100

    for i in range(0,4):
        time.sleep(delay)
        touch_pure(x, y)
        x -= 250

    time.sleep(delay + 0.2)
    
if __name__ == "__main__":

    max_food = 2178000
    common_weapons = 0
    uncommon_weapons = 0
    cost_per_common_weapon = 250
    cost_per_uncommon_weapon = 870

    if sys.argv[1] == "1":
        max_weapons = max_food / cost_per_common_weapon
        weapon_type = "common"
    elif sys.argv[1] == "2":
        max_weapons = max_food / cost_per_uncommon_weapon
        weapon_type = "uncommon"
    else:
        sys.stderr.write("ERROR: please enter 1 for common, 2 for uncommon weapons\n")
        sys.stderr.flush()
        sys.exit(1)

    sys.stdout.write("Will sell %d %s weapons " % (max_weapons, weapon_type))
    sys.stdout.write("doing %d iterations\n" % (max_weapons/5))
    sys.stdout.flush()

    sold = 0
    for n in range(0, max_weapons/5):
        # Touch 5 items
        select_all()
        sys.stdout.write("(n = %d) " % (n))
        sys.stdout.flush()

        common_weapons   += (250*5)
        uncommon_weapons += (870*5)
        
        sys.stdout.write("If you sell now you'll get %d from common weapons or %d from uncommon weapons\n" % (common_weapons, uncommon_weapons))
        sys.stdout.flush()

        #pos1 = convert([1750, 500])
        #pos2 = convert([100,  500])
        #command = "adb shell input swipe %d %d %d %d 1200" % (pos1[0], pos1[1], pos2[0], pos2[1])
        command = "adb shell input swipe 1130 370 210 370 400"
        if verbosity == "HIGH":
            sys.stdout.write(command + "\n")
            sys.stdout.flush()
        os.system(command)
        n += 4
        sold += 5

        if sold >= 200:
            sold = 0

            command = "adb shell input tap 780 570"
            os.system(command)
            time.sleep(1)
            command = "adb shell input tap 500 500"
            os.system(command)


