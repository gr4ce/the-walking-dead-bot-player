#!/usr/bin/python
import os
import cv2
import time
import numpy
import random
import imutils
import argparse
import scipy.misc
from scipy import ndimage
from PIL import Image
import matplotlib.pyplot as plt


class Rectangle():
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def randomise(self):
        x = random.randrange(self.x,  self.x + self.width)
        y = random.randrange(self.y,  self.y + self.height)
        return [x, y]

    def to_string(self):
        return str(self.x) + "," + str(self.y) + "," + str(self.width) + "," + str(self.height)


parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument('--img',
                    dest='img_path',
                    default=None,
                    help='The image to be read')

parser.add_argument('--template',
                    dest='template_path',
                    default=None,
                    help='The image to be found in')

parser.add_argument('--proc',
                    dest='process',
                    default='resize',
                    choices=['resize', 'rotate'],
                    help='What kind of process should be done on the image')

parser.add_argument('--threshold',
                    dest='threshold_value',
                    default=0.9,
                    help='The value to be used in the image recognition process')


def rotate_screenshot(file_name):

    if os.path.isfile(file_name):
        img = scipy.misc.imread(file_name)
    else:
        exit()

    rotate_img = ndimage.rotate(img, 90)
    scipy.misc.imsave(file_name + "-processed.png", rotate_img)
    # plt.imshow(rotate_img)
    # plt.show()


def resize_screenshot(file_name):

    if os.path.isfile(file_name):
        img = Image.open(file_name)
    else:
        exit()

    size = 1024, 600
    img.thumbnail(size, Image.ANTIALIAS)
    img.save(file_name + "-processed.png", "png")


def show_screenshot(file_name):

    if os.path.isfile(file_name):
        img = scipy.misc.imread(file_name)
    else:
        exit()

    plt.imshow(img)
    plt.show()


def are_similar(rect1, rect2):

    diff = 2
    val1 = rect1.x
    val2 = rect2.x
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.y
    val2 = rect2.y
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.width
    val2 = rect2.width
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.height
    val2 = rect2.height
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    return True


def detect_image_simple1(original, to_detect, threshold):

    img_rgb = cv2.imread(original)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    template = cv2.imread(to_detect, 0)
    w, h = template.shape[::-1]
    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    loc = numpy.where(res >= threshold)

    rectangle_list = []
    for pt in zip(*loc[::-1]):
        print("Found a match!")
        cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
        new_rect = Rectangle(pt[1], template.shape[1],
                             pt[0], template.shape[0])
        found_similar = False
        for r in rectangle_list:
            print(" comparing " + new_rect.to_string() + " and " +
                  r.to_string() + " are similar, not adding")
            if are_similar(r, new_rect):
                print(" == > " + new_rect.to_string() + " and " +
                      r.to_string() + " are similar, not adding")
                found_similar = True
                break

        if found_similar == False:
            print(" Adding " + new_rect.to_string())
            rectangle_list.append(new_rect)
        print("")

    print("actual size is " + str(len(rectangle_list)))

    cv2.imwrite('res.png', img_rgb)

    return rectangle_list


def detect_image_simple(original, to_detect, method):

    img_rgb = cv2.imread(original)
    #cv2.imshow("original", img_rgb)
    # cv2.waitKey(0)
    #img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    img_gray = cv2.imread(original, 0)
    #cv2.imshow("original gray", img_gray)
    # cv2.waitKey(0)
    template_gray = cv2.imread(to_detect, 0)
    w, h = template_gray.shape[::-1]
    print("Original template size is (" + str(w) + "," + str(h) + ")")

    rectangle_list = []
    # Resizing the original image
    img_gray_resized = img_gray

    if (img_gray_resized.shape[0] <= template_gray.shape[0]) or (img_gray_resized.shape[1] <= template_gray.shape[1]):
        cv2.imshow("resized ", img_gray_resized)
        cv2.waitKey(0)

    # Do the template match
    results = cv2.matchTemplate(img_gray, template_gray, eval(method))

    # Set the threshold value and use it as a filter
    threshold = args.threshold_value
    located = numpy.where(results >= threshold)

    #
    name = "image " + \
        " (" + str(img_gray_resized.shape[0]) + \
           "," + str(img_gray_resized.shape[1]) + ")"
    cv2.imshow(name, img_gray_resized)
    cv2.waitKey(0)
    # time.sleep(2)
    rectangle_list = []
    for pt in zip(*located[::-1]):
        print("   Match found!!")
        cv2.rectangle(img_gray_resized, pt, (
            pt[0] + template_gray.shape[0], pt[1] + template_gray.shape[1]), (0, 0, 255), 2)
        r = Rectangle(pt[1], template_gray.shape[1],
                      pt[0], template_gray.shape[0])
        rectangle_list.append(r)

    if len(rectangle_list) > 0:
        cv2.imwrite("result_attempt" + str(size - w + 1) + ".png", img_rgb)
        show_screenshot("result_attempt" + str(size - w + 1) + ".png")

    cv2.imshow("resized ", img_gray_resized)
    cv2.waitKey(0)

    return rectangle_list


def detect_image(source_path, template_path, method, threshold):

    print("Using " + method)

    source_img = None
    source_img_copy = None
    template_img = None
    w = None
    h = None

    use_grayscale = False

    if use_grayscale == False:
        source_img = cv2.imread(source_path)
        source_img_copy = source_img.copy()
        template_img = cv2.imread(template_path)
        w, h, _ = template_img.shape[::-1]
    else:
        source_img = cv2.imread(source_path, 0)
        source_img_copy = source_img.copy()
        template_img = cv2.imread(template_path, 0)
        w, h = template_img.shape[::-1]

    img = source_img_copy.copy()
    method = eval(method)

    # Apply template Matching
    res = cv2.matchTemplate(img, template_img, method)
    loc = numpy.where(res >= threshold)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)

    rects = []
    for pt in zip(*loc[::-1]):
        rects.append(pt, (pt[0] + w, pt[1] + h))
        cv2.rectangle(source_img, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)

    plt.subplot(121), plt.imshow(res, cmap='gray')
    plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(cv2.cvtColor(
        cv2.imread(source_path), cv2.COLOR_BGR2RGB))
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(meth)

    plt.show()

    return rects


def detect_image_resized(original, template):

    # Load original and convert it in grayscale
    original_rgb = cv2.imread(original)
    original_gray = cv2.cvtColor(original_rgb, cv2.COLOR_BGR2GRAY)


if __name__ == "__main__":

    args = parser.parse_args()

    if args.img_path is None:
        print("ERROR: please use an image file as source argument")
        exit()

    if args.template_path is None:
        print("ERROR: please use an image file as template argument")
        exit()

    print("Threshold: " + str(args.threshold_value))
    # resize_screenshot(args.img_path)

    methods = ['cv2.TM_CCOEFF',
               'cv2.TM_CCOEFF_NORMED',
               'cv2.TM_CCORR',
               'cv2.TM_CCORR_NORMED',
               'cv2.TM_SQDIFF',
               'cv2.TM_SQDIFF_NORMED']

    # for meth in methods:
    #    r_list = detect_image(args.img_path, args.template_path, meth, args.threshold_value)
    #    for rect in r_list:
    #        print rect.to_string()

    # for meth in methods:
    #    r_list = detect_image_simple(args.img_path, args.template_path, meth)
    #    for rect in r_list:
    #        print rect.to_string()

    r_list = detect_image_simple1(
        args.img_path, args.template_path, float(args.threshold_value))
    for rect in r_list:
        print(rect.to_string())
