import os
import sys
import time



touch_event_id = 1

tablet = True
verbosity = "LOW"
REFERENCE_X=1920.0
REFERENCE_Y=1080.0 
TABLET_X=1024.0
TABLET_Y=600.0

EV_ABS             = 0x0003
EV_SYN             = 0x0000
ABS_MT_POSITION_X  = 0x0035
ABS_MT_POSITION_Y  = 0x0036
#ABS_MT_PRESSURE    = 0x003a
ABS_MT_PRESSURE    = 0x0032
ABS_MT_TOUCH_MAJOR = 0x0030
SYN_REPORT         = 0x0000
ABS_MT_TRACKING_ID = 0x0039

if tablet:
   event_dev = "/dev/input/event0"
else:
   event_dev = "/dev/input/event1"


#0003 0039 00002588
#0003 0030 0000000c
#0003 0032 0000000c
#0003 0035 0000021d
#0003 0036 000002d7
#0000 0000 00000000
#0003 0030 00000008
#0003 0032 00000008
#0003 0036 000002d6
#0000 0000 00000000
#0003 0039 ffffffff
#0000 0000 00000000

def transform(button):
    button_transformed = [ TABLET_Y - button[1], button[0]]
    return button_transformed

def convert(button):
    if tablet:
        button_converted = [ int((button[0] * TABLET_X) / REFERENCE_X), int((button[1] * TABLET_Y) / REFERENCE_Y) ]
        return button_converted
    return button

def touch_2d(button):
    button_converted = transform(convert(button))
    touch(button_converted[0], button_converted[1])

def touch(x, y):

    global touch_event_id

    event_string =  "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_TRACKING_ID, touch_event_id)
 
    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_TOUCH_MAJOR, 10) 
    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_PRESSURE,    10)
    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_POSITION_Y,  y)
    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_POSITION_X,  x)

#    event_string += "sendevent /dev/input/event1 %d %d %d\n" % (EV_ABS, ABS_MT_POSITION_X,  y)
#    event_string += "sendevent /dev/input/event1 %d %d %d\n" % (EV_ABS, ABS_MT_POSITION_Y,  x)
#    event_string += "sendevent /dev/input/event1 %d %d %d\n" % (EV_ABS, ABS_MT_PRESSURE,    5)
#    event_string += "sendevent /dev/input/event1 %d %d %d\n" % (EV_ABS, ABS_MT_TOUCH_MAJOR, 5)

    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_SYN, SYN_REPORT,         0)

    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_ABS, ABS_MT_TRACKING_ID, -1)
    event_string += "sendevent %s %d %d %d\n" % (event_dev, EV_SYN, SYN_REPORT,         0)
    
    touch_event_id+=1
    
    os.system('adb shell "%s" &' % event_string)
    
def select_all():
    delay = 0.5
    y = 800
    x = 1800

    for i in range(0,5):
        time.sleep(delay)
        touch_2d([x, y])
        x -= 320
    
if __name__ == "__main__":

    max_food = 2178000
    common_weapons = 0
    uncommon_weapons = 0
    cost_per_common_weapon = 250
    cost_per_uncommon_weapon = 870

    if sys.argv[1] == "1":
        max_weapons = max_food / cost_per_common_weapon
        weapon_type = "common"
    elif sys.argv[1] == "2":
        max_weapons = max_food / cost_per_uncommon_weapon
        weapon_type = "uncommon"
    else:
        sys.stderr.write("ERROR: please enter 1 for common, 2 for uncommon weapons\n")
        sys.stderr.flush()
        sys.exit(1)

    sys.stdout.write("Will sell %d %s weapons " % (max_weapons, weapon_type))
    sys.stdout.write("doing %d iterations\n" % (max_weapons/5))
    sys.stdout.flush()

    sold = 0
    for n in range(0, max_weapons/5):
        # Touch 5 items
        select_all()
        sys.stdout.write("(n = %d) " % (n))
        sys.stdout.flush()

        common_weapons   += (250*5)
        uncommon_weapons += (870*5)
        
        sys.stdout.write("If you sell now you'll get %d from common weapons or %d from uncommon weapons\n" % (common_weapons, uncommon_weapons))
        sys.stdout.flush()

        pos1 = convert([1750, 500])
        pos2 = convert([100,  500])
        command = "adb shell input swipe %d %d %d %d 1200" % (pos1[0], pos1[1], pos2[0], pos2[1])
        if verbosity == "HIGH":
            sys.stdout.write(command + "\n")
            sys.stdout.flush()
        os.system(command)
        n += 4
        sold += 5

        if sold >= 200:
            sold = 0

            command = "adb shell input tap 780 570"
            os.system(command)
            time.sleep(1)
            command = "adb shell input tap 500 500"
            os.system(command)


