if [ -z $1 ];
then
    echo "Energy not set"
    python play.py --map-type custom --area 8 --stage-energy 6 --stage 4 --wait 20 --no-supporter --log VERBOSE
else
    echo "Energy set"
    python play.py --map-type custom --area 8 --stage-energy 6 --stage 4 --wait 20 --no-supporter --log VERBOSE --world-energy $1
fi
