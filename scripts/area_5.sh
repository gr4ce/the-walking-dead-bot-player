ENERGY=68

if [ -z $1 ];
then
    echo "Energy not set"    
else
    echo "Energy set"
    ENERGY=$1
fi

echo "Will start with ENERGY=$ENERGY"

python play.py --no-supporter --map-type world --area 5 --stage 3 --wait 65 --energy $ENERGY
