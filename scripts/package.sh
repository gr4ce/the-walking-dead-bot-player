#!/bin/bash

DATE_TIME=$(date | awk -F " " '{print $2"-"$3"-"$6"-"$4}'|sed s/:/_/g)

echo "Creating archive twd-package-$DATE_TIME.tar.gz"

tar -czvf twd-package-$DATE_TIME.tar.gz \
	scripts \
	data \
	lib \
	play.py 


