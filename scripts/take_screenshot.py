#!/usr/bin/python

import os
import sys
import time

DEVICE_ONE_PLUS_3T = 100
DEVICE_NEXUS_5     = 101

def read_device_id():
    device_file = "deviceid.dat"
    os.system("adb devices | awk 'FNR == 2 {print $1}' > " + device_file)
    f = open(device_file, "r")
    device_id = f.read()
    f.close()

    return device_id.strip()

def screenshot(filename = "screenshot.png"):
    
    deviceid   = read_device_id()
    devicename = None
    command    = None
    if deviceid == "64f99753":
        deviceid = DEVICE_ONE_PLUS_3T
        devicename = "OnePlus3"
        command = "adb shell screencap -p  > " + filename
    else:
        deviceid = DEVICE_NEXUS_5
        devicename = "Nexus5"
        command = "adb shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > " + filename        
    print "Capturing from " + devicename + " a screenshot in " + filename
    
    os.system(command)
   
def main():
    if len(sys.argv) == 2:
        screenshot(sys.argv[1])
    else:
        screenshot()
    return 0

if __name__ == "__main__":
    sys.exit(main())
