
if [ -z $1 ];
then
    echo "Energy not set, using maximum"
    python play.py --map-type custom --area 6 --stage-energy 3 --stage 3 --wait 20 --no-supporter --log VERBOSE    
else
    echo "Energy set"
    ENERGY=$1
    echo "Will start with ENERGY=$ENERGY"
    python play.py --map-type custom --area 6 --stage-energy 3 --stage 3 --wait 20 --no-supporter --log VERBOSE --world-energy $ENERGY    
fi




