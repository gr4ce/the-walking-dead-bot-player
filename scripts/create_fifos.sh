#!/bin/bash

echo "Creating fifos for remote access..."
rm -f twd_recv_fifo twd_send_fifo twd_send_filtered_fifo
mkfifo twd_recv_fifo twd_send_fifo twd_send_filtered_fifo
echo "All good!"

