#!/bin/bash
echo "Cleaning previous created files..."
rm -f ../../TelegramCLI/bin/tg_recv_fifo
rm -f ../../TelegramCLI/bin/tg_send_fifo
rm -f ../twd_recv_fifo
rm -f ../twd_send_fifo
rm -f ../twd_send_filtered_fifo
sleep 1

echo "Creating fifos..."
mkfifo ../../TelegramCLI/bin/tg_recv_fifo
mkfifo ../../TelegramCLI/bin/tg_send_fifo
mkfifo ../twd_send_filtered_fifo
sleep 1

echo "Linking fifos"
ln -s ../TelegramCLI/bin/tg_recv_fifo ../twd_recv_fifo
ln -s ../TelegramCLI/bin/tg_send_fifo ../twd_send_fifo
sleep 1

