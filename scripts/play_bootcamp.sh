
if [ -z "$1" ];
then
	python3.8 play.py --map-type roadmap --area 6 --stage-energy 4 --stage 3 --wait 15 --no-supporter --log VERBOSE
else
	python3.8 play.py --map-type roadmap --area 6 --stage-energy 4 --stage 3 --wait 15 --no-supporter --log VERBOSE --world-energy $1
fi
