#!/bin/bash

if [ -z $1 ];
then
    echo "Energy not set"
    python play.py --map-type raid --wait 20 --raid-energy 6 --log VERBOSE
else
    echo "Energy set"
    python play.py --map-type raid --wait 20 --raid-energy $1 --log VERBOSE
fi
