import os
import sys
import time
import unittest
import datetime
import threading

print(threading.enumerate())

# Project specific imports from lib
import lib.logger as logger

from lib.activities import DummyActivity
from lib.scheduler import ActivityTrigger, ScheduledActivity



logger.add_module("TiggerActivity")
logger.add_module("FireActivity")
logger.add_module("ActivityScheduler")
logger.add_module("ActivityTrigger")
logger.set_log_level("PEDANTIC")

print(threading.enumerate())

class TiggerActivity(unittest.TestCase):
    def test_size(self):

        print("\nRunning   TiggerActivity - test_size")
        
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        trigger = ActivityTrigger()
        logger.log_info("Adding an activity to be trigger in 1 sec")
        trigger.arm(sa, 1)
        self.assertEqual(len(trigger.get_sleep_queue()), 1)
        time.sleep(4)

        self.assertEqual(len(trigger.get_sleep_queue()), 0)
        trigger.terminate(wait=True)
        self.assertEqual(trigger.scheduler.executor_done, True)
        self.assertEqual(trigger.terminated, True)
        
        print("Completed TiggerActivity - test_size")

    def test_trigger_date(self):
        
        print("\nRunning   TiggerActivity - test_trigger_date")

        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        trigger = ActivityTrigger()
        logger.log_info("Adding an activity to be trigger in 1 sec")

        time_delay = 1

        trigger_date = datetime.datetime.now() + datetime.timedelta(0, time_delay)
        trigger.arm(sa, time_delay)
        sleep_queue = trigger.get_sleep_queue()
        queue_time = sleep_queue[0].get_trigger_time()

        logger.log_info("Trigger date from the queue : " + str(queue_time))
        logger.log_info("Calculated trigger date     : " + str(trigger_date))

        time.sleep(6)

        self.assertEqual(queue_time.hour == trigger_date.hour, True)
        self.assertEqual(queue_time.minute == trigger_date.minute, True)
        self.assertEqual(queue_time.second == trigger_date.second or queue_time.second ==
                         trigger_date.second + 1 or queue_time.second == trigger_date.second - 1, True)

        trigger.terminate(wait=True)
        self.assertEqual(trigger.scheduler.executor_done, True)
        self.assertEqual(trigger.terminated, True)

        print("Completed TiggerActivity - test_trigger_date")

    def test_cancel_armed_event(self):

        print("\nRunning   TiggerActivity - test_cancel_armed_event")

        dummy_activity_list = []
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        trigger = ActivityTrigger()
        trigger.arm(dummy_activity_list[0], 3)
        trigger.arm(dummy_activity_list[0], 4)
        self.assertEqual(len(trigger.get_sleep_queue()), 2)
        trigger.cancel_first()
        time.sleep(1)
        self.assertEqual(len(trigger.get_sleep_queue()), 1)
        trigger.cancel_first()
        time.sleep(1)
        self.assertEqual(len(trigger.get_sleep_queue()), 0)
        trigger.terminate(wait=True)
        self.assertEqual(trigger.scheduler.executor_done, True)
        self.assertEqual(trigger.terminated, True)

        print("Completed TiggerActivity - test_cancel_armed_event")


class FireActivity(unittest.TestCase):
    # def test_trigger_happens(self):
    #     print("\nRunning   FireActivity - test_trigger_happens")

    #     sa = ScheduledActivity()
    #     sa.set_activity(DummyActivity())
    #     trigger = ActivityTrigger()
    #     logger.log_info("Adding an activity to be trigger in 1")

    #     time_delay = 3
    #     trigger.arm(sa, time_delay)
    #     time.sleep(5)
    #     trigger.terminate(wait=True)
    #     self.assertEqual(trigger.scheduler.executor_done, True)
    #     self.assertEqual(trigger.terminated, True)
    #     print("Completed FireActivity - test_trigger_happens")

    def test_multiple_events(self):

        print("\nRunning   FireActivity - test_multiple_events")

        dummy_activity_list = []
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        trigger = ActivityTrigger()
        logger.log_info("Adding multiple events triggering every second")

        trigger.arm(dummy_activity_list[0], 1)
        trigger.arm(dummy_activity_list[1], 2)
        trigger.arm(dummy_activity_list[2], 3)
        logger.log_info("Waiting all activities to complete")
        time.sleep(15)

        trigger.terminate(wait=True)
        self.assertEqual(trigger.scheduler.executor_done, True)
        self.assertEqual(trigger.terminated, True)

        print("Completed FireActivity - test_multiple_events")

    def test_multiple_events_reverse(self):

        print("\nRunning   FireActivity - test_multiple_events_reverse")

        dummy_activity_list = []
        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)

        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)

        sa = ScheduledActivity()
        sa.set_activity(DummyActivity())
        dummy_activity_list.append(sa)
        trigger = ActivityTrigger()
        logger.log_info("Adding multiple events triggering every second")

        trigger.arm(dummy_activity_list[0], 3)
        trigger.arm(dummy_activity_list[1], 1)
        trigger.arm(dummy_activity_list[2], 2)
        trigger.print_queue_content()

        time.sleep(10)
        trigger.terminate(wait=True)
        self.assertEqual(trigger.scheduler.executor_done, True)
        self.assertEqual(trigger.terminated, True)
        print(threading.enumerate())

        print("Completed FireActivity - test_multiple_events_reverse")


if __name__ == '__main__':
    unittest.main()