import os
import sys
import unittest

# Project specific imports from lib
import lib.logger as logger

# Project specific imports from test
import activity_test


if __name__ == "__main__":
    
    #logger.log_info("Running all tests", "TestRunner")    
    #logger.log_info("Running activity test", "TestRunner")
    #activity_test.run()

    activityTestSuite = unittest.TestSuite()
    activityTestSuite.addTest(activity_test.ActivityWorldAreaTest)
    #runner = unittest.TextTestRunner()
    #runner.run(ActivityWorldAreaTest)

    unittest.main()
