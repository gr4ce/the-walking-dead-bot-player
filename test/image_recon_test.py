import os
import unittest
import threading


# Project specific imports from lib
import lib.logger as logger
import lib.device_handler as device_handler
import lib.img_detector as img_detector


logger.add_module("ImageScanning")
device = device_handler.get_device()

# Current directory is the root of the project
screenshot_base_image_directory = "test/images/"
target_base_image_directory = "data/images/" + device.devicename + "/"


class ImageScanning(unittest.TestCase):

    def test_scan_home(self):
        print("\nRunning   ImageScanning - test_scan_home")

        screenshot_file_name = screenshot_base_image_directory + "home_screenshot.png"
        target_file_name = target_base_image_directory + "home_screenshot_target.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)
        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))
        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertEqual(len(result), 1)

        print(threading.enumerate())
        print("Completed ImageScanning - test_scan_home")

    def test_scan_area_13_on_map(self):
        print("\nRunning   ImageScanning - test_scan_area_13_on_map")

        screenshot_file_name = screenshot_base_image_directory + "home_screenshot.png"
        target_file_name = target_base_image_directory + "home_screenshot_target.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertEqual(len(result), 1)

        print("Completed ImageScanning - test_scan_area_13_on_map")

    def test_scan_area_13_stages_energy_6(self):
        print("\nRunning   ImageScanning - test_scan_area_13_stages_energy_6")

        screenshot_file_name = screenshot_base_image_directory + \
            "world_area13_stages_screenshot.png"
        target_file_name = target_base_image_directory + \
            "world/" + "world_stage_energy_6.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertEqual(len(result), 5)

        print("Completed ImageScanning - test_scan_area_13_stages_energy_6")

    def test_scan_area_13_stages_energy_7(self):
        print("\nRunning   ImageScanning - test_scan_area_13_stages_energy_7")

        screenshot_file_name = screenshot_base_image_directory + \
            "world_area13_stages_screenshot.png"
        target_file_name = target_base_image_directory + \
            "world/" + "world_stage_energy_7.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertEqual(len(result), 3)

        print("Completed ImageScanning - test_scan_area_13_stages_energy_7")

    def test_raid_won(self):
        print("\nRunning   ImageScanning - test_raid_won")

        screenshot_file_name = screenshot_base_image_directory + "raid_won_screenshot.png"
        target_file_name = target_base_image_directory + "raid/" + "winner.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertGreaterEqual(len(result), 1)

        print("Completed ImageScanning - test_raid_won")

    def test_raid_timeout(self):
        print("\nRunning   ImageScanning - test_raid_timeout")

        screenshot_file_name = screenshot_base_image_directory + "raid_lost_screenshot.png"
        target_file_name = target_base_image_directory + "raid/" + "defeated.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertGreaterEqual(len(result), 1)

        print("Completed ImageScanning - test_raid_timeout")

    def test_raid_lost(self):
        print("\nRunning   ImageScanning - test_raid_lost")

        screenshot_file_name = screenshot_base_image_directory + "raid_lost_screenshot.png"
        target_file_name = target_base_image_directory + "raid/" + "defeated.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertGreaterEqual(len(result), 1)

        print("Completed ImageScanning - test_raid_lost")

    def test_rts_daily_won(self):
        print("\nRunning   ImageScanning - test_rts_daily_won")

        screenshot_file_name = screenshot_base_image_directory + \
            "rtsd_completed_screenshot.png"
        target_file_name = target_base_image_directory + \
            "rtsd/" + "stage_cleared_ok_button.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertGreaterEqual(len(result), 1)

        print("Completed ImageScanning - test_rts_daily_won")

    def test_rts_daily_lost(self):
        print("\nRunning   ImageScanning - test_rts_daily_lost")

        screenshot_file_name = screenshot_base_image_directory + "rtsd_lost_screenshot.png"
        target_file_name = target_base_image_directory + "rtsd/" + "all_dead.png"

        logger.log_debug("Screenshot file = " + screenshot_file_name)
        logger.log_debug("Target file     = " + target_file_name)

        self.assertTrue(os.path.exists(screenshot_file_name))
        self.assertTrue(os.path.exists(target_file_name))

        result = img_detector.detect_multiple_matches(
            screenshot_file_name, target_file_name, 0.95)

        self.assertNotEqual(result, None)
        self.assertGreaterEqual(len(result), 1)

        print("Completed ImageScanning - test_rts_daily_lost")


if __name__ == '__main__':
    unittest.main()
