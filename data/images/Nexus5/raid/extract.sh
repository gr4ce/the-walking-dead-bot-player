#!/bin/bash

# Consider that $1 parameter should be the source screenshot that should have
# all the information required for the rest of the images to be captured. Note
# that the timestamp field is pupulated from the input file, so be aware of
# the format                  

timestamp=$(echo $1|cut -c 1-19)
mkdir $timestamp

convert -crop 260x295+1130+400 $1 $timestamp-opponent-leader.png
convert -crop 460x45+1185+340  $1 $timestamp-opponent-name.png
convert -crop 150x50+1540+490  $1 $timestamp-opponent-reputation.png
convert -crop 270x135+1410+560 $1 $timestamp-opponent-faction.png

echo "" > $timestamp-opponent-hash.txt

md5sum $timestamp-opponent-leader.png 	  >> $timestamp-opponent-hash.txt
md5sum $timestamp-opponent-name.png 	  >> $timestamp-opponent-hash.txt
md5sum $timestamp-opponent-reputation.png >> $timestamp-opponent-hash.txt
md5sum $timestamp-opponent-faction.png    >> $timestamp-opponent-hash.txt

mv $timestamp-opponent* $timestamp
