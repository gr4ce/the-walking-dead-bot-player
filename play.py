#!/usr/bin/python3.8
""" 
This is the entry point that starts the game controller and imports all the other modules
"""
import os
import time
import datetime
from multiprocessing import Event

# Other game modules
import lib.buttons as b
import lib.logger as logger
import lib.energy_handler as energy
import lib.parameters as parameters
import lib.msg_handler as msg_handler
import lib.stats_handler as stats_handler
import lib.device_handler as device_handler

# Import all the needed activities
from lib.scheduler import ActivityTrigger
from lib.scheduler import ScheduledActivity
from lib.activities import PlayRaid
from lib.activities import PlayWorldmap
from lib.activities import PlayBootcamp
from lib.activities import PlayRoadToSurvivalDaily
from lib.activities import PlayRoadToSurvivalTournament
#from lib.activities import ScanRoadmapForStages


DEBUG = False
SIM_MODE = False
ASYNC = True
WAIT_TME = 30


SCREENSHOT_PATH = ""

# MAXIMUM VALUES
SECONDS_IN_ONE_MINUTE = 60
START_PLAY_AT = 40
STOP_PLAY_AT = 5

MATCH_TIME = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
SCREENSHOT_PATH = "./screenshots/game-" + MATCH_TIME + "/"
TMP_SCREENSHOT_PATH = "./screenshots/tmp/"
HISTORY_SCREENSHOT_PATH = "./screenshots/tmp/history/"


SCREENSHOT_DATA = [-1, -1, -1]
DEVICE = None
ARGS = None


def print_array_of_coordinates(array_of_coordinates):

    for elem in array_of_coordinates:
        print("[" + str(elem[0]) + "," + str(elem[1]) + "]")
    print("")


# ACTIVE = 1
# SLEEPING = 0

# STATUS_ACTIVE = 1
# STATUS_SLEEPING = 0


# class OpponentType:
#     HUMAN, WALKERS = range(2)


# class OpponentTrait:
#     FAST, ALERT, STRONG, TOUGHT = range(4)


# class MapType:
#     WORLD, ROADMAP = range(2)


SCREENSHOT_THREAD_EVENT = Event()
SCREENSHOT_THREAD_EVENT.clear()



def from_rect_to_point(rect):
    return [rect.x + (rect.width / 2), rect.y + (rect.height / 2)]


def merge_results(x, y):
    z = x.copy()
    z.update(y)
    return z


def quit_game():

    logger.log_info("Pressing home button", "Controller")
    DEVICE.press_home_button()

    logger.log_debug("Press menu button", "Controller")
    command = "shell input tap 830 1850"
    DEVICE.execute_adb_command(command)
    time.sleep(1)

    logger.log_debug("Closing applications", "Controller")
    # Assumption! There should only be an application running.. we might close more than one
    for _ in range(1, 4):
        command = "shell input swipe 600 1000 1200 1000 100"
        # command = "adb shell input swipe 450 650 1400 650 100"
        DEVICE.execute_adb_command(command)
        time.sleep(1)

    # We need to go back home
    DEVICE.press_home_button()
    time.sleep(2)


def start_game():

    DEVICE.unlock()
    DEVICE.press_home_button()
    DEVICE.press_game_icon()

    # Now wait a bit as this can take a very long time
    time.sleep(30)


def regain_control_sequence():

    # Keep an eye on timing
    # take_tmp_screenshot_and_send()
    logger.log_debug("Close Advert", "Player")
    DEVICE.pree_point(b.CLOSE_ADVERT)
    time.sleep(3)

    logger.log_debug("Switching Account", "Player")
    # os.system("adb shell input tap 820 530")
    DEVICE.pree_point(b.SWITCH_ACCOUNT)
    time.sleep(3)

    # take_tmp_screenshot_and_send()
    logger.log_debug("Connect with facebook", "Player")
    # os.system("adb shell input tap 480 680")
    DEVICE.pree_point(b.CONNECT_WITH_FB)
    time.sleep(3)

    # take_tmp_screenshot_and_send()
    logger.log_debug("Confirm connection", "Player")
    # os.system("adb shell input tap 620 650")
    DEVICE.pree_point(b.CONNECT_WITH_FB_CONFIRM)
    time.sleep(3)

    # take_tmp_screenshot_and_send()
    secs_waiting = 20
    logger.log_debug("Waiting transfer for " +
                     str(secs_waiting) + " sec", "Player")
    time.sleep(secs_waiting)

    logger.log_debug("Transfer complete", "Player")
    # os.system("adb shell input tap 630 510")
    DEVICE.pree_point(b.TRANSFER_COMPLETE)
    time.sleep(3)

    logger.log_debug("Close offer", "Player")
    DEVICE.pree_point(b.CLOSE_OFFER)
    time.sleep(3)

    logger.log_debug("Close Raid", "Player")
    DEVICE.pree_point(b.CLOSE_OFFER)
    time.sleep(3)


def start_sequence():

    global DEVICE
    # Keep an eye on timing
    # take_tmp_screenshot_and_send()
    tag = "StartSequence"

    logger.log_debug("Close Game", tag)
    DEVICE.close_game()
    time.sleep(2)

    logger.log_debug("Open Game", tag)
    DEVICE.unlock()
    DEVICE.press_game_icon()
    time.sleep(10)

    logger.log_debug("Close Advert", tag)
    DEVICE.pree_point(b.CLOSE_ADVERT)
    time.sleep(2)

    logger.log_debug("Close offer", tag)
    DEVICE.pree_point(b.CLOSE_OFFER)
    time.sleep(2)

    logger.log_debug("Close Raid", tag)
    DEVICE.pree_point(b.CLOSE_OFFER)
    time.sleep(2)


def refill_ingame_world_energy(area, stage, match):

    global SCREENSHOT_DATA

    time.sleep(3)
    component = "Player"
    logger.log_info("Ok to war has ended", component)
    DEVICE.press_point(b.OK_TO_WAR_RESULT)
    time.sleep(1)

    logger.log_info("Checking if we were raided", component)
    DEVICE.press_point(b.IGNORE_RAIDED)
    time.sleep(1)

    logger.log_info("Playing " + str(area.area_name) +
                    " - Stage " + str(stage), component)
    DEVICE.press_point([area.all_stages[stage - 1][0],
                        area.all_stages[stage - 1][1]])
    time.sleep(1)

    # We need a way to figure out how to do this and skip
    logger.log_info(
        "Fight anyway if roster of weapon inventory full", component)
    DEVICE.pree_point(b.FIGHT_ANYWAY)
    time.sleep(1)

    logger.log_info("Team select", component)
    DEVICE.pree_point(b.TEAM_SELECT)
    time.sleep(1)

    DEVICE.pree_point(b.FIGHT)
    time.sleep(1)

    # Refill and go back
    energy.timer["world"].use_refill()
    DEVICE.press_point([550, 500])
    time.sleep(1)

    logger.log_info("Go back to stages", component)
    DEVICE.pree_point(b.CLOSE_AD)
    time.sleep(1)


def configure_logger():
    logger.set_log_level("PEDANTIC")
    logger.add_module("CommandListener")
    logger.add_module("CommandReceiver")
    logger.add_module("MainLoop")
    logger.add_module("EnergyHandler")
    logger.add_module("Device")
    logger.add_module("Activity")
    logger.add_module("Boot")


def main():

    global WAIT_TME
    global ARGS
    global DEVICE

    ARGS = parameters.initialise()

    if not os.path.exists(SCREENSHOT_PATH):
        os.makedirs(SCREENSHOT_PATH)

    map_type = ARGS.map_val
    area = ARGS.area_val
    stage = ARGS.stage_val
    mode = ARGS.mode_val
    WAIT_TME = int(ARGS.wait_val)
    stage_energy = int(ARGS.stage_energy)

    configure_logger()
    component = "MainLoop"

    stats_handler.initialise(SCREENSHOT_PATH)
    activity_trigger = ActivityTrigger()
    msg_handler.initialise(activity_trigger)

    # Initialising timers for various energy
    energy.initialise_timer("rts",
                            parameters.RTST_ENERGY_CAP,
                            ARGS.rtst_energy,
                            parameters.RTST_RECHARGE_TIME_MINUTES,
                            parameters.RTST_RECHARGE_TIME_SECONDS,
                            parameters.RTST_RECHARGE_TIME_MINUTES,
                            parameters.RTST_RECHARGE_TIME_SECONDS)

    energy.initialise_timer("world",
                            parameters.WORLD_ENERGY_CAP,
                            ARGS.world_energy,
                            parameters.RECHARGE_TIME_MINUTES,
                            parameters.RECHARGE_TIME_SECONDS,
                            parameters.RECHARGE_TIME_MINUTES,
                            parameters.RECHARGE_TIME_SECONDS)

    energy.initialise_timer("raid",
                            parameters.RAID_ENERGY_CAP,
                            ARGS.raid_energy,
                            parameters.RAID_RECHARGE_TIME_MINUTES,
                            parameters.RAID_RECHARGE_TIME_SECONDS,
                            parameters.RAID_RECHARGE_TIME_MINUTES,
                            parameters.RAID_RECHARGE_TIME_SECONDS)

    logger.log_info("Walking Dead: Road to survival", component)
    logger.log_info("Command Line Arguments are:", component)

    # logger.log_info("Tablet   : " + str(args.tablet_val)  + " - Using tablet", component)
    logger.log_info("Energy   : " + str(ARGS.world_energy) +
                    " - World energy start value", component)
    logger.log_info("Map      : " + map_type + " - Map type", component)
    logger.log_info("Area     : " + area + " - Area identifier", component)
    logger.log_info("Stage    : " + stage +
                    " - Stage within the area", component)
    logger.log_info("Mode     : " + mode + " - How we pick stages", component)
    logger.log_info("Refills  : " + str(ARGS.refills_val) +
                    " - Number of refills to be used", component)
    logger.log_info("Stage en : " + str(stage_energy) +
                    " - In case we are using a custom map", component)
    logger.log_info("Time     : " + str(ARGS.time_val[0]) + ":" + str(
        ARGS.time_val[1]) + " - Time start value", component)
    logger.log_info("Increment: " + str(ARGS.incremental), component)
    logger.log_info("Take scsh: " + str(ARGS.take_screenshot), component)
    logger.log_info("TS    arg: " + str(ARGS.take_screenshot), component)
    logger.log_info(
        "Send scsh: " + str(ARGS.send_screenshot and ARGS.take_screenshot), component)
    logger.log_info("Supporter: " + str(ARGS.supporter), component)

    DEVICE = device_handler.get_device()
    device_handler.current_device = DEVICE

    energy.timer["rts"].start_timer()
    energy.timer["raid"].start_timer()
    energy.timer["world"].start_timer()
    energy.timer["world"].set_energy(ARGS.world_energy)

   

    # # Configuring the raid activity
    raid = PlayRaid()
    raid.set_screenshot_folder(SCREENSHOT_PATH)
    raid.configure({"raid_tournament": ARGS.raid_tournament})
    raid_activity = ScheduledActivity()
    raid_activity.set_activity(raid)

    # Configuring bootcamp activity
    bootcamp = PlayBootcamp()
    bootcamp.configure(energy.timer["world"])
    bootcamp_activity = ScheduledActivity()
    bootcamp_activity.set_activity(bootcamp)

    # Configuring road to survival daily activity
    road_to_survival_daily = PlayRoadToSurvivalDaily()
    road_to_survival_daily_activity = ScheduledActivity()
    road_to_survival_daily_activity.set_activity(road_to_survival_daily)

    # Configuring world map activity
    world_map = PlayWorldmap()
    world_map.configure(energy.timer["world"])
    world_map_activity = ScheduledActivity()
    world_map_activity.set_activity(world_map)

    # Configuring road to survival tournament activity
    road_to_survival_tournament = PlayRoadToSurvivalTournament()
    road_to_survival_tournament_activity = ScheduledActivity()
    road_to_survival_tournament_activity.set_activity(
        road_to_survival_tournament)

    # Queuing up all the activities, the scheduler will figure out which one
    # to play first
    activity_trigger.print_queue_content()
    activity_trigger.arm(raid_activity, 9)
    activity_trigger.arm(road_to_survival_tournament_activity, 5)
    activity_trigger.arm(world_map_activity, 11)
    activity_trigger.arm(bootcamp_activity, 3)
    activity_trigger.arm(road_to_survival_daily_activity, 7)
    activity_trigger.print_queue_content()

    msg_handler.command_receiver.thread.join()


if __name__ == '__main__':

    main()

    # try:
    #    main()
    # except:
    #    raise
    # finally:
    #    logger.log_info("Setting the device to a good state","MainEntry")
    #    device.set_stay_always_on(True)
    #    time.sleep(1)
    #    device.restore_screen_timeout()
    #    time.sleep(1)

# end of main
