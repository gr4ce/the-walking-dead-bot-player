#!/usr/bin/python

import os
import time
import threading


import lib.logger as logger
import lib.energy_handler as energy
import lib.device_handler as device_handler
import lib.parameters as parameters
import lib.scheduler as scheduler


# We will be using two fifos. Although fifoa are bidirectionl
# due to the asynchronous nature of the application we don't
# want the program to read what it just wrote on the fifo
receive_command_fifo = os.getcwd() + "/twd_send_filtered_fifo"
send_log_fifo = "twd_recv_fifo"

initialised = False
command_receiver = None
CRITICAL_SECTION = threading.Lock()


class Command:

    def __init__(self):
        self.command = ""
        self.params = []

    def set_command(self, command):
        self.command = command

    def set_params(self, params):
        self.params = params


class CommandDecoder:

    def parse(self, input_string):
        logger.log_debug("Parsing input string: " +
                         input_string.strip(), "CommandListener")
        tokens = input_string.rstrip().split(" ")

        if tokens[0] == "com":
            comm = Command()
            comm.set_command(tokens.pop(0))
            comm.set_params(tokens)
            return comm
        else:
            logger.log_error("Excetpion parsing input string",
                             "CommandListener")
            return None


def remove_newline(string):
    return string[0:len(string)-1]


def send_remote_message(message):
    if parameters.env["environment"] == "test":
        return
    if parameters.args is not None and parameters.args.no_remote:
        return
    os.system('echo "msg Giuseppe_Salvatore ' +
              message + '" > twd_recv_fifo &')


def send_remote_screenshot(abs_image_path):
    if parameters.env["environment"] == "test":
        return
    if parameters.args is not None and parameters.args.no_remote:
        return
    os.system('echo "send_photo Giuseppe_Salvatore ' +
              abs_image_path + '" > twd_recv_fifo &')


def compress_and_send_screenshot(abs_image_path):
    if parameters.env["environment"] == "test":
        return      
    if parameters.args is not None and parameters.args.no_remote:
        return
    command = 'convert ' + abs_image_path + \
        ' -resize 90% -quality 16 ' + abs_image_path
    command = command.replace("·png", ".jpg")
    os.system(command)
    abs_image_path = abs_image_path.replace("·png", ".jpg")
    os.system('echo "send_photo Giuseppe_Salvatore ' +
              abs_image_path + '" > twd_recv_fifo &')


def capture_compress_and_send_screenshot():
    if parameters.env["environment"] == "test":
        return
    abs_image_path = os.getcwd() + "/tmp/quick_screenshot.png"
    logger.log_verbose("Path for screenshot is " +
                       abs_image_path, "CommandListener")
    device_handler.current_device.capture_screenshot(abs_image_path)
    compress_and_send_screenshot(abs_image_path)


control_requested = False


def initialise(activity_trigger):


    CRITICAL_SECTION.acquire()
    global initialised
    global command_receiver

    if parameters.env["environment"] == "test":
        return

    if not initialised:
        command_receiver = CommandReceiver(activity_trigger)
        initialised = True
    CRITICAL_SECTION.release()


class CommandReceiver():

    def __init__(self,activity_trigger):
        self.name = "CommandReceiver"
        self.fifo_initialised = False
        self.thread = threading.Thread(
            name="CommandReceiverThread",
            target=self.thread_body)
        self.thread.start()
        self.trigger = activity_trigger

    def initialise_fifos(self):
        if not os.path.exists(receive_command_fifo):
            os.mkfifo(receive_command_fifo)
        self.fifo = open(receive_command_fifo, "r")

        if not os.path.exists(send_log_fifo):
            os.mkfifo(send_log_fifo)

        self.fifo_initialised = True
        logger.log_info("Fifos initialised")

    def receive_message(self):
        if not self.fifo_initialised:
            self.initialise_fifos()
        return self.fifo.readline()

    def thread_body(self):
        global control_requested
        logger.log_info(self.name + " is starting")

        logger.log_pedantic("Instantiating " + self.name)
        decoder = CommandDecoder()
        logger.log_pedantic(self.name + " instantiated")

        while True:

            logger.log_info("Ready to receive messages")
            string = self.receive_message()
            if string == '':
                break

            send_remote_message(string)

            command = decoder.parse(string)
            if command != None:
                try:
                    # com rc 0/1
                    if command.params[0] == "rc":
                        if command.params[1] == "1":
                            control_requested = True
                            logger.log_debug(
                                "Got a control request, serving it at the early occasion")
                            send_remote_message(
                                "Got a control request, serving it at the early occasion")
                        elif command.params[1] == "0":
                            control_requested = False
                            logger.log_debug("Retaking control of game")
                            send_remote_message("Retaking control of game")
                        else:
                            send_remote_message(
                                "Ignoring unknown rc param: " + command.params[1])
                    # com set energy <num>
                    # com set energy refill
                    # com set timer <num1> <num2>
                    # com set wait <num1>
                    # com set area <num1> <num2>
                    elif command.params[0] == "set":
                        if command.params[1] == "we":
                            if command.params[2] == "refill":
                                energy.timer["world"].use_refill()
                                logger.log_debug(
                                    "Refilling world energy: " + 
                                    str(energy.timer["world"].curr_energy), self.name)
                                send_remote_message(
                                    "Refilling world energy: " + 
                                    str(energy.timer["world"].curr_energy))
                            else:
                                energy.timer["world"].set_energy(
                                    int(command.params[2]))
                                logger.log_debug(
                                    "Setting world energy: " + 
                                    str(energy.timer["world"].curr_energy), self.name)
                                send_remote_message(
                                    "Setting world energy: " + 
                                    str(energy.timer["world"].curr_energy))
                        elif command.params[1] == "re":
                            if command.params[2] == "refill":
                                energy.timer["raid"].use_refill()
                                logger.log_debug(
                                    "Refilling raid energy: " + 
                                    str(energy.timer["raid"].curr_energy), self.name)
                                send_remote_message(
                                    "Refilling raid energy: " + 
                                    str(energy.timer["raid"].curr_energy))
                            else:
                                energy.timer["raid"].set_energy(
                                    int(command.params[3]))
                                logger.log_debug(
                                    "Setting raid energy: " + 
                                    str(energy.timer["raid"].curr_energy), self.name)
                                send_remote_message(
                                    "Setting raid energy: " + 
                                    str(energy.timer["raid"].curr_energy))
                        elif command.params[1] == "worldtimer":
                            send_remote_message("Setting world timer")
                            energy.timer["world"].set_timer_value(
                                int(command.params[2]), int(command.params[3]))
                            minutes, seconds = energy.timer["world"].get_timer_value()
                            send_remote_message(
                                "Now timer is = %d:%d" % (minutes, seconds))
                        elif command.params[1] == "wait":
                            send_remote_message("Got a set wait request")
                            parameters.args.WAIT_TIME = int(command.params[2])
                            send_remote_message("Now waiting time is %d seconds" % (
                                parameters.args.WAIT_TIME))
                        else:
                            send_remote_message(
                                "Unknown set param: " + command.params[1])
                            print("ERROR!")

                    # com dump
                    elif command.params[0] == "dump":
                        dump = "WorldEnergy.energy       = " + \
                            str(energy.timer["world"].energy_left())
                        send_remote_message(dump)
                        dump = "WorldEnergy.timer        = " + \
                            str(energy.timer["world"].curr_minutes) + \
                            ":" + str(energy.timer["world"].curr_seconds)
                        send_remote_message(dump)
                        dump = "WorldEnergy.timer_status = " + \
                            str(energy.timer["world"].timer_status)
                        send_remote_message(dump)
                        dump = "log.level = " + logger.get_log_level_string()
                        send_remote_message(dump)

                    # com log <level>
                    elif command.params[0] == "log":
                        level_string = command.params[1]
                        logger.set_log_level(level_string)
                        send_remote_message("log level set to " + logger.get_log_level_string())

                    elif command.params[0] == "stats":
                        send_remote_message("not yet implemented")

                    # com help
                    elif command.params[0] == "help":
                        send_remote_message(
                            "com rc 1 (requesting control)")
                        send_remote_message(
                            "com rc 0 (giving up control)")
                        send_remote_message(
                            "com set we <value> (sets world energy to <value>)")
                        send_remote_message(
                            "com set we refill (refills world energy)")

                    # com power
                    elif command.params[0] == "power":
                        send_remote_message("Pressing power button")
                        device_handler.current_device.press_power_button()

                    # com ss
                    elif command.params[0] == "ss":
                        send_remote_message("Send screenshoot")
                        device_handler.current_device.capture_screenshot()

                    elif command.params[0] == "act":
                        
                        queue = self.trigger.get_queue_content()
                        if len(queue) == 0:
                            send_remote_message("Activity Queue is empty!")
                        else:
                            send_remote_message("Activity Queue content:")
                            for elem in queue:
                                name = elem.get_activity().name
                                clock = str(elem.get_trigger_time())[:16]
                                time.sleep(0.2)
                                send_remote_message(name + " will run at " + clock)
                            

                    # com ts 0/1
                    elif command.params[0] == "ts":
                        if command.params[1] == "1":
                            parameters.args.take_screenshot = True
                            send_remote_message(
                                "Activating capturing screenshot")
                        elif command.params[1] == "0":
                            parameters.args.take_screenshot = False
                            send_remote_message(
                                "Deactivating capturing screenshot")
                        else:
                            send_remote_message(
                                "Capturing screenshot param unknown: " + command.params[1])

                    elif command.params[0] == "exit":
                        send_remote_message("Will exit asap")
                        parameters.args.exit = True

                    # com power
                    elif command.params[0] == "power":
                        send_remote_message("Pressing power button")
                        device_handler.current_device.press_power_button()

                    # com ss
                    elif command.params[0] == "ss":
                        send_remote_message("Send screenshoot")
                        device_handler.current_device.capture_screenshot()

                    # com ts 0/1
                    elif command.params[0] == "ts":
                        if command.params[1] == "1":
                            parameters.args.take_screenshot = True
                            send_remote_message(
                                "Activating capturing screenshot")
                        elif command.params[1] == "0":
                            parameters.args.take_screenshot = False
                            send_remote_message(
                                "Deactivating capturing screenshot")
                        else:
                            send_remote_message(
                                "Capturing screenshot param unknown: " + command.params[1])
                except Exception as e:
                    send_remote_message(
                        "Command not recognised, please try again")
                    time.sleep(0.5)
                    send_remote_message(str(e))
            else:
                print("ERROR!")
