#!/usr/bin/python

import os
import math
import datetime
from datetime import timedelta

import lib.logger as logger
import lib.executor as shell

MAX_FACTION_SUPPORTERS = 29
FACTION_SUPPORTER_FILE = os.path.join("data", "supporters.dat")
AVAILABLE_AFTER = 60 * 60 * 8      # 8 hours expressed in seconds


global global_supporters
global_supporters = []


def load_supporters():

    f = open(FACTION_SUPPORTER_FILE, 'r')
    supporter_id = 0

    del global_supporters[:]
    for line in f:
        global_supporters.append([supporter_id, line[0:len(line)-1]])
        supporter_id += 1

    f.close()


def pickup_first_available_faction_supporter():

    load_supporters()

    for supporter in global_supporters:
        supp_string = supporter[1]
        last_time_hired = datetime.datetime.strptime(
            supp_string[0:19], "%Y-%m-%d %H:%M:%S")
        delta_time = timedelta(seconds=1)
        delta_time = datetime.datetime.now() - last_time_hired
        _, seconds = math.modf(delta_time.total_seconds())
        if int(seconds) > AVAILABLE_AFTER:
            choosen_supporter = supporter[0]
            logger.log_debug(
                "Team supporter " + str(supporter[0]) + " will be used", "FactionSupporter")
            return choosen_supporter

    return -1


def choose_supporter_now(supporter_id):
    supporters = global_supporters
    for supporter in supporters:
        #print("Checking " + str(supporter[0]))
        if supporter_id == supporter[0]:
            #print("Updating " + str(supporter[0]) + supporter[1])
            now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            supporter[0] = supporter_id
            supporter[1] = now_string
            #print("Updated " + str(supporter[0]) + supporter[1])


def flush_supporters():
    supporters = global_supporters
    f = open(FACTION_SUPPORTER_FILE, 'w')
    for supporter in supporters:
        #print("Flushing " + supporter[1])
        f.write(supporter[1] + '\n')
    f.flush()
    f.close()
