"""
All the activities are modelled in this module
"""
import os
import time
import random
import datetime

import lib.buttons as b
import lib.util as util
import lib.logger as logger
import lib.energy_handler as energy
import lib.msg_handler as msg_handler
import lib.img_detector as img_detector
import lib.stats_handler as stats_handler
import lib.device_handler as device_handler


device = device_handler.current_device


map_positions = {"base": [1000, 750, 1000, 200], "low": [1000, 200, 1000, 750]}

first_stage_coordinates = {"3": [850, 800],
                           "4": [750, 800],
                           "5": [650, 800],
                           "6": [550, 800],
                           "7": [450, 800],
                           "8": [350, 800]}

stages_per_area = {
    #                   World areas
    "Area1": "5",
    "Area2": "5",
    "Area3": "5",
    "Area4": "6",
    "Area5": "6",
    "Area6": "6",
    "Area7": "7",
    "Area8": "7",
    "Area9": "7",
    "Area10": "8",
    "Area11": "7",
    "Area12": "8",
    "Area13": "8",
    #                   Roadmap areas
    "RoadmapEasy": "6",
    "RoadmapHard": "6",
    "Custom3": "3",
    "Custom4": "4",
    "Custom5": "5",
    "Custom6": "6",
    "Custom7": "7",
    "Custom8": "8"}

energy_per_stage = {"Area1": [3, 3, 3, 3, 3],
                    "Area2": [3, 3, 3, 3, 3],
                    "Area3": [3, 3, 3, 3, 3, 3],
                    "Area4": [3, 3, 3, 3, 3, 3],
                    "Area5": [3, 3, 3, 3, 3, 3],
                    "Area6": [3, 3, 3, 3, 3, 3],
                    "Area7": [3, 4, 4, 4, 4, 4, 4],
                    "Area8": [4, 4, 4, 4, 4, 4, 4],
                    "Area9": [4, 4, 4, 4, 5, 5, 5],
                    "Area10": [5, 5, 5, 5, 5, 5, 5, 5],
                    "Area11": [5, 5, 5, 5, 5, 6, 6],
                    "Area12": [6, 6, 6, 6, 6, 6, 6, 6],
                    "Area13": [6, 6, 6, 6, 6, 7, 7, 7],
                    "RoadmapEasy": [3, 3, 3, 3, 3, 3],
                    "RoadmapHard": [4, 4, 4, 4, 4, 4],
                    "Custom3": None,  # Custom maps need specific loading
                    "Custom4": None,
                    "Custom5": None,
                    "Custom6": None,
                    "Custom7": None,
                    "Custom8": None}

# Area swipe position and coordinates

map_zone = {"Area1": "none",
            "Area2": "none",
            "Area3": "none",
            "Area4": "none",
            "Area5": "low",
            "Area6": "low",
            "Area7": "none",
            "Area8": "none",
            "Area9": "none",
            "Area10": "none",
            "Area11": "none",
            "Area12": "base",
            "Area13": "base",
            "RoadmapEasy": "base",
            "RoadmapHard": "base",
            "Custom3": "base",
            "Custom4": "base",
            "Custom5": "base",
            "Custom6": "base",
            "Custom7": "base",
            "Custom8": "base"}

area_position = {"Area1": [800, 520],
                 "Area2": [800, 520],
                 "Area3": [800, 520],
                 "Area4": [1050, 770],
                 "Area5": [1050, 770],
                 "Area6": [800, 520],
                 "Area7": [800, 520],
                 "Area8": [800, 520],
                 "Area9": [800, 520],
                 "Area10": [800, 520],
                 "Area11": [800, 520],
                 "Area12": [1550, 470],
                 "Area13": [1150, 460],
                 "RoadmapEasy": [1150, 460],
                 "RoadmapHard": [1150, 460],

                 "Custom3": [0, 0],
                 "Custom4": [0, 0],
                 "Custom5": [0, 0],
                 "Custom6": [0, 0],
                 "Custom7": [0, 0],
                 "Custom8": [0, 0]}


up_coord = 800
low_coord = 950
shift_right = 170


def get_coord_array(stage_count_string):
    stage_count_int = int(stage_count_string)
    stage_coordinate_array = [first_stage_coordinates[stage_count_string]]

    prev_stage = stage_coordinate_array[0]

    for _ in range(1, stage_count_int):

        if prev_stage[1] == up_coord:
            next_stage_coords = [prev_stage[0] + shift_right, low_coord]
        else:
            next_stage_coords = [prev_stage[0] + shift_right, up_coord]
        prev_stage = next_stage_coords
        stage_coordinate_array.append(next_stage_coords)

    return stage_coordinate_array


class MapArea():

    def __init__(self, area_name, area_id):
        self.area_id = area_id
        self.area_name = area_name
        self.all_stages = get_coord_array(stages_per_area[area_name])
        self.stages = len(self.all_stages)
        self.stage_energies = energy_per_stage[area_name]
        self.map_zone = map_zone[area_name]
        self.position = area_position[area_name]

    def get_energy_needed(self, stage):
        return self.stage_energies[int(stage) - 1]

    def add_stage(self, stage):
        pass

    def get_stage(self, stage):
        return self.all_stages[stage - 1]

    def get_stage_count(self):
        return self.stages

    def get_location(self):
        return [self.map_zone, self.position]


AREA_1 = 1
AREA_2 = 2
AREA_3 = 3
AREA_4 = 4
AREA_5 = 5
AREA_6 = 6
AREA_7 = 7
AREA_8 = 8
AREA_9 = 9
AREA_10 = 10
AREA_11 = 11
AREA_12 = 12
AREA_13 = 13

RM_EASY = 1000
RM_HARD = 1001

CUSTOM_3 = 2003
CUSTOM_4 = 2004
CUSTOM_5 = 2005
CUSTOM_6 = 2006
CUSTOM_7 = 2007
CUSTOM_8 = 2008

# Device Buttons
DEVICE_PWR_BUTTON = "26"
DEVICE_HOME_BUTTON = "3"
DEVICE_BACK_BUTTON = "4"

CLOSE_HOME_OFFER = [1500, 510]


WORLD_ENERGY_MAX_ENERGY = 68
WORLD_ENERGY_MAX_MINUTES = 0
WORLD_ENERGY_MAX_SECONDS = 5
WORLD_ENERGY_CURR_ENERGY = 67
WORLD_ENERGY_CURR_MINUTES = 0
WORLD_ENERGY_CURR_SECONDS = 4

#                 AreaName   AreaId
area4 = MapArea("Area4", AREA_4)
area5 = MapArea("Area5", AREA_5)
area6 = MapArea("Area6", AREA_6)
area7 = MapArea("Area7", AREA_7)
area8 = MapArea("Area8", AREA_8)
area9 = MapArea("Area9", AREA_9)
area10 = MapArea("Area10", AREA_10)
area11 = MapArea("Area11", AREA_11)
area12 = MapArea("Area12", AREA_12)
area13 = MapArea("Area13", AREA_13)

rm_easy = MapArea("RoadmapEasy", RM_EASY)
rm_hard = MapArea("RoadmapHard", RM_HARD)

custom3 = MapArea("Custom3", CUSTOM_3)
custom4 = MapArea("Custom4", CUSTOM_4)
custom5 = MapArea("Custom5", CUSTOM_5)
custom6 = MapArea("Custom6", CUSTOM_6)
custom7 = MapArea("Custom7", CUSTOM_7)
custom8 = MapArea("Custom8", CUSTOM_8)


def init_areas_and_stages():
    area_maps = {"Area1":  MapArea("Area1", AREA_1),
                 "Area2":  MapArea("Area2", AREA_2),
                 "Area3":  MapArea("Area3", AREA_3),
                 "Area4":  MapArea("Area4", AREA_4),
                 "Area5":  MapArea("Area5", AREA_5),
                 "Area6":  MapArea("Area6", AREA_6),
                 "Area7":  MapArea("Area7", AREA_7),
                 "Area8":  MapArea("Area8", AREA_8),
                 "Area9":  MapArea("Area9", AREA_9),
                 "Area10": MapArea("Area10", AREA_10),
                 "Area11": MapArea("Area11", AREA_11),
                 "Area12": MapArea("Area12", AREA_12),
                 "Area13": MapArea("Area13", AREA_13),
                 "RoadmapEasy": MapArea("RoadmapEasy", RM_EASY),
                 "RoadmapHard": MapArea("RoadmapHard", RM_HARD),
                 "Custom3": MapArea("Custom3", CUSTOM_3),
                 "Custom4": MapArea("Custom4", CUSTOM_4),
                 "Custom5": MapArea("Custom5", CUSTOM_5),
                 "Custom6": MapArea("Custom6", CUSTOM_6),
                 "Custom7": MapArea("Custom7", CUSTOM_7),
                 "Custom8": MapArea("Custom8", CUSTOM_8)}

    return area_maps


AREA_MAPS = init_areas_and_stages()

default_area = AREA_MAPS["Area5"]
default_stage = 3
playing_area = default_area
playing_stage = default_stage
selected_area = default_area
selected_stage = default_stage



def passively_play(minutes, seconds):
    prev_device_status = "Awake"

    device = device_handler.current_device
    try:
        device.set_stay_always_on(False)
        device.minimize_screen_timeout()
        while True:
            if seconds == 0 and minutes > 0:
                seconds = 59
                minutes -= 1
            elif seconds == 0 and minutes == 0:
                pass
            else:
                seconds -= 1
            time.sleep(1)
            if minutes > 0 or seconds > 0:
                device.press_point(b.NETWORK_ERROR, b.ASYNC)

            curr_device_status = device.get_screen_status()
            if curr_device_status != prev_device_status:
                logger.log_info("Device " + prev_device_status +
                                " ----> " + curr_device_status, "RaidPlayer")
                prev_device_status = curr_device_status

            if curr_device_status == "Asleep":
                device.press_point(b.NETWORK_ERROR, b.ASYNC)
                device.unlock()
                device.press_point(b.NETWORK_ERROR, b.ASYNC)
                time.sleep(4)
                device.press_point(b.NETWORK_ERROR, b.ASYNC)
                return

    finally:
        device.set_stay_always_on(True)
        device.restore_screen_timeout()


class Activity():

    def __init__(self):
        self.priority = 10
        self.device = device_handler.current_device
        self.screenshot_folder = None
        self.max_number_of_attempts = 6

    def dev(self):
        return self.device

    def set_screenshot_folder(self, folder):
        self.screenshot_folder = folder

    def set_priority(self, prio):
        self.priority = prio

    def get_priority(self):
        return self.priority

    def go_back_and_check_we_are_home(self):
        number_of_attempts = 0
        while not img_detector.are_we_home():
            logger.log_debug("Not home, pressing back button")
            self.device.press_back_button()
            number_of_attempts += 1
            if number_of_attempts == self.max_number_of_attempts:
                break
        return number_of_attempts

    def prepare(self):
        number_of_attempts = 0        

        logger.log_info("Preparing common activity")

        # Simple recovery mode
        # Here we simply try to go back a few times before we try harder ways of revovering
        number_of_attempts = self.go_back_and_check_we_are_home()

        if number_of_attempts < self.max_number_of_attempts:
            logger.log_info("Successfully found the home screen")
            return
        logger.log_warn("Simple recovery home didn't work")

        # Complex recovery mode
        # We try here a few things
        number_of_attempts = 0
        logger.log_debug("Trying complex recovery mode")
        while not img_detector.are_we_home():
            if util.handle_exception() is True:
                return True
            number_of_attempts += 1
            if number_of_attempts == self.max_number_of_attempts:
                logger.log_warn(
                    "Something is wrong, tried to restart the game " +
                    "a few times but will need user input")
                return False
            logger.log_debug("Not Home going back and retry, attempt " +
                             str(number_of_attempts))
            msg_handler.send_remote_message(
                "Not Home going back and retry (attempt " +
                str(number_of_attempts) + ")")
            self.device.close_game()
            self.device.press_game_icon()
            time.sleep(3)
            if self.go_back_and_check_we_are_home() < self.max_number_of_attempts:
                break

        logger.log_info(
            "Successfully recovered from wrong input sequence")
        return True

    def run(self):
        # Steps to play
        pass

    def is_ready_to_run(self):
        # Check with a screenshot if the activity is ready to start
        pass


class DummyActivity(Activity):
    def __init__(self):
        self.name = "Dummy Activity"
        self.is_periodic = False

    def prepare(self):
        logger.log_info("Preparing")
        time.sleep(1)
        logger.log_info("Done")

    def is_ready_to_run(self):
        return True

    def run(self):
        logger.log_info("Running")
        time.sleep(2)
        logger.log_info("Done")


class PlayRoadmap(Activity):

    def prepare(self):
        # All the steps
        pass


class PlayWorldmap(Activity):

    def __init__(self):
        super().__init__()
        self.name = "World"
        self.device = device_handler.current_device
        self.matches_played = 0
        self.screenshot_file_name = "tmp/world_status.png"
        self.areas = {25: {5: ["20", "humans"],
                           6: ["20", "walkers"],
                           7: ["20", "humans"],
                           8: ["20", "humans"]}}
        self.world_energy = None
        self.stage_energy = 20
        self.take_screenshot = True
        self.incremental = False
        self.use_refills_per_session = 0
        self.refill_time_secs = (5 * 60) + 3
        self.period_in_secs = self.refill_time_secs * \
            45 + (random.randint(0, 45))
        self.is_periodic = True

        self.area = None
        self.stage = None
        self.energy_needed = None
        self.world_energy = None
        self.stop_playing_at_energy = None
        self.start_playing_at_energy = None

    def configure(self, energy):
        # self.config = {}
        self.area = 25
        self.stage = 7
        self.energy_needed = 20
        self.world_energy = energy
        logger.log_info("Setting energy to " +
                        str(self.world_energy.energy_left()))
        self.stop_playing_at_energy = 6
        self.start_playing_at_energy = 81

    def is_ready_to_run(self):
        # Here we check that after performing all the actions to get to the start
        # point of the activity, we get the expected image in the screenshot
        logger.log_info("Check we are in the expected area " + str(self.area))
        world_area = img_detector.scan_world_area()
        if world_area is not None and world_area == self.area:
            logger.log_pedantic("We are in area " + str(self.area))
            return True
        else:
            logger.log_pedantic("We are NOT in area " + str(self.area))
            return False

    def prepare(self):
        tag = self.name

        # This will go back home
        super().prepare()

        logger.log_debug("Pressing battle button", tag)
        self.device.press_point(b.BATTLE_BUTTON)
        time.sleep(1)

        logger.log_debug("Pressing world button", tag)
        self.device.press_point(b.WORLD_BUTTON)
        time.sleep(1)

        # TODO here depending on the area we might have to move to the different
        # parts of the world ...
        #    self.go_to_woodbury()
        #    self.go_to_selected_area(13)

        area_image = img_detector.search_world_area(self.area)

        if area_image is not None:
            self.device.press_point(area_image[0].randomise())

        self.is_ready_to_run()

    def run(self):

        # TODO this can be improved reusing the screenshot before in ready_to_run
        self.device.capture_screenshot(
            "tmp/world_area_screenshot_for_energy.png")
        value = img_detector.get_world_energy(
            "tmp/world_area_screenshot_for_energy.png")
        logger.log_info("Value scanned is " + str(value))
        if value != -1 and value <= 92:
            self.world_energy.set_energy(value)

        while self.world_energy.energy_left() > self.energy_needed and self.world_energy.energy_left() > self.stop_playing_at_energy:

            logger.log_verbose("Ok to war has ended")
            # self.device.press_point(b.OK_TO_WAR_RESULT)
            time.sleep(1)

            logger.log_verbose("Checking if we were raided")
            self.device.press_point(b.IGNORE_RAIDED)
            time.sleep(1)

            logger.log_info("Playing " + str(self.area) +
                            " - Stage " + str(self.stage))

            # TODO here somehow we need to have access to the coordinates of the
            # area/stage we want to play
            stages = img_detector.scan_stages_and_energy([19, 20])

            self.device.press_point([stages[5].x, stages[5].y])
            time.sleep(1)

            # We need a way to figure out how to do this and skip
            logger.log_verbose(
                "Fight anyway if roster of weapon inventory full")
            self.device.press_point(b.FIGHT_ANYWAY)
            time.sleep(1)

            logger.log_verbose("Team select")
            self.device.press_point(b.TEAM_SELECT)
            time.sleep(1)

            # if (args.incremental):
            logger.log_verbose("Skip comics")
            self.device.press_point(b.SKIP_COMICS)
            time.sleep(1)

            # fs = -1
            # supp_string = "Faction supporter is " + str(args.supporter)
            # logger.log_info(supp_string)
            # if args.supporter == True:
            #    fs = faction_supporter.pickup_first_available_faction_supporter()
            #    logger.log_info("Faction supporter " + str(fs) +
            #                    " will be used", component)
            #
            # if fs != -1:
            #    logger.log_info("Slowly scrolling supporter", component)
            #    add_faction_supporter(fs)

            logger.log_info("Now fighting using " + str(self.stage_energy) +
                            "/" + str(self.world_energy.energy_left()) + " energy")
            self.device.press_point(b.FIGHT)

            # if fs != -1:
            #    faction_supporter.choose_supporter_now(fs)
            #    faction_supporter.flush_supporters()

            self.world_energy.use_energy(self.stage_energy)
            self.world_energy.energy_full.set()
            time.sleep(2)

            logger.log_info("Pressinng Autoplay, playing now...")
            self.device.press_point(b.INFIGHT_AUTOPLAY)
            # wait_t = WAIT_TIME + random.randrange(1, 5)
            passively_play(0, 40)

            logger.log_verbose("Closing multipe survivors found")
            os.system("adb shell input tap 1120 40")  # Tekstor
            time.sleep(1)

            if self.take_screenshot:
                logger.log_verbose("Searching for next button")
                screenshot_file = "tmp/tmp_area_screenshot.png"
                self.device.capture_screenshot(screenshot_file)
                time.sleep(2)
                rect = img_detector.scan_next_button(screenshot_file, 0.98)

                if len(rect) > 0:
                    logger.log_verbose("Pressing next")
                    self.device.press_point(rect[0].randomise())
                else:
                    logger.log_error(
                        "Expected (next) button not found!")
                    logger.log_info(
                        "Sending screenshot to check what is happening")
                    msg_handler.capture_compress_and_send_screenshot()
                    util.handle_exception()
                    # screenshot_data[0]=area.area_name
                    # screenshot_data[1]=stage
                    # screenshot_data[2]=match
                    # msg_handler.compress_and_send_screenshot(os.path.join(
                    #   "../..", "WalkingDeadCtl", screenshot_file))
                    # msg_handler.send_remote_message(
                    #   "I didn't get what I expected: next button")
                    # Here you can send a screenshot of what you got
            else:
                self.device.press_point(b.ENDFIGHT_NEXT)

            # if args.exit:
            #    return

            if (self.incremental):
                logger.log_pedantic("Skip comics")
                self.device.press_point(b.SKIP_COMICS)
                time.sleep(1)

            if (self.incremental):
                logger.log_pedantic("Claim rewards")
                self.device.press_point(b.ENDFIGHT_CLAIM_REWARDS)
                time.sleep(5)

            logger.log_verbose("Ok to war has ended")
            # self.device.press_point(b.OK_TO_WAR_RESULT)
            time.sleep(1)

            # logger.log_info("Checking if we were raided")
            # self.device.press_point(b.IGNORE_RAIDED)
            # time.sleep(1)

            logger.log_verbose("Ignoring resources/roaster full message")
            self.device.press_point(b.ROASTER_FULL)
            time.sleep(1)

            self.device.capture_screenshot(
                "tmp/world_area_screenshot_for_energy.png")
            value = img_detector.get_world_energy(
                "tmp/world_area_screenshot_for_energy.png")
            logger.log_info("Value scanned is " + str(value))
            if value != -1 and value <= 92:
                self.world_energy.set_energy(value)

        logger.log_info("Not enough energy to continue playing (left " + str(
            self.world_energy.energy_left()) + "/92 needed " + str(self.energy_needed) + ")")

        if self.use_refills_per_session == 0:
            logger.log_verbose("Use refill per session set to " +
                               str(self.use_refills_per_session))
            return
        else:
            logger.log_verbose("Use refill per session set to " +
                               str(self.use_refills_per_session) + " now using one")
            # Here the code to refill

        logger.log_info("Finished playing WorldMap session")

    def move_to(self, position):

        coords = map_positions[position]
        device.swipe(coords[0], coords[1], coords[2], coords[3], 500)

    def go_to_selected_area(self, area):
        position = area.get_location()
        # Make area 5 visible
        self.move_to(position[0])
        msg_handler.send_remote_message(
            "The next screenshot should be the selected area")
        logger.log_info("Select " + str(area), "Player")
        device.press_point(position[1])
        time.sleep(3)
        # TODO: here we need to take the screenshots

    def go_to_woodbury(self):

        global playing_area
        global playing_stage

        logger.log_info("Press battle button", "Player")
        device.press_point(b.BATTLE_BUTTON)
        time.sleep(3)

        logger.log_info("Press world button", "Player")
        device.press_point(b.WORLD_BUTTON)
        time.sleep(3)

        # Make woodbury button visible swiping from almost top left corner to bottom right
        self.device.swipe(250, 250, 1910, 1050, 200)
        time.sleep(1)

        logger.log_info("Go to woodbury map", "Player")
        self.device.press_point(b.GO_TO_WOODBURY)
        # os.system("adb shell input tap 280 560")
        time.sleep(2)

        logger.log_info("Select area 13", "Player")
        self.device.press_point(b.AREA_13_POSITION)
        time.sleep(2)
        # os.system("adb shell input tap 760 300")

        area_num = 13
        area_str = "Area" + str(area_num)
        playing_area = AREA_MAPS[area_str]
        playing_stage = 4
        time.sleep(3)


class PlayBootcamp(Activity):

    def __init__(self):
        super().__init__()
        self.name = "BootCamp player"
        self.device = device_handler.current_device
        self.screenshot_file_name = "tmp/bootcamp_status.png"
        self.play_continuosly = False
        self.world_energy = None
        self.stage_energy = 4
        self.take_screenshot = True
        self.period_in_secs = 60 * 60
        self.is_periodic = True
        self.matches_played_in_session = 0
        self.max_matches_per_session = 80

    def configure(self, energy):
        self.energy_needed = 4
        self.world_energy = energy
        logger.log_info("Setting energy to " +
                        str(self.world_energy.energy_left()))
        self.stop_playing_at_energy = 6
        self.start_playing_at_energy = 81

    def prepare(self):

        # This will go back home
        super().prepare()
        
        logger.log_debug("Pressing battle button")
        self.device.press_point(b.BATTLE_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting roadmap")
        self.device.press_point(b.ROADMAP_BUTTON)
        time.sleep(1)

        logger.log_debug("Moving towards the bootcamp map")
        self.device.swipe(1200, 300, 300, 800, 200)
        self.device.swipe(1200, 300, 300, 800, 200)
        time.sleep(1)


    def is_ready_to_run(self):

        # Here we check that after performing all the actions to get to the start
        # point of the activity, we get the expected image in the screenshot
        logger.log_info("Checking we have the Bootcamp stage")
        self.device.capture_screenshot(self.screenshot_file_name)
        try:
            bootcamp_button = img_detector.scan_find_bootcamp_button(
                self.screenshot_file_name)
        except Exception:
            logger.log_error("Failed to scan bootcamp button")
        if bootcamp_button is not None:
            logger.log_pedantic("Bootcamp button found")
        else:
            logger.log_pedantic("Bootcamp button not found")
            return False

        logger.log_debug("Going to bootcamp")
        self.device.press_point([bootcamp_button.x, bootcamp_button.y])

        logger.log_debug("Select middle match")
        self.device.press_point([980, 850])

        return True

    def check_conditions(self):
        if self.matches_played_in_session < self.max_matches_per_session:
            return True
        return False

    def run(self):

        while self.check_conditions():
            logger.log_debug("Select a round")
            self.device.press_point([1670, 900])

            logger.log_verbose(
                "Fight anyway if roster of weapon inventory full")

            self.device.press_point(b.FIGHT_ANYWAY)

            time.sleep(1)

            logger.log_verbose("Team select")
            self.device.press_point(b.TEAM_SELECT)
            time.sleep(1)

            logger.log_info("Now fighting using " + str(self.stage_energy) +
                            "/" + str(self.world_energy.energy_left()) + " energy")

            self.device.press_point(b.FIGHT)

            time.sleep(1)

            refill_button = img_detector.is_refill_needed()
            if refill_button is not None:
                self.device.press_point([refill_button.x, refill_button.y])
                time.sleep(1)
                self.device.press_point(b.FIGHT)
                self.world_energy.use_refill()

            logger.log_info("Pressinng Autoplay, playing now...")

            self.device.press_point(b.INFIGHT_AUTOPLAY)
            self.world_energy.use_energy(self.stage_energy)
            self.world_energy.energy_full.set()
            passively_play(0, 10)

            logger.log_pedantic("Closing multipe survivors found")
            os.system("adb shell input tap 1120 40")  # Tekstor
            time.sleep(1)

            if self.take_screenshot:
                logger.log_pedantic("Searching for next button")
                screenshot_file = "tmp/tmp_area_screenshot.png"
                self.device.capture_screenshot(screenshot_file)
                time.sleep(2)
                rect = img_detector.scan_next_button(screenshot_file, 0.98)
                msg_handler.compress_and_send_screenshot(screenshot_file)

                if len(rect) > 0:
                    logger.log_verbose("Pressing next")
                    self.device.press_point(rect[0].randomise())
                else:
                    logger.log_error("Expected (next) button not found!")
                    raise Exception("Expected (next) button not found!")
                    # screenshot_data[0]=area.area_name
                    # screenshot_data[1]=stage
                    # screenshot_data[2]=match
                    # msg_handler.compress_and_send_screenshot(os.path.join(
                    #   "../..", "WalkingDeadCtl", screenshot_file))
                    # msg_handler.send_remote_message(
                    #   "I didn't get what I expected: next button")
                    # Here you can send a screenshot of what you got
            else:
                self.device.press_point(b.ENDFIGHT_NEXT)

            self.matches_played_in_session += 1
            logger.log_info("Bootcamp matches played = " +
                            str(self.matches_played_in_session))


class PlayRaid(Activity):

    def __init__(self):
        super().__init__()
        self.name = "Raid"
        self.matches_played = 0
        self.matches_won = 0
        self.screenshot_file_name = "tmp/raid_status.png"
        self.play_continuosly = True
        self.refill_time_secs = 45 * 60
        self.period_in_secs = self.refill_time_secs * 5
        self.is_periodic = True
        self.device = device_handler.current_device

    def configure(self, config_params):
        self.play_continuosly = config_params["raid_tournament"]
        logger.log_info(
            "Configuring to play tournament mode = " + str(self.play_continuosly))

    def is_ready_to_run(self):
        # Here we check that after performing all the actions to get to the start
        # point of the activity, we get the expected image in the screenshot
        logger.log_info("Checking we have the find opponent button visible")
        self.device.capture_screenshot(self.screenshot_file_name)
        if img_detector.scan_find_opponent_button(self.screenshot_file_name) is not None:
            logger.log_pedantic("FindOpponent button found")
            return True
        logger.log_pedantic("FindOpponent button not found")
        return False

    def prepare(self):
        # This will go back home
        super().prepare()

        logger.log_debug("Pressing versus button")
        self.device.press_point(b.VERSUS_BUTTON)
        time.sleep(1)

        logger.log_debug("Pressing raid button")
        self.device.press_point(b.RAID_BUTTON)
        time.sleep(1)

        self.is_ready_to_run()

    def run(self):

        tag = self.name

        while True:

            logger.log_info("Analysing opponent")
            energy_left = img_detector.get_raid_energy_value()

            if energy.timer["raid"].energy_left() != energy_left and energy_left != -1:
                energy.timer["raid"].set_energy(energy_left)

            if (energy.timer["raid"].energy_left() == 0) and (self.play_continuosly is not True):
                logger.log_info("Raid energy over, ending play session")
                return

            battery_level = self.device.get_battery_level()
            logger.log_debug("Battery level: " + str(battery_level))
            if (energy.timer["raid"].energy_left() == 0) and battery_level <= 20:
                logger.log_info("Battery level critical, going to sleep")
                return

            self.device.press_point(b.FIND_OPPONENT)
            time.sleep(10)

            now = datetime.datetime.now()

            time_stamp = now.strftime("%Y-%m-%d_%H-%M-%S")
            # raid_opponent_file_name = str(time_stamp) + "-match_" + str(matches) + "-raid-opponent-0.png"

            candidate_count = 0
            find_opponent_attempt = 0
            opponent_found = False
            while find_opponent_attempt < 5:
                time.sleep(5)
                # TODO try:
                raid_opponent_file_name = self.device.capture_screenshot(self.screenshot_folder + "/raid-opponent-candidate_" + str(
                    candidate_count) + "_match" + str(self.matches_played) + "_" + str(time_stamp))
                reputation = img_detector.get_reputation(
                    raid_opponent_file_name)
                if reputation == -1:
                    logger.log_warn("Reputation value not scanned correctly")
                    # self.device.press_point(b.FIND_NEW_OPPONENT)
                    # time.sleep(5)
                    find_opponent_attempt += 1
                    continue
                if reputation < 25000:
                    logger.log_info("Selected opponent with " +
                                    str(reputation) + " reputation")
                    raid_opponent_file_name = self.device.capture_screenshot(
                        self.screenshot_folder + "/raid-opponent-selected_" + str(
                        candidate_count) + "_match" + str(self.matches_played) + 
                        "_" + str(time_stamp))
                    opponent_found = True
                    break
                else:
                    logger.log_info("Skipping opponent with " +
                                    str(reputation) + " reputation")
                    self.device.press_point(b.FIND_NEW_OPPONENT)
                    find_opponent_attempt = 0
                    candidate_count += 1
                    continue

            if not opponent_found:
                logger.log_error(
                    "Struggling to find an opponent, something might have gone wrong")
                return

            if energy.timer["raid"].energy_left() == 0 and self.play_continuosly:
                logger.log_info(
                    "Energy is up, attack an opponent and ...")
                self.device.press_point(b.ATTACK_OPPONENT)
                time.sleep(1)

                logger.log_info("... refill")
                self.device.press_point(b.USE_REFILL)
                time.sleep(1)

                energy.timer["raid"].use_refill()

            start_time = time.time()

            logger.log_info("Attack opponent")
            self.device.press_point(b.ATTACK_OPPONENT)
            time.sleep(2)

            logger.log_verbose("Raid anyway")
            self.device.press_point(b.RAID_ANYWAY)
            time.sleep(1)

            energy.timer["raid"].use_energy(1)
            energy.timer["raid"].energy_full.set()

            logger.log_info("Press Autoplay")
            self.device.press_point(b.INFIGHT_AUTOPLAY)

            wait_minutes = 0
            wait_seconds = 10
            passively_play(wait_minutes, wait_seconds)

            duration = time.time() - start_time
            logger.log_info("Total duration of the match was " +
                            str(int(duration)) + " seconds")

            logger.log_verbose("Checking result")
            raid_result_file_name = self.device.capture_screenshot(self.screenshot_folder + "/raid-result_" + str(
                candidate_count) + "_match" + str(self.matches_played) + "_" + str(time_stamp))
            result = img_detector.get_raid_result(raid_result_file_name)
            self.matches_played += 1
            if result == "WINNER":
                self.matches_won += 1
                played = self.matches_played
                won = self.matches_won
                lost = played - won
                logger.log_info(
                    "Raid result ================================        WINNER (" + str(won) + "/" + str(played) + ")", tag)
                self.device.press_point(b.FINISH_RAID)
                time.sleep(1)
            elif result == "DEFEATED":
                played = self.matches_played
                won = self.matches_won
                lost = played - won
                logger.log_info(
                    "Raid result ================================        DEFEATED (" + str(lost) + "/" + str(played) + ")", tag)
                self.device.press_point(b.FINISH_RAID_FAIL)
                time.sleep(1)
            else:
                logger.log_error("Something wrong getting raid result", tag)
                msg_handler.capture_compress_and_send_screenshot()

            stats_handler.update_data(now.strftime("%Y-%m-%d"), now.strftime("%H-%M-%S"), "raid",
                                      "", "", str(int(duration)), "won", raid_opponent_file_name, raid_result_file_name)

            # if args.exit:
            #    return

            logger.log_info("Ok to out of storage", tag)
            self.device.press_point(b.CLOSE_OUT_OF_STORAGE)
            time.sleep(1)

            logger.log_info("Back to raid tab", tag)
            self.device.press_point(b.RAID_TAB)
            time.sleep(1)


class PlayRoadToSurvivalDaily(Activity):

    def __init__(self):
        super().__init__()
        self.name = "RTS Daily"
        self.matches_played = 0
        self.reset_time = 0
        self.is_periodic = False
        self.consecutive_failures = 0
        self.stages_completed = 0
        self.screenshot_file_name = "tmp/rtsd_status.png"
        self.device = device_handler.current_device
        self.is_locked = False

    def reset(self):
        self.matches_played = 0
        self.reset_time = 0
        self.consecutive_failures = 0
        self.stages_completed = 0

    def prepare(self):

        tag = self.name

        super().prepare()

        logger.log_debug("Pressing battle button", tag)
        self.device.press_point(b.BATTLE_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting road to survival", tag)
        self.device.press_point(b.RTS_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting road to survival daily", tag)
        self.device.press_point(b.RTS_SELECT_DAILY)
        time.sleep(1)

        logger.log_pedantic("Complete the level", tag)
        self.device.press_point(b.RTS_DAILY_COMPLETE_THE_LVL)
        time.sleep(1)

        logger.log_pedantic("Close possible rewards window", tag)
        self.device.press_point(b.RTS_DAILY_CLOSE_REWARDS)
        time.sleep(1)

        if img_detector.rtsd_locked():
            logger.log_info("Road to Survival Daily already completed")
            msg_handler.send_remote_message("Road to Survival Daily already completed")
            self.is_locked = True
            raise Exception("Road to Survival Daily already completed")

        logger.log_debug("Go to stages", tag)
        self.device.press_point(b.RTS_DAILY_GO_TO_STAGES)
        time.sleep(4)

        return self.is_ready_to_run()

    def is_ready_to_run(self):

        if self.is_locked is True:
            return False
        # Here we check that after performing all the actions to get to the start
        # point of the activity, we get the expected image in the screenshot
        logger.log_info("Checking we have the attack button visible")
        self.device.capture_screenshot(self.screenshot_file_name)
        if img_detector.scan_attack_button_rtsd(self.screenshot_file_name) is False:
            logger.log_pedantic("The bloody image is NOT there")
            return False
        else:
            logger.log_pedantic("The bloody image is there")
            return True

    def go_back_and_in_stages(self):
        tag = self.name

        logger.log_debug("Back to briefing", tag)
        self.device.press_point(b.RTS_BACKTO_BRIEFING)
        time.sleep(3)

        logger.log_debug("Go to stages", tag)
        self.device.press_point(b.RTS_DAILY_GO_TO_STAGES)
        time.sleep(3)

    def run(self):

        tag = self.name

        if self.is_locked:
            logger.log_info("Daily session completed, skipping")
            return True

        while self.stages_completed != 10 :

            enemies_list = img_detector.scan_enemy_types_in_rts(
                self.screenshot_file_name, 0.97)
            if len(enemies_list) == 0:
                logger.log_warn(
                    "PlayRoadToSurvivalDaily: enemy scan didn't report any enemy")
                # raise Exception("PlayRoadToSurvivalDaily: enemy scan didn't report any enemy")

            logger.log_info("Found following enemies: " +
                            str(enemies_list))

            if not img_detector.scan_attack_button_rtsd(self.screenshot_file_name):
                raise Exception(
                    "PlayRoadToSurvivalDaily: attack button not found")

            logger.log_debug("Autofill")
            self.device.press_point(b.RTS_DAILY_AUTOFILL)
            time.sleep(1)

            # if args.exit:
            #     logger.log_info("User requested exit from Road to survival daily")
            #     return 1

            logger.log_debug("Attack")
            self.device.press_point(b.RTS_ATTACK)
            time.sleep(2)

            logger.log_debug("Autoplay")
            self.device.press_point(b.RTS_AUTOPLAY)
            time.sleep(2)

            logger.log_debug("Fighting!")
            wait_minutes = 0
            wait_seconds = 10
            passively_play(wait_minutes, wait_seconds)

            logger.log_debug("Stage completed")

            # Here we check that after performing all the actions to get to the start
            # point of the activity, we get the expected image in the screenshot
            logger.log_debug("Checking we have the next button visible")
            self.device.capture_screenshot(self.screenshot_file_name)
            next_button = img_detector.scan_next_button_rts(
                self.screenshot_file_name, 0.95)

            if next_button is not None:
                self.stages_completed += 1
                logger.log_debug("Pressing next")
                self.device.press_point(next_button.randomise())
                self.consecutive_failures = 0
                time.sleep(4)

                logger.log_pedantic("Close message insufficient capacity")
                self.device.press_point(b.CLOSE_INSUFFICIENT_CAPACITY)
                time.sleep(2)

                logger.log_debug("Get crate")
                self.device.press_point(b.RTS_GET_CARTE)
                time.sleep(1)

                self.device.press_point(b.RTS_GET_CARTE)
                time.sleep(2)

                logger.log_debug("Claim carte")
                self.device.press_point(b.RTS_CLAIM_CARTE)
                time.sleep(2)

                if img_detector.rtsd_completed():
                    logger.log_info("Road to survival daily completed!")
                    return "COMPLETED"

            elif img_detector.are_all_fighters_dead(self.screenshot_file_name):
                msg_handler.send_remote_message(
                    "All fighters dead in Road to Survival daily against: " + str(enemies_list))
                msg_handler.capture_compress_and_send_screenshot()
                logger.log_info("All fighters dead")
                self.consecutive_failures += 1
                self.go_back_and_in_stages()

            else:
                raise Exception(
                    "PlayRoadToSurvivalDaily: expected next button or all fighters dead")

            # if args.exit:
            #     logger.log_info("User requested exit from Road to survival daily", tag)
            #     return 1

            if self.consecutive_failures == 4:
                logger.log_info(
                    "Too many consecutive dead, please do something", tag)
                return "IMPOSSIBLE"

            self.device.capture_screenshot(self.screenshot_file_name)

    def configure(self, config_file):
        # Read a configuration file that tells what to play and how
        pass


class PlayRoadToSurvivalTournament(Activity):

    def __init__(self):
        super().__init__()
        self.name = "RTS Tournament"
        self.matches_played = 0
        self.reset_time = 0
        self.is_periodic = True
        self.refill_time_secs = 15 * 65
        self.period_in_secs = self.refill_time_secs * 7
        self.consecutive_failures = 0
        self.stages_completed = 0
        self.screenshot_file_name = "tmp/rtst_status.png"
        self.device = device_handler.current_device

    def reset(self):
        self.matches_played = 0
        self.reset_time = 0
        self.consecutive_failures = 0
        self.stages_completed = 0

    def prepare(self):

        tag = self.name

        super().prepare()

        logger.log_debug("Pressing battle button", tag)
        self.device.press_point(b.BATTLE_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting road to survival", tag)
        self.device.press_point(b.RTS_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting road to survival tournament", tag)
        self.device.press_point(b.RTS_SELECT_TOURNAMENT)
        time.sleep(1)

        logger.log_debug("Go to stages", tag)
        self.device.press_point(b.RTS_DAILY_GO_TO_STAGES)
        time.sleep(4)

        self.is_ready_to_run()

    def is_ready_to_run(self):
        # Here we check that after performing all the actions to get to the start
        # point of the activity, we get the expected image in the screenshot
        logger.log_info("Checking we have the attack button visible")
        self.device.capture_screenshot(self.screenshot_file_name)
        if img_detector.scan_attack_button_rtst(self.screenshot_file_name) is False:
            logger.log_info("Attack button not found")
            return False
        else:
            logger.log_pedantic("Found attack button")
            return True

    def go_back_and_in_stages(self):
        tag = self.name

        logger.log_debug("Back to briefing", tag)
        self.device.press_point(b.RTS_BACKTO_BRIEFING)
        time.sleep(3)

        logger.log_debug("Go to stages", tag)
        self.device.press_point(b.RTS_DAILY_GO_TO_STAGES)
        time.sleep(3)

    def run(self):

        tag = self.name

        while(True):

            energy = img_detector.get_rtst_energy_value()
            if energy == 0:
                logger.log_debug(
                    "Energy over going to pass for other activities")
                return

            enemies_list = img_detector.scan_enemy_types_in_rts(
                self.screenshot_file_name, 0.97)
            if len(enemies_list) == 0:
                logger.log_warn(
                    "Enemy scan didn't report any enemy")
                # raise Exception("PlayRoadToSurvivalDaily: enemy scan didn't report any enemy")

            logger.log_info("Found following enemies: " +
                            str(enemies_list))

            if not img_detector.scan_attack_button_rtst(self.screenshot_file_name):
                raise Exception(
                    "PlayRoadToSurvivalTournament: Attack button not found")

            logger.log_debug("Autofill")
            self.device.press_point(b.RTS_DAILY_AUTOFILL)
            time.sleep(1)

            # if args.exit:
            #     logger.log_info("User requested exit from Road to survival daily")
            #     return 1

            logger.log_debug("Attack")
            self.device.press_point(b.RTS_ATTACK)
            time.sleep(2)

            logger.log_debug("Autoplay")
            self.device.press_point(b.RTS_AUTOPLAY)
            time.sleep(2)

            logger.log_debug("Fighting!")
            wait_minutes = 0
            wait_seconds = 10
            passively_play(wait_minutes, wait_seconds)

            logger.log_debug("Stage completed")

            # Here we check that after performing all the actions to get to the start
            # point of the activity, we get the expected image in the screenshot
            logger.log_debug("Checking we have the next button visible")
            self.device.capture_screenshot(self.screenshot_file_name)
            next_button = img_detector.scan_next_button_rts(
                self.screenshot_file_name, 0.95)

            if next_button is not None:
                self.stages_completed += 1
                logger.log_debug("Pressing next")
                self.device.press_point(next_button.randomise())
                self.consecutive_failures = 0
                time.sleep(4)

                logger.log_pedantic("Close message insufficient capacity")
                self.device.press_point(b.CLOSE_INSUFFICIENT_CAPACITY)
                time.sleep(2)

                logger.log_debug("Get crate")
                self.device.press_point(b.RTS_GET_CARTE)
                time.sleep(1)

                self.device.press_point(b.RTS_GET_CARTE)
                time.sleep(2)

                logger.log_debug("Claim carte")
                self.device.press_point(b.RTS_CLAIM_CARTE)
                time.sleep(2)

                if img_detector.rtsd_completed():
                    logger.log_info("Road to survival daily completed!")
                    return "COMPLETED"

            elif img_detector.are_all_fighters_dead(self.screenshot_file_name):
                msg_handler.send_remote_message(
                    "All fighters dead in Road to Survival daily against: " + str(enemies_list))
                msg_handler.capture_compress_and_send_screenshot()
                logger.log_info("All fighters dead")
                self.consecutive_failures += 1
                self.go_back_and_in_stages()

            else:
                raise Exception(
                    "PlayRoadToSurvivalDaily: expected next button or all fighters dead")

            # if args.exit:
            #     logger.log_info("User requested exit from Road to survival daily", tag)
            #     return 1

            if self.consecutive_failures == 4:
                logger.log_info(
                    "Too many consecutive dead, please do something", tag)
                return "IMPOSSIBLE"

            self.device.capture_screenshot(self.screenshot_file_name)

    def configure(self, config_file):
        # Read a configuration file that tells what to play and how
        pass


class ScanRoadmapForStages(Activity):
    def __init__(self):
        self.screenshot_file_name = "tmp/roadmap_status.png"
        self.name = "Roadmap Finder"

    def prepare(self):
        tag = self.name
        device = device_handler.current_device

        logger.log_debug("Pressing battle button", tag)
        device.press_point(b.BATTLE_BUTTON)
        time.sleep(1)

        logger.log_debug("Selecting road to survival", tag)
        device.press_point(b.ROADMAP_BUTTON)
        time.sleep(1)

    def go_to_quadrant(self, quadrant):
        device = device_handler.current_device
        quadrant_coords = {1: [[300, 300], [1900, 1000]], 2: [[1450, 300], [
            1, 1000]], 3: [[1600, 700], [1, 1]], 4: [[300, 700], [1900, 1]]}
        start_point = quadrant_coords[quadrant][0]
        end_point = quadrant_coords[quadrant][1]
        device.swipe(start_point[0], start_point[1],
                     end_point[0], end_point[1], 500)
        device.swipe(start_point[0], start_point[1],
                     end_point[0], end_point[1], 500)

    def run(self):
        device = device_handler.current_device

        for quadrant in [1, 2, 3, 4]:
            self.go_to_quadrant(quadrant)
            #file_name = "data/images/Nexus/roadmap/roadmap_area_button_border.png"
            device.capture_screenshot(self.screenshot_file_name)
            result = img_detector.detect_playable_roadmap_areas(
                self.screenshot_file_name)
            if result is None:
                logger.log_info(
                    "Didn't find any playable maps in quadrant " + str(quadrant), "Roadamp player")
            else:
                for key in result.keys():
                    logger.log_info(
                        "Found " + key + " in quadrant " + str(quadrant), "Roadamp player")
                    result[key] = [quadrant, result[key]]
