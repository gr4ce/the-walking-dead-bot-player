""" Scheduler module: used to schedule activities and model the activity containers
"""
import time
import inspect
import datetime
import threading
import traceback

from multiprocessing import Event

import lib.util as util
import lib.logger as logger
import lib.msg_handler as msg_handler
import lib.energy_handler as energy
import lib.parameters as parameters
import lib.device_handler as device_handler



class ScheduledActivity():

    def __init__(self):
        self.activity = None
        self.trigger_time = None

    def set_activity(self, activity):
        self.activity = activity

    def get_activity(self):
        return self.activity

    def get_trigger_time(self):
        return self.trigger_time

    def set_trigger_time(self, trigger_time):
        self.trigger_time = trigger_time


class ActivityScheduler():

    def __init__(self, activityTrigger):
        self.running = None
        self.ready_queue = []
        self.activity_trigger = activityTrigger
        self.activity_ready = Event()
        self.activity_ready.clear()

        self.activity_ready_condition = threading.Condition()
        self.activity_completed = Event()
        self.executor_terminated = Event()
        self.going_idle = Event()
        self.executor_terminated.clear()
        self.terminate_executor = False
        self.executor_done = False
        self.device = device_handler.get_device()
        self.timer_thread = threading.Thread(
            name="ActivitySchedulerThread",
            target=self.activity_executor)
        self.timer_thread.start()

    def terminate(self, wait=False):
        try:
            self.activity_ready_condition.acquire()
            logger.log_info("Requested scheduler termination")
            self.terminate_executor = True
            #self.activity_ready.set()           
            self.activity_ready_condition.notify()
        finally:
            self.activity_ready_condition.release()

        if wait:
            logger.log_info("Waiting executor to complete")
            #self.executor_terminated.wait()
            if self.timer_thread.is_alive():
                self.timer_thread.join()


    def is_idle(self):
        return not (self.running or len(self.ready_queue) > 0)

    def activity_executor(self):
        logger.log_debug("Getting into the executor")
        while not self.terminate_executor:

            logger.log_debug("Check if an activity is ready")
            try:
                self.activity_ready_condition.acquire()
                if self.running is None:
                    if self.terminate_executor:
                        break
                    logger.log_info(
                        "No activity ready, locking the device")
                    self.device.set_stay_always_on(False)
                    self.device.minimize_screen_timeout()
                    #self.device.lock()
                    logger.log_info("The scheduler locking semaphor is locked = " + str(self.activity_ready.is_set()))
                    #self.activity_ready.clear()
                    #self.activity_ready.wait()
                    self.activity_ready_condition.wait()
                    if self.terminate_executor:
                        break
                    assert self.running is not None
            finally:
                self.activity_ready_condition.release()

            # try:
            activity = self.running.get_activity()
            logger.log_debug("Activity " + activity.name + " triggered")
            msg_handler.send_remote_message("Activity " + activity.name + " triggered")

            self.device.unlock()

            if activity.name == "BootCamp player" or not activity.is_ready_to_run():
                try:
                    logger.log_debug("Activity " + activity.name + " in preparation")
                    msg_handler.send_remote_message("Activity " + activity.name + " in preparation")
                    activity.prepare()
                except Exception as prepare_exception:
                    logger.log_debug(
                        "Activity " + activity.name + " got an exception during preparation")
                    msg_handler.send_remote_message(
                        "Activity " + activity.name + " got an exception during preparation")
                    logger.log_error(str(prepare_exception))
                    util.handle_exception()

            if activity.is_ready_to_run():
                try:
                    logger.log_info("Activity " + activity.name + " ready and running")
                    msg_handler.send_remote_message(
                        "Activity " + activity.name + " ready and running")
                    activity.run()
                    logger.log_debug("Activity " + activity.name + " completed successfully")
                    msg_handler.send_remote_message(
                        "Activity " + activity.name + " completed successfully")
                except Exception as run_exception:
                    logger.log_debug(
                        "Activity " + activity.name + " got an exception")
                    traceback.print_exc()
                    logger.log_error(str(run_exception))
                    util.handle_exception()    
                
            else:
                logger.log_debug("Activity " + activity.name + " skipped")
                msg_handler.send_remote_message("Activity " + activity.name + " completed")

            if activity.is_periodic is True:
                self.activity_trigger.arm(
                    self.running, activity.period_in_secs)

                future_time = str(datetime.datetime.now(
                ) + datetime.timedelta(0, activity.period_in_secs))[:16]
                logger.log_info("Rescheduling " +
                                activity.name + " at " + future_time)
                msg_handler.send_remote_message("Rescheduling " + 
                                                activity.name + " at " + future_time)
                self.activity_trigger.print_queue_content()

            self.running = None
            #logger.log_pedantic("Print queue content again")
            #self.activity_trigger.print_queue_content()

            if len(self.ready_queue) > 0:
                self.running = self.ready_queue.pop(0)
            # except Exception as e:
            #    logger.log_error(str(e))

        self.executor_done = True
        logger.log_debug("Executor terminated!")
        self.executor_terminated.set()

    def ready_to_run(self, activity):
        try:
            self.activity_ready_condition.acquire()
            logger.log_pedantic("Scheduling activity ")
            if self.running is None:
                logger.log_info("Running activity")
                self.running = activity
                self.activity_ready_condition.notify()
            else:
                logger.log_pedantic("Queuing activity")
                self.ready_queue.append(activity)
        finally:
            self.activity_ready_condition.release()


class ActivityTrigger():

    def __init__(self):
        """Initialise the ActivityTrigger"""
        logger.log_pedantic("Initialising")
        self.sleep_queue = []
        self.triggered_queue = []
        self.trigger_timer = None
        self.terminating = False
        self.terminated = False
        self.critical = threading.Lock()
        self.scheduler = ActivityScheduler(self)

        # Initialising timers for various energy
        self.raid_timer = energy.timer["raid"]
        self.world_timer = energy.timer["world"]
        self.rtsd_timer = energy.timer["rts"]

    def lock(self):
        logger.log_pedantic(str(inspect.stack()[1][3]) + " is locking")
        self.critical.acquire()

    def unlock(self):
        logger.log_pedantic(str(inspect.stack()[1][3]) + " is unlocking")
        self.critical.release()

    def fire(self):
        self.lock()
        logger.log_debug("Fireing event from top of the queue " +
                         str(self.sleep_queue[0].get_trigger_time()))

        head = self.sleep_queue.pop(0)
        self.trigger_timer.cancel()

        # If there is at least an element we should trigger the top of the queue
        if len(self.sleep_queue) > 0:
            logger.log_debug("Queue not empty arming next activity " +
                             self.sleep_queue[0].get_activity().name  + " at " +
                             str(self.sleep_queue[0].get_trigger_time()))
            trigger_secs = (self.sleep_queue[0].get_trigger_time() - datetime.datetime.now()).seconds
            self.trigger_timer = threading.Timer(trigger_secs, self.fire)
            self.trigger_timer.start()
        else:
            logger.log_debug("Queue empty, nothing to trigger")

        # Then we finally add the activity to the scheduler to run or to sleep
        self.scheduler.ready_to_run(head)
        self.unlock()

    def terminate(self, wait=True):

        try:
            self.lock()
            logger.log_debug("Terminating activity trigger")
            self.terminating = True

            if len(self.sleep_queue) == 0:
                self.scheduler.terminate(wait)
                self.terminated = True
                logger.log_debug("Activity trigger terminated")
            else:
                # Normally when you cancel the first element in the queue the next element
                # if any, will be triggered, unless we are terminating or terminated
                self.unlock()
                self.cancel_first()
                self.lock()
                self.sleep_queue.clear()
                self.scheduler.terminate(wait)
                logger.log_debug("Activity trigger terminated")
                self.terminated = True
        finally:
            self.unlock()

    def get_sleep_queue(self):
        return self.sleep_queue

    def is_terminated(self):
        return self.terminated

    def cancel_first(self):

        try:
            self.lock()
            if len(self.sleep_queue) > 0:
                logger.log_debug("Cancelling first in queue")
                self.sleep_queue.pop(0)
                self.trigger_timer.cancel()
                # Triggering the next element in the queue that is becoming the top once the
                # previous top has been unarmed, unless we are terminating the activitiy trigger
                if (len(self.sleep_queue) > 0) and not (self.terminating or self.terminated):
                    logger.log_debug("Queue not empty arming next activity at " +
                                     str(self.sleep_queue[0].get_trigger_time()))

                    trigger_secs = (self.sleep_queue[0].get_trigger_time(
                    ) - datetime.datetime.now()).seconds
                    self.trigger_timer = threading.Timer(
                        trigger_secs, self.fire)
                    self.trigger_timer.start()
        finally:
            self.unlock()

    def print_queue_content(self):
        self.lock()
        index = 0
        for elem in self.sleep_queue:
            logger.log_verbose(
                "TriggerQueue[" + str(index) + "] = " + elem.get_activity().name +
                " at " + str(elem.get_trigger_time()))
            index += 1
        self.unlock()

    def get_queue_content(self):
        copylist = []
        try:
            self.lock()
            for elem in self.sleep_queue:
                copylist.append(elem)
        finally:
            self.unlock()
        return copylist

    def arm(self, scheduled_activity, trigger_secs):
        self.lock()

        if self.terminating or self.terminated:
            self.unlock()
            return

        sleep_queue = []
        name = scheduled_activity.get_activity().name
        triggered = False

        trigger_at = datetime.datetime.now() + datetime.timedelta(0, trigger_secs)

        # If the queue is emypy we add the element staight in and trigger the timer
        if len(self.sleep_queue) == 0:
            logger.log_pedantic("Queue empty insterting in head")
            scheduled_activity.set_trigger_time(trigger_at)
            sleep_queue.append(scheduled_activity)
            self.trigger_timer = threading.Timer(trigger_secs, self.fire)
            self.trigger_timer.start()
            triggered = True
            logger.log_debug("Triggering " + name + " at " +
                             str(scheduled_activity.get_trigger_time()))
        else:
            # otherwise we need to loop in the queue and insert the element in the right
            # position given the time ordering and in case it is the first element
            # we need to cancel the timer and trigger a new one
            index = 0
            inserted = False
            for elem in self.sleep_queue:
                if trigger_at < elem.get_trigger_time() and not inserted:
                    logger.log_pedantic(
                        "Inserting " + name + " in element " + str(index))
                    scheduled_activity.set_trigger_time(trigger_at)
                    sleep_queue.append(scheduled_activity)
                    if index == 0:
                        self.trigger_timer.cancel()
                        self.trigger_timer = threading.Timer(
                            trigger_secs, self.fire)
                        self.trigger_timer.start()
                        logger.log_debug("Triggering " + name + " at " +
                                         str(scheduled_activity.get_trigger_time()))
                    triggered = True
                    inserted = True
                index += 1
                sleep_queue.append(elem)

            # If the activity is the last in the ordering it wouldn't have been inserted
            # in the prvious loop so we need to force it
            if not triggered:
                logger.log_pedantic("Inserting " + name +
                                    " the back of the queue")
                scheduled_activity.set_trigger_time(trigger_at)
                sleep_queue.append(scheduled_activity)
                triggered = True

        self.sleep_queue = sleep_queue
        self.unlock()
        logger.log_pedantic(
            "===========================================================================")
        self.print_queue_content()
        logger.log_pedantic(
            "===========================================================================")
