import os
import sys
import time
from syslog import LOG_DEBUG

import lib.logger as logger

stats_path = os.path.join("data", "stats")
stats_data_file_name = os.path.join(stats_path, "global_stats.dat")
stats_html_file_name = os.path.join(stats_path, "global_stats.html")
template_header_name = os.path.join(stats_path, "template_header.html")

stats_data_file = None
stats_html_file = None
screenshots_subdir = None

stats_data_list = []


class StatsData():
    def __init__(self):
        self.date = ""
        self.time = ""
        self.match_type = ""
        self.area = ""
        self.stage = ""
        self.duration = ""
        self.result = ""
        self.screenshot_file_start = ""
        self.screenshot_file_end = ""

    def export_html_table_line(self):
        table_line = "  <tr>\n"

        table_line += "    <td class=\"tg-0lax\">" + self.date + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.time + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.match_type + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.area + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.stage + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.duration + "</td>\n"
        table_line += "    <td class=\"tg-0lax\">" + self.result + "</td>\n"

        img_opponent_path = "raid/thumb_" + \
            self.screenshot_file_start.split("/")[6]
        img_result_path = "raid/thumb_" + \
            self.screenshot_file_end.split("/")[6]

        table_line += "    <td class=\"tg-0lax\">"
        if len(self.screenshot_file_start) > 0:
            table_line += "<a href=\"" + self.screenshot_file_start + \
                "\"><img src=\"" + img_opponent_path + "\"></a>"
        table_line += "</td>\n"

        table_line += "    <td class=\"tg-0lax\">"
        if len(self.screenshot_file_end) > 0:
            table_line += "<a href=\"" + self.screenshot_file_end + \
                "\"><img src=\"" + img_result_path + "\"></a>"
        table_line += "</td>\n"

        table_line += "  </tr>\n"
        return table_line

    def set_directly(self, date, time, match_type, area, stage, duration, result, screenshot_file_start, screenshot_file_end):
        self.date = date
        self.time = time
        self.match_type = match_type
        self.area = area
        self.stage = stage
        self.duration = duration
        self.result = result
        self.screenshot_file_start = screenshots_subdir + screenshot_file_start
        self.screenshot_file_end = screenshots_subdir + screenshot_file_end

    def set_from_string(self, data_string):
        tokens = data_string.strip().split(",")

        if len(tokens) < 9:
            return False

        logger.log_pedantic("Tokens in data file line are " +
                            str(len(tokens)), "StatsHandler")
        for token in tokens:
            logger.log_pedantic("value = " + token)

        self.date = tokens[0]
        self.time = tokens[1]
        self.match_type = tokens[2]
        self.area = tokens[3]
        self.stage = tokens[4]
        self.duration = tokens[5]
        self.result = tokens[6]
        self.screenshot_file_start = tokens[7]
        self.screenshot_file_end = tokens[8]
        return True


def initialise(screenshots_subdir_parm):
    global stats_path
    global stats_data_file
    global stats_html_file
    global screenshots_subdir

    screenshots_subdir = "../../" + screenshots_subdir_parm + "/"

    # Creates dir path if it doesn't exist
    if not os.path.exists(stats_path):
        os.makedirs(stats_path)

    # Creates file if it doesn't exist
    if not os.path.exists(stats_data_file_name):
        stats_data_file = open(stats_data_file_name, "w")
    else:
        stats_data_file = open(stats_data_file_name, "a")
    
    # Create anyway
    stats_html_file = open(stats_html_file_name, "w")


def update_data(date, time, match_type, area, stage, duration, result, screenshot_file_start, screenshot_file_end):
    global stats_data_file

    stats_data_file.write(date + ",")
    stats_data_file.write(time + ",")
    stats_data_file.write(match_type + ",")
    stats_data_file.write(area + ",")
    stats_data_file.write(stage + ",")
    stats_data_file.write(duration + ",")
    stats_data_file.write(result + ",")
    stats_data_file.write(screenshots_subdir + screenshot_file_start + ",")
    stats_data_file.write(screenshots_subdir + screenshot_file_end + "\n")
    #stats_data_list.append(StatsData(date, time, match_type, area, stage, duration, result,screenshot_file_start,screenshot_file_end))


def update_html():
    global stats_data_list
    global stats_html_file
    with open(template_header_name, "r") as template_header:
        for line in template_header:
            stats_html_file.write(line)

        with open(stats_data_file_name, "r") as stats_data_file:
            for entry in stats_data_file:
                if len(entry.strip()) == 0:
                    continue
                stats_data = StatsData()
                if stats_data.set_from_string(entry):
                    stats_html_file.write(stats_data.export_html_table_line())
        stats_html_file.write("</table>\n")


def deinitialise():
    global stats_data_file
    global stats_html_file

    stats_data_file.close()
    update_html()

    stats_html_file.close()
