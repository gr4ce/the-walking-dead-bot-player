"""
This module handles all the global parameters and the ones passed
by command line
"""
import os
import argparse

# Other game modules
import lib.logger as logger
import lib.user_levels as user_levels



def import_levels():

    levels = {}
    level_file = open(os.path.join("data", "user_levels.csv"), "r")
    header = level_file.readline()
    header_tokens = header.rstrip().split(',')

    for line in level_file:
        tokens = line.rstrip().split(',')
        levels[tokens[0]] = {header_tokens[1]: int(tokens[1]),
                             header_tokens[2]: int(tokens[2]),
                             header_tokens[3]: int(tokens[3])}

    level_file.close()

    return levels


# Handling the world energy constants
CURRENT_LEVEL = "150"
LEVELS = import_levels()

WORLD_ENERGY_CAP = LEVELS[CURRENT_LEVEL]["max_world_energy"]
RECHARGE_TIME_MINUTES = LEVELS[CURRENT_LEVEL]["world_energy_timer_minutes"]
RECHARGE_TIME_SECONDS = LEVELS[CURRENT_LEVEL]["world_energy_timer_seconds"]

# Handling the raid energy constants
RAID_ENERGY_CAP = 6
RAID_RECHARGE_TIME_MINUTES = 45
RAID_RECHARGE_TIME_SECONDS = 0

# Handling the territory energy constants
TERR_ENERGY_CAP = 10
TERR_RECHARGE_TIME_MINUTES = 60
TERR_RECHARGE_TIME_SECONDS = 0

# Handling the road to survival tournament energy constants
RTST_ENERGY_CAP = 8
RTST_RECHARGE_TIME_MINUTES = 15
RTST_RECHARGE_TIME_SECONDS = 0


args = None
env = {}

with open('.env') as envfile:
    logger.log_debug("Loading .env file", "Env loader")
    envstring = envfile.readline().strip()
    env["environment"] = envstring
    logger.log_debug("environment = " + str(envstring), "Env loader")


def initialise():

    global args

    parser = argparse.ArgumentParser(
        description='Initialise the game parameters')

    parser.add_argument('--no-supporter',
                        dest='supporter',
                        action='store_const',
                        const=False,
                        default=True,
                        help='If used then pick a faction supporter')

    parser.add_argument('--no-remote',
                        dest='no_remote',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Do not use remote communication')

    parser.add_argument('--exit',
                        dest='exit',
                        action='store_const',
                        const=False,
                        default=False,
                        help='Exit after one play')

    parser.add_argument('--raid-tournament',
                        dest='raid_tournament',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Play raid in tournament mode')

    parser.add_argument('--take-screenshot',
                        dest='take_screenshot',
                        action='store_const',
                        const=True,
                        default=False,
                        help='If set then screenshot will be captured')

    parser.add_argument('--send-screenshot',
                        dest='send_screenshot',
                        action='store_const',
                        const=True,
                        default=False,
                        help='If set then captured screenshots will be sent via messaging system')

    parser.add_argument('--world-energy',
                        dest='world_energy',
                        type=int,
                        default=WORLD_ENERGY_CAP,
                        help='Energy initial value (default: WORLD_ENERGY_CAP)')

    parser.add_argument('--raid-energy',
                        dest='raid_energy',
                        type=int,
                        default=RAID_ENERGY_CAP,
                        help='Energy initial value (default: RAID_ENERGY_CAP)')

    parser.add_argument('--terr-energy',
                        dest='terr_energy',
                        type=int,
                        default=TERR_ENERGY_CAP,
                        help='Energy initial value (default: TERR_ENERGY_CAP)')

    parser.add_argument('--rtst-energy',
                        dest='rtst_energy',
                        type=int,
                        default=RTST_ENERGY_CAP,
                        help='Energy initial value (default: RTST_ENERGY_CAP)')

    parser.add_argument('--map-type',
                        dest='map_val',
                        default='world',
                        help='Map type (world, roadmap, road to survival, raid or custom)',
                        choices=['world', 'roadmap', 'custom', 'rtsdaily', 'rtstourney', 'raid', 'roadmapfind'])

    parser.add_argument('--area',
                        dest='area_val',
                        default='5',
                        help='Area identifier (1-14 for world map and easy, hard for roadmap map)',
                        choices=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "easy", "hard"])

    parser.add_argument('--refills',
                        dest='refills_val',
                        default='0',
                        help='Tells how many refills to use. -1 for infinite but be carefull about this option')

    parser.add_argument('--stage',
                        dest='stage_val',
                        default='rand',
                        help='Choosing the stage explicitly or select rand (which is the default choice)',
                        choices=["1", "2", "3", "4", "5", "6", "7", "8", "rand"])

    parser.add_argument('--mode',
                        dest='mode_val',
                        default='param',
                        help='Mode to play the stages within an area',
                        choices=["random", "from-start", "param"])

    parser.add_argument('--time',
                        dest='time_val',
                        type=int,
                        nargs=2,
                        default=[RECHARGE_TIME_MINUTES, RECHARGE_TIME_SECONDS],
                        help='Minutes and seconds left')

    parser.add_argument('--incremental',
                        dest='incremental',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Play incrementally from the stage define to the max of the area')

    parser.add_argument('--wait',
                        dest='wait_val',
                        default=180,
                        help='Seconds to wait before starting the next match')

    parser.add_argument('--stage-energy',
                        dest='stage_energy',
                        default=-1,
                        help='In case you are playing a custom stage and want to specify the energy')

    parser.add_argument('--log',
                        dest='log_level',
                        default='PEDANTIC',
                        help='Verbosity level of the logger',
                        choices=["INFO", "DEBUG", "VERBOSE", "PEDANTIC"])

    arguments = parser.parse_args()
    args = arguments

    return arguments


