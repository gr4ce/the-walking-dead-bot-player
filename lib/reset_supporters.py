#!/usr/bin/python

import sys
import math
import datetime
import faction_supporter as fs


if __name__ == '__main__':
    
    if len(sys.argv) == 2:
        sup_id = int(sys.argv[1])
        fs.load_supporters()
        for supporter in fs.global_supporters:
            if sup_id == supporter[0]:
                supporter[1] = "2016-01-01 00:00:00"
    else:
        for i in range(0, fs.MAX_FACTION_SUPPORTERS):        
            supporter = [i, "2016-01-01 00:00:00"]
            fs.global_supporters.append(supporter)

    fs.flush_supporters()

