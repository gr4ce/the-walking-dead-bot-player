import os
import abc

class ExperienceMap():
    
    def __init__(self, max_tier, max_levels):
        
        self.experience_map = max_tier * []
        
        for tier in range(1, max_tier + 1):
            self.experience_map[tier] = max_levels * []
        
    
    def get_experience(self, tier, level):
        return self.experience_map[tier][level]
        
    def set_experience(self, tier, level, xp_val):
        self.experience_map[tier][level] = xp_val

class Character():
    
    __metaclass__ = abc.ABCMeta
    
    def __init__(self, name, stars, persona, icon):

        # Read only properties 
        self.name    = name
        self.stars   = stars
        self.persona = persona
        self.icon    = icon
        
        # 
        self.current_tier  = 1
        self.max_tier      = None
        self.current_level = 1

        # This depends on the rarity
        self.max_level          = None
        self.current_experience = 0
        
    def get_name(self):
        return self.name
    
    def get_current_tier(self):
        return self.current_tier
    
    def get_current_leve(self):
        return self.current_level
    
    @abc.abstractmethod
    def get_stars(self):
        ...
        
    @abc.abstractmethod
    def get_max_level(self, tier):
        ...
    
    @abc.abstractmethod
    def get_all_max_levels(self):
        ...
    
    @abc.abstractmethod
    def get_max_tier(self):
        ...
        
class Character1Stars(Character):
    Character1Stars.MAX_TIER = 2
    Character1Stars.MAX_LEVEL = [10, 20]
    Character1Stars.experience = ExperienceMap(Character1Stars.MAX_TIER, Character1Stars.MAX_LEVEL)
    
    def get_stars(self):
        return 1
    
    def get_max_level(self, tier):
        return Character1Stars.MAX_LEVEL[tier]
    
    def get_all_max_levels(self):
        return Character1Stars.MAX_LEVELS
    
    def get_max_tier(self):
        return Character1Stars.MAX_TIER
    
    def load_experience_map(self, filename):
        if os.path.isfile(filename):
            os.open(filename, 'r')
        else:
            # Throw exception

class Character2Stars(Character):
    MAX_TIER = 3
    MAX_LEVEL = [20, 30, 40]
    
    def get_stars(self):
        return 2
    
class Character3Stars(Character):
    MAX_TIER = 3
    MAX_LEVEL = [30, 40, 50]
    
    def get_stars(self):
        return 1
    
class Character4Stars(Character):
    MAX_TIER = 4
    MAX_LEVEL = [40, 50, 60, 70]

class Character5Stars(Character):
    MAX_TIER = 4
    MAX_LEVEL = [50, 60, 70, 80]