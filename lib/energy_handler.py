import os
import sys
import time
import threading
from multiprocessing import Event

import lib.logger as logger
import lib.executor as shell
import lib.parameters as parameters

GAME_CONFIG_START_PLAY_AT = 40

timer = {}


class EnergyHandler:
    def __init__(self, name, max_energy, max_minutes, max_seconds, curr_energy, curr_minutes, curr_seconds):
        self.name = name
        self.max_energy = max_energy
        self.max_minutes = max_minutes
        self.max_seconds = max_seconds
        self.curr_energy = curr_energy
        self.curr_minutes = curr_minutes
        self.curr_seconds = curr_seconds
        self.timer_status = "UNINITIALISED"
        self.critical = threading.Lock()
        self.timer_thread = threading.Thread(
            name="EnergyHandlerThread-"+self.name,
            target=self.energy_loop)
        logger.log_verbose("Initialising timer for " +
            self.name + " at value " + str(self.curr_energy))

        # This needs to be used to notify listeners that the energy is full
        self.energy_full = Event()

        # This needs to be used to notify listeners that the energy has increased by 1 unit
        self.energy_increased = Event()
        self.energy_increased.clear()

    def energy_left(self):
        return self.curr_energy

    def get_max_energy(self):
        return self.max_energy

    def __str__(self):
        value = "timer : " + self.name + "\n"
        value += "c_min : " + str(self.curr_minutes) + "\n"
        value += "c_sec : " + str(self.curr_seconds) + "\n"
        value += "status: " + self.timer_status
        return value

    def increment_energy(self):
        self.critical.acquire()

        if self.timer_status == "ACTIVE" and self.curr_energy < self.max_energy:
            self.curr_minutes = self.max_minutes
            self.curr_seconds = self.max_seconds
            self.curr_energy += 1
            message = self.name + " +1, left " + str(self.energy_left())
            logger.log_debug(message, self.name)

        elif self.timer_status == "SLEEPING" and self.curr_energy >= self.max_energy:
            message = "Requested increment energy when time_status = SLEEPING"
            logger.log_warn(message, self.name)

        else:
            message = "Something about the status is fundamentally wrong"
            logger.log_error(message, self.name)

        self.energy_increased.set()
        self.critical.release()

    def get_timer_value(self):
        return [self.curr_minutes, self.curr_seconds]

    def wait_util_energy_gets_to(self, value):
        while self.energy_left() < value:
            message = "Energy needs to increase"
            logger.log_info(message, self.name)
            self.energy_increased.clear()
            self.energy_increased.wait()
            message = "Energy refilled (" + str(self.energy_left()) + ")"
            logger.log_info(message, self.name)

    def set_timer_value(self, minutes, seconds):
        self.critical.acquire()
        self.curr_minutes = minutes
        self.curr_seconds = seconds
        self.critical.release()

    def sync_wait(self):

        self.critical.acquire()

        while not (self.curr_minutes == 0 and self.curr_seconds == 0) and self.timer_status != "EXIT":
            if self.curr_seconds == 0:
                self.curr_seconds = 59
                self.curr_minutes -= 1
            else:
                self.curr_seconds -= 1
            self.critical.release()
            time.sleep(1)
            self.critical.acquire()

        sys.stdout.flush()
        self.critical.release()
        time.sleep(1)

    def set_energy(self, energy):

        self.critical.acquire()

        # A few checks is better to do
        if energy > self.max_energy:
            raise Exception("energy value " + str(energy) +
                            " is greater than the max energy cap = " + self.max_energy)
        if energy < 0:
            raise Exception("energy value " + str(energy) +
                            " can't be less than 0")

        # We are increasing the energy so better notify who is waiting for this
        if self.curr_energy < energy:
            self.energy_increased.set()

        # We set the value
        self.curr_energy = energy
        logger.log_debug("Energy value set to " +
                         str(self.curr_energy), self.name)

        # If the timer is sleeping we need to wake it up
        if self.timer_status == "SLEEPING":
            self.timer_status == "ACTIVE"
            self.energy_full.set()

        self.critical.release()

    def start_timer(self):
        self.timer_thread.start()
        pass

    def wait_timer_termination(self):
        self.timer_thread.join()

    def use_refill(self):

        self.critical.acquire()

        self.curr_minutes = self.max_minutes
        self.curr_seconds = self.max_seconds
        self.curr_energy = self.max_energy

        if self.timer_status == "ACTIVE":
            self.timer_status = "SLEEPING"
            self.energy_increased.set()

        self.critical.release()

    def use_energy(self, energy):
        self.critical.acquire()
        if energy > self.curr_energy:
            raise Exception("Requesting more energy(" + str(energy) +
                            ") than available(" + str(self.curr_energy) + ")")
        self.curr_energy -= energy
        message = "Energy used " + \
            str(energy) + " left " + str(self.energy_left())
        logger.log_debug(message, self.name)
        self.energy_full.set()
        self.critical.release()

    def exit(self):
        logger.log_debug("status = EXIT", self.name)
        self.timer_status = "EXIT"
        self.energy_full.set()

    def energy_loop(self):
        logger.log_debug("Energy handler is starting", self.name)

        if self.curr_energy < self.max_energy:
            self.energy_increased.set()
            self.timer_status = "ACTIVE"
        elif self.curr_energy == self.max_energy:
            self.energy_full.set()
            self.timer_status = "SLEEPING"
        else:
            message = "Inconsitency in energy values: curr_energy = " + \
                str(self.curr_energy) + ", max_energy = " + str(self.max_energy)
            logger.log_error(message, self.name)
            raise Exception(message)

        while True:

            if self.timer_status == "EXIT":
                break

            # TODO this needs absolutely to be changed
            if self.curr_energy > GAME_CONFIG_START_PLAY_AT:
                self.energy_increased.set()

            if self.curr_energy == self.max_energy:
                self.timer_status = "SLEEPING"
                logger.log_debug("status = SLEEPING", self.name)
                self.energy_full.clear()

                # Wait until someone start a match and exactly when it presses fight button
                logger.log_debug(
                    "Max energy cap, can't refill anymore", self.name)
                self.energy_full.wait()

                if self.timer_status == "EXIT":
                    break

                logger.log_debug(
                    "Someone's playing, start refilling", self.name)
                self.timer_status = "ACTIVE"
            else:
                self.timer_status = "ACTIVE"
                logger.log_debug("status = ACTIVE", self.name)
                self.sync_wait()

                if self.timer_status == "EXIT":
                    break

                if self.timer_status == "ACTIVE":
                    energy_before = self.energy_left()
                    self.increment_energy()
                    energy_after = self.energy_left()
                    logger.log_debug(
                        "Energy refilled (" + str(energy_before) + " -> " + str(energy_after) + ")", self.name)

        logger.log_debug("Job done here I'm going to die", self.name)


def initialise_timer(name, max_val, curr_val, max_min, max_sec, curr_min, curr_sec):

    # We need to pick this up from a config file
    logger.log_verbose("Initialising timer for " + name +
                       " at value " + str(curr_val), name)
    energyHandler = EnergyHandler(
        name, max_val, max_min, max_sec, curr_val, curr_min, curr_sec)
    timer[name] = energyHandler


initialise_timer("rts",
                 parameters.RTST_ENERGY_CAP, parameters.RTST_ENERGY_CAP,
                 parameters.RTST_RECHARGE_TIME_MINUTES, parameters.RTST_RECHARGE_TIME_SECONDS,
                 parameters.RTST_RECHARGE_TIME_MINUTES, parameters.RTST_RECHARGE_TIME_SECONDS)

initialise_timer("world",
                 parameters.WORLD_ENERGY_CAP, parameters.WORLD_ENERGY_CAP,
                 parameters.RECHARGE_TIME_MINUTES, parameters.RECHARGE_TIME_SECONDS,
                 parameters.RECHARGE_TIME_MINUTES, parameters.RECHARGE_TIME_SECONDS)

initialise_timer("raid",
                 parameters.RAID_ENERGY_CAP,
                 parameters.RAID_ENERGY_CAP,
                 parameters.RAID_RECHARGE_TIME_MINUTES, parameters.RAID_RECHARGE_TIME_SECONDS,
                 parameters.RAID_RECHARGE_TIME_MINUTES, parameters.RAID_RECHARGE_TIME_SECONDS)
