#!/usr/bin/python

import sys
import math
import datetime
import faction_supporter as fs

from datetime import timedelta

if __name__ == '__main__':

	if len(sys.argv) != 5:
		raise ValueException("Usage is " + sys.argv[0] + " supporter_id hours minutes seconds")

	# Some check on valuse here would be nice
	supp_id = int(sys.argv[1])	
	hours   = sys.argv[2]
	minutes = sys.argv[3]
	seconds = sys.argv[4]

	# Calculating the time differece based on what the user passed
	time_string = str(hours) + ":" + str(minutes) + ":" + str(seconds) 
	available_in = timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
	now = datetime.datetime.now() 
	
	#print("Was last hired at: " + (now - available_in).strftime("%Y-%m-%d %H:%M:%S"))
	
	# Load the faction supporter file which will be using the global global_supporters
	fs.load_supporters()

	# Calculating the time differece based on what the user passed
	time_string = str(hours) + ":" + str(minutes) + ":" + str(seconds) 
	available_in = timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
	now = datetime.datetime.now() 

	supporter = fs.global_supporters[supp_id]
	supporter[1] = (now - available_in).strftime("%Y-%m-%d %H:%M:%S")
	#for supporter in fs.global_supporters:
	#	print( "Supporter " + str(supporter[0]) + " used at time " + str(supporter[1]))

	fs.flush_supporters()
