import sys
import inspect
import datetime
import threading

LOG_LEVEL_PEDANTIC = 6
LOG_LEVEL_VERBOSE = 5
LOG_LEVEL_DEBUG = 4
LOG_LEVEL_INFO = 3
LOG_LEVEL_WARNING = 2
LOG_LEVEL_ERROR = 1
LOG_LEVEL = LOG_LEVEL_PEDANTIC

LOG_ENABLE_TIMESTAMP = True

log_strings = {"INFO": LOG_LEVEL_INFO,
               "DEBUG": LOG_LEVEL_DEBUG,
               "VERBOSE": LOG_LEVEL_VERBOSE,
               "PEDANTIC": LOG_LEVEL_PEDANTIC}

log_strings_reverse = {LOG_LEVEL_INFO: "INFO",
                       LOG_LEVEL_DEBUG: "DEBUG",
                       LOG_LEVEL_VERBOSE: "VERBOSE",
                       LOG_LEVEL_PEDANTIC: "PEDANTIC"}

log_crit_section = threading.Lock()

modules = []

def add_module(module):
    modules.append(module)

def set_log_level(level):
    global LOG_LEVEL
    try:
        int(level)
        LOG_LEVEL = level
    except:
        if level in log_strings.keys():
            LOG_LEVEL = log_strings[level]
        else:
            raise Exception("Unknown log level " + str(LOG_LEVEL))

    log_info("Setting logger vebosity level to " +
             log_strings_reverse[LOG_LEVEL], "Logger")


def get_log_level():
    return LOG_LEVEL

def get_log_level_string():
    return log_strings_reverse[get_log_level()]

def enable_timestamp():
    global LOG_ENABLE_TIMESTAMP
    LOG_ENABLE_TIMESTAMP = True


def disable_timestamp():
    global LOG_ENABLE_TIMESTAMP
    LOG_ENABLE_TIMESTAMP = False


def log_get_time_string():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def log_error(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_ERROR:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[ERROR  ]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stderr")
    log_crit_section.release()


def log_warn(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_WARNING:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[WARNING]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stderr")
    log_crit_section.release()


def log_debug(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_DEBUG:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[DEBUG  ]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stdout")
    log_crit_section.release()


def log_info(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_INFO:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[INFO   ]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stdout")
    log_crit_section.release()


def log_verbose(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_VERBOSE:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[VERBOSE]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stdout")
    log_crit_section.release()


def log_pedantic(message, source="unknown"):
    log_crit_section.acquire()
    _stack = inspect.stack()[1]
    if source == "unknown":
        source = _stack[0].f_locals['self'].__class__.__name__
    output_string = ""
    if LOG_LEVEL >= LOG_LEVEL_PEDANTIC:
        if LOG_ENABLE_TIMESTAMP:
            output_string += "[" + log_get_time_string() + "]"
        output_string += "[PDNTC  ]"
        output_string += "[%s] - " % source
        output_string += message
        output_string += "\n"
        filtered_write(output_string, source, "stdout")
    log_crit_section.release()

def filtered_write(message, source, dest):
   
    #for mod in modules:
    #    if mod in message:
    #        sys.stdout.write(message)
    if "adb" not in source:
        if dest == "stdout":
            sys.stdout.write(message)
        if dest == "stderr":
            sys.stderr.write(message)

