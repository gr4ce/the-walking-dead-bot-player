"""A simple ulil module with some handy functions"""
import lib.logger as logger
import lib.buttons as buttons
import lib.msg_handler as msg_handler
import lib.img_detector as img_detector
import lib.device_handler as device_handler


def recover_war_results():
    device_handler.current_device.press_point(buttons.OK_TO_WAR_RESULT)


def recover_war_scorecard():
    device_handler.current_device.press_point(buttons.OK_TO_WAR_RESULT)


def recover_network_error():
    device_handler.current_device.press_point(buttons.NETWORK_ERROR)


def recover_switch_account():
    device_handler.current_device.press_point(buttons.SWITCH_ACCOUNT)

def recover_region_war_over():
    device_handler.current_device.press_point(buttons.REGION_WAR_OVER_NEXT_BUTTON)

def recover_region_war_has_ended():
    device_handler.current_device.press_point(buttons.REGION_WAR_HAS_ENDED_NEXT_BUTTON)

def recover_commercial_msg():
    pass


def recover_prestige_rewards():
    device_handler.current_device.press_point(buttons.PRESTIGE_REWARDS_OK)


EXCEPTION_RECOVERY = {"WAR RESULTS":      {"description": "Pressing OK button",
                                           "action": recover_war_results},
                      "WAR SCORECARD":    {"description": "Pressing OK button",
                                           "action": recover_war_scorecard},
                      "NETWORK ERROR":    {"description": "Pressing OK button",
                                           "action": recover_network_error},
                      "SWITCH ACCOUNT":   {"description": "Switching account",
                                           "action": recover_switch_account},
                      "REGION WAR OVER":  {"description": "Pressing NEXT button",
                                           "action": recover_region_war_over},
                      "REGION WAR HAS ENDED":  {"description": "Pressing NEXT button",
                                           "action": recover_region_war_has_ended},
                      "PRESTIGE REWARDS": {"description": "Collect the rewards",
                                           "action": recover_prestige_rewards}}

def handle_exception():
    logger.log_verbose("Trying to identify exception case", "ExceptionHandler")
    exception_case = img_detector.identify_exception_case()
    msg_handler.send_remote_message(
        "Exception handler identified the problem to be: " + exception_case)
    msg_handler.capture_compress_and_send_screenshot()
    return recover_exception(exception_case)


def recover_exception(exception_string):

    global EXCEPTION_RECOVERY
    if exception_string in EXCEPTION_RECOVERY.keys():
        exception_element = EXCEPTION_RECOVERY[exception_string]
        logger.log_info("Trying to recover from " + exception_string +
                        " with action = " + exception_element["description"], "ExceptionHandler")
        exception_element["action"]()
        if img_detector.are_we_home() is True:
            logger.log_info("Action worked, successfully recovered", "ExceptionHandler")
            return True
        return False

    logger.log_error("Exception string not recognised: " + exception_string, "ExceptionHandler")
    return False
