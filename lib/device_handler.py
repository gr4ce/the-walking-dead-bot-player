"""
This modules deals with everything that is related to a physical device, its
connections and the commands that it can run
"""

import os
import time
import random

import lib.logger as logger
import lib.executor as shell
import lib.parameters as parameters
# from adb import adb_commands as adbpy

ANDROID_DEVICE_ONE_PLUS_3T = 1000
ANDROID_DEVICE_NEXUS_5 = 1001
ANDROID_DEVICE_GALAXY_TAB_3 = 1002
ANDROID_DEVICE_TEXTOR_TAB = 1003
SIMULATED_DEVICE = 1004


available_devices = []
current_device = None



def get_device():
    simulated = False
    global current_device

    if current_device is not None:
        return current_device

    if parameters.env["environment"] == "test":
        simulated = True

    device_id = None
    device_info = None

    if simulated:
        logger.log_info("Connected device SimulatedDevice", "DeviceHandler")
        device_id = SIMULATED_DEVICE
        if DEVICES[device_id] is None:
            DEVICES[device_id] = SimulatedDevice(
                "SimulatedConnection", "SimulatedDevice")

        current_device = DEVICES[device_id]
        return current_device

    device_info = get_device_info()

    if len(device_info) == 0:
        logger.log_warn(
            "No device conntected! Please connect a device", "DeviceHandler")

    while len(device_info) == 0:
        time.sleep(5)
        device_info = get_device_info()

    logger.log_info("Device connected", "DeviceHandler")

    for device in device_info:
        logger.log_info("Connected device " +
                        device[1] + " thorugh connection " + device[0], "DeviceHandler")
        if device[1] == "Nexus_5":
            device_id = ANDROID_DEVICE_NEXUS_5
            if DEVICES[device_id] is None:
                DEVICES[device_id] = DeviceNexus5(device[1], device[0])
            break

        if device[1] == "ONEPLUS_A3003":
            device_id = ANDROID_DEVICE_ONE_PLUS_3T
            if DEVICES[device_id] is None:
                DEVICES[device_id] = DeviceOnePlus3T(device[1], device[0])
            break

    current_device = DEVICES[device_id]
    return current_device


def get_device_info():

    device_info = []
    line = shell.execute("adb devices -l|tail -2|head -1")

    if line.strip() == "":
        return

    tokens = line.split("device ")

    info_tokens = tokens[1].strip().split(" ")
    logger.log_pedantic("info_tokens[0] = " + info_tokens[0], "DeviceHandler")
    logger.log_pedantic("info_tokens[1] = " + info_tokens[1], "DeviceHandler")
    logger.log_pedantic("info_tokens[2] = " + info_tokens[2], "DeviceHandler")
    if "5555" in tokens[0].strip():
        device_info.append(
            [tokens[0].strip(), info_tokens[1].split(":")[1]])
    else:
        device_info.append(
            [tokens[0].strip(), info_tokens[2].split(":")[1]])

    return device_info


class Device():
    def __init__(self, deviceid, deviceconn):
        self.deviceid = 0
        self.deviceconn = ""
        self.devicename = "<not set>"
        self.set_device_id(deviceid)
        self.set_device_conn(deviceconn)

    # Device common stuff
    def set_device_id(self, deviceid):
        if deviceid == "Nexus_5":
            self.deviceid = ANDROID_DEVICE_NEXUS_5
            self.devicename = "Nexus5"
            return

        if deviceid == "ONEPLUS_A3003":
            self.deviceid = ANDROID_DEVICE_ONE_PLUS_3T
            self.devicename = "OnePlus3T"
            return

        self.deviceid = SIMULATED_DEVICE
        self.devicename = "simulated"

    def set_device_conn(self, deviceconn):
        self.deviceconn = deviceconn

    def get_device_id(self):
        return self.deviceid

    def get_device_conn(self):
        return self.deviceconn

    def press_back_button(self):
        self.execute_adb_command("shell input keyevent 4")
        time.sleep(1)

    def press_power_button(self):
        self.execute_adb_command("shell input keyevent 26")
        time.sleep(2)

    def press_home_button(self):
        logger.log_debug("Pressing home button", self.devicename)
        self.execute_adb_command("shell input keyevent 3")
        time.sleep(2)

    def close_game(self):
        self.press_menu_button()
        self.force_stop_game()
        self.press_home_button()

    def force_stop_game(self):
        logger.log_debug(
            "Forcing stop game.. this will leave all the app opened", self.devicename)
        self.execute_adb_command("shell am force-stop com.scopely.headshot")

    def execute_adb_command(self, command):
        full_command_line = "adb -s " + self.deviceconn + " " + command
        logger.log_pedantic(full_command_line, "adb subsystem")
        return shell.execute(full_command_line)

    def convert_to_resolution(self, coords):
        curr_res = self.get_resolution()
        if curr_res != [1920, 1080]:
            coords[0] = int((coords[0] * (curr_res[0] / 1920.0)))
            coords[1] = int((coords[1] * (curr_res[1] / 1080.0)))
        return coords

    def press_point(self, point, asynch=False):
        coords = self.convert_to_resolution(point)
        command = "shell input tap " + str(coords[0]) + " " + str(coords[1])
        if asynch:
            command += " &"
        self.execute_adb_command(command)

    def press_button_as_list(self, button, rando=True):
        self.press_button(button[0], button[1], button[2], button[3], rando)

    def press_button(self, x1, y1, x2, y2, rando=True):
        up_left_coords = self.convert_to_resolution([x1, y1])
        low_right_coords = self.convert_to_resolution([x2, y2])

        if rando:
            x = random.randint(up_left_coords[0], low_right_coords[0])
            y = random.randint(up_left_coords[1], low_right_coords[1])
        else:
            x = up_left_coords[0] + \
                ((low_right_coords[0] - up_left_coords[0]) / 2.0)
            y = up_left_coords[1] + \
                ((low_right_coords[1] - up_left_coords[1]) / 2.0)

        self.press_point([int(x), int(y)])

    def swipe(self, from_x, from_y, to_x, to_y, speed=100):
        [from_x, from_y] = self.convert_to_resolution([from_x, from_y])
        [to_x, to_y] = self.convert_to_resolution([to_x, to_y])
        command = "shell input swipe " + \
            str(from_x) + " " + str(from_y) + " " + \
            str(to_x) + " " + str(to_y) + " " + str(speed)
        self.execute_adb_command(command)

    def long_press(self, _2D_button, speed=200):
        from_x = _2D_button[0] - 1
        from_y = _2D_button[1] - 1
        to_x = _2D_button[0] + 1
        to_y = _2D_button[1] + 1

        command = "shell input swipe " + \
            str(from_x) + " " + str(from_y) + " " + \
            str(to_x) + " " + str(to_y) + " " + str(speed)
        self.execute_adb_command(command)

    def get_screen_status(self):
        result = self.execute_adb_command(
            "shell dumpsys power | grep 'mWakefulness=' | awk -F '=' '{print $2}'")
        return result.strip()

    def is_screen_locked(self):
        # The following line was used before but now it shouldn't be longer applicable
        # self.execute_adb_command("shell dumpsys power | grep mHoldingWakeLockSuspendBlocker  | awk -F '=' '{print $2}' > " + status_file_name)
        result = self.execute_adb_command(
            "shell dumpsys window|grep -i mShowing| awk -F' ' '{print $1}'|awk -F'=' '{print $2}'")

        if result.strip() == "false":
            return False

        if result.strip() == "true":
            return True

        raise Exception("Can't get the status of screen locked the screen")

    def get_battery_level(self):
        command = "shell dumpsys battery|grep level|awk -F ':' '{print $2}'"
        try:
            battery_level = int(str(self.execute_adb_command(command)).strip())
            return battery_level
        except:
            return int(-1)

    def get_adb_device_info(self):
        command = "devices -l|head -2|tail -1"
        try:
            return self.execute_adb_command(command)
        except Exception:
            return None

    def set_stay_always_on(self, always_on):
        command = "shell settings put global stay_on_while_plugged_in "
        if always_on:
            command += "1"
        else:
            command += "0"
        self.execute_adb_command(command)
        time.sleep(0.2)

    def minimize_screen_timeout(self):
        command = "shell settings put system screen_off_timeout 500"
        self.execute_adb_command(command)
        time.sleep(0.2)

    def restore_screen_timeout(self):
        command = "shell settings put system screen_off_timeout 120000"
        self.execute_adb_command(command)
        time.sleep(0.2)

    def get_resolution(self):
        return [1920, 1080]

    # Device specific stuff
    def capture_screenshot(self, filename):
        pass

    def capture_screenshot_and_resize(self, filename):
        self.capture_screenshot(filename)
        logger.log_debug("Converting image from png to jpg", self.devicename)
        os.system("convert " + filename +
                  " -resize 90% -quality 16 " + filename + ".jpg")
        return filename + ".jpg"

    def unlock(self):
        pass

    def lock(self):
        pass

    def press_game_icon(self):
        pass

    def close_all_opened_apps(self):
        pass

    def press_menu_button(self):
        pass


class SimulatedDevice(Device):
    def capture_screenshot(self, filename):
        logger.log_info("Simulating capture_screenshot", self.devicename)
        time.sleep(0.5)
        return filename

    def capture_screenshot_and_resize(self, filename):
        return self.capture_screenshot(filename)

    def unlock(self):
        logger.log_info("Simulating unlock", self.devicename)
        time.sleep(0.5)

    def press_game_icon(self):
        logger.log_info("Simulating press_game_icon", self.devicename)
        time.sleep(0.5)

    def close_all_opened_apps(self):
        logger.log_info("Simulating close_all_opened_apps", self.devicename)
        time.sleep(0.5)

    def press_menu_button(self):
        logger.log_info("Simulating press_menu_button", self.devicename)
        time.sleep(0.5)

    def get_resolution(self):
        return [1920, 1080]


class DeviceOnePlus3T(Device):
    def capture_screenshot(self, filename):
        self.execute_adb_command("shell screencap -p > " + filename)
        return filename

    def unlock(self):
        if self.get_screen_status() == "Asleep":
            self.press_power_button()
            time.sleep(2)
        self.execute_adb_command("shell input swipe 500 1900 500 1 500")
        time.sleep(2)


    def press_game_icon(self):
        logger.log_debug("Pressing game icon (130 750)")
        self.execute_adb_command("shell input tap 130 750")
        time.sleep(2)

    def close_all_opened_apps(self):
        # logger.log_debug("Close all opened apps", self.devicename)
        # self.execute_adb_command("shell input tap 550 1800")
        # time.sleep(2)
        # logger.log_verbose("Closing all opened apps", "Player")
        # for i in range(1, 4):
        #    self.execute_adb_command("shell input swipe 600 1000 1200 1000 200")
        #    time.sleep(2)
        self.execute_adb_command("shell am force-stop com.scopely.headshot")

    def press_menu_button(self):
        logger.log_debug("Pressing menu button")
        self.execute_adb_command("shell input keyevent 1")
        time.sleep(2)

    def get_resolution(self):
        return [1920, 1080]


class DeviceNexus5(Device):

    def capture_screenshot(self, filename):
        command = "shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > " + filename
        logger.log_pedantic("Capturing screenshot in " + filename)
        os.system("adb " + command)
        logger.log_pedantic("Screenshot captured! ")
        return filename

    def unlock(self):
        self.set_stay_always_on(True)
        time.sleep(1)
        self.restore_screen_timeout()
        time.sleep(1)
        if self.get_screen_status() == "Asleep":
            time.sleep(1)
            self.press_power_button()
            time.sleep(2)
        logger.log_info("Unlocking device")
        self.execute_adb_command("shell input swipe 500 1800 500 50 100")

    def lock(self):
        logger.log_info("Locking device")
        self.set_stay_always_on(False)
        time.sleep(1)
        self.minimize_screen_timeout()
        if self.get_screen_status() == "Awake":
            logger.log_info("Locking device")
            time.sleep(1)
            self.press_power_button()
            time.sleep(2)


    def press_game_icon(self):
        # self.execute_adb_command("shell input tap 160 200")
        self.execute_adb_command("shell input tap 150 550")
        time.sleep(2)

    def press_menu_button(self):
        self.press_home_button()
        self.execute_adb_command("shell input tap 855 1880")
        time.sleep(2)

    def close_all_opened_apps(self):
        logger.log_verbose("Closing all opened apps")
        for _ in range(1, 4):
            self.execute_adb_command(
                "shell input swipe 600 1000 1200 1000 200")
            time.sleep(2)
        time.sleep(2)

    def get_resolution(self):
        return [1920, 1080]


DEVICES = {ANDROID_DEVICE_ONE_PLUS_3T: None,
           ANDROID_DEVICE_NEXUS_5: None,
           SIMULATED_DEVICE: None}
