import os
import logger

GAME_CONFIG_FILE="game_config.cfg"
USER_CONFIG_FILE="user_config.cfg"
DEVICE_CONFIG_FILE="device_config.cfg"


def load_config(file_name):

    cfg_file = open(file_name, 'r', 1)
    config = { "FILE" : file_name }
    
    for line in cfg_file:
        #logger.log_verbose(line)
        tokens = line.split("=")
        config[tokens[0]] = tokens[1].strip()

    for key in config:
        logger.log_verbose(key + "\t=\t" + config[key])

    cfg_file.close()

    return config

if __name__ == "__main__":

    logger.log_debug("module cfg_handler loaded")
    load_config(DEVICE_CONFIG_FILE)
