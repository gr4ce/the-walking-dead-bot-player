"""
This module operates images comparison and conversions
"""

import os
import time
import random
import datetime

import numpy
import pytesseract

import cv2

try:
    from PIL import Image
except ImportError:
    import Image

import lib.logger as logger
import lib.device_handler as device


tag = "ImgDetector"


def are_similar(rect1, rect2):

    diff = 10
    val1 = rect1.x
    val2 = rect2.x
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.y
    val2 = rect2.y
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.width
    val2 = rect2.width
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    val1 = rect1.height
    val2 = rect2.height
    if not ((val1 <= (val2 + diff)) and (val1 >= (val2 - diff))):
        return False

    return True


class Rectangle():
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def randomise(self):
        x = random.randrange(self.x,  self.x + self.width)
        y = random.randrange(self.y,  self.y + self.height)
        return [x, y]

    def __str__(self):
        return "[" + str(self.x) + "," + str(self.y) + "," + str(self.width) + "," + str(self.height) + "]"

    def to_list(self):
        return [self.x, self.y, self.x+self.width, self.y+self.height]

    def to_string(self):
        return str(self.x) + "," + str(self.y) + "," + str(self.width) + "," + str(self.height)


def detect_multiple_matches(source_file_name, sample_file_name, threshold=0.99):

    logger.log_pedantic("Detecting multiple matches", tag)
    failed_attempts = 0
    max_failed_attempts = 3
    while True:
        try:
            logger.log_pedantic(
                "Attempt " + str(failed_attempts + 1), tag)
            img_rgb = cv2.imread(source_file_name)
            img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
            template = cv2.imread(sample_file_name, 0)
            if template is None:
                logger.log_error("Opening image at: " + sample_file_name, tag)
                raise Exception(
                    "detect_multiple_matches failed opening image at: " + sample_file_name)

            width, height = template.shape[::-1]
            res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
            loc = numpy.where(res >= threshold)
            rectangle_list = []

            for pt in list(zip(*loc[::-1])):

                logger.log_pedantic("x,y,w,h = %d,%d,%d,%d" % (
                    pt[0], pt[1], template.shape[1], template.shape[0]), tag)
                new_rect = Rectangle(pt[0], pt[1], template.shape[1],
                                     template.shape[0])
                found_similar = False
                for r in rectangle_list:
                    logger.log_pedantic(
                        "Comparing " + new_rect.to_string() + " and " + r.to_string(), tag)
                    if are_similar(r, new_rect):
                        logger.log_pedantic(
                            " == > " + new_rect.to_string() + " and " + r.to_string() + " are similar, not adding", tag)
                        found_similar = True
                        break

                if not found_similar:
                    logger.log_pedantic(" Adding " + new_rect.to_string(), tag)
                    rectangle_list.append(new_rect)

                    # We are only going to save an image with the result boxes if
                    # the debug level is on
                    if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
                        logger.log_pedantic(
                            "Found a match in " + str(new_rect), tag)
                        cv2.rectangle(
                            img_rgb, pt, (pt[0] + width, pt[1] + height), (0, 0, 255), 2)

            if len(rectangle_list) > 0:
                if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
                    cv2.imwrite(source_file_name[:len(
                        source_file_name) - 4] + '-res.png', img_rgb)

            return rectangle_list

        except Exception as e:
            logger.log_pedantic("Exception thrown: " + str(e), tag)
            device.current_device.capture_screenshot(source_file_name)


def is_there_at_least_one_match(source_file_name, sample_file_name, threshold=0.99):
    rlist = detect_multiple_matches(
        source_file_name, sample_file_name, threshold)
    if len(rlist) > 0:
        return True
    else:
        return False


def scan_enemy_types_in_rts(image_file_name, threshold=0.99):

    device_folder = device.current_device.devicename
    enemies_list = []

    rts_images_path = os.path.join("data", "images", device_folder, "rts")

    rts_walkers_image_file = os.path.join(
        rts_images_path, "enemies_walkers.png")
    if not os.path.exists(rts_walkers_image_file):
        logger.log_warn(
            "File " + rts_walkers_image_file + " not found", tag)
        return enemies_list
    rts_humans_image_file = os.path.join(rts_images_path, "enemies_human.png")
    if not os.path.exists(rts_humans_image_file):
        logger.log_warn("File " + rts_humans_image_file +
                        " not found", tag)
        return enemies_list

    rts_fast_image_file = os.path.join(rts_images_path, "trait_fast.png")
    if not os.path.exists(rts_fast_image_file):
        logger.log_warn("File " + rts_fast_image_file +
                        " not found", tag)
        return enemies_list
    rts_strong_image_file = os.path.join(rts_images_path, "trait_strong.png")
    if not os.path.exists(rts_strong_image_file):
        logger.log_warn("File " + rts_strong_image_file +
                        " not found", tag)
        return enemies_list
    rts_alert_image_file = os.path.join(rts_images_path, "trait_alert.png")
    if not os.path.exists(rts_alert_image_file):
        logger.log_warn("File " + rts_alert_image_file +
                        " not found", tag)
        return enemies_list
    rts_tough_image_file = os.path.join(rts_images_path, "trait_tough.png")
    if not os.path.exists(rts_tough_image_file):
        logger.log_warn("File " + rts_tough_image_file +
                        " not found", tag)
        return enemies_list

    enemies = ["WALKERS", "HUMANS", "ALERT", "FAST", "TOUGH", "STRONG"]

    enemies_files = {"WALKERS": cv2.imread(rts_walkers_image_file, 0),
                     "HUMANS": cv2.imread(rts_humans_image_file, 0),
                     "ALERT": cv2.imread(rts_alert_image_file, 0),
                     "FAST": cv2.imread(rts_fast_image_file, 0),
                     "TOUGH": cv2.imread(rts_tough_image_file, 0),
                     "STRONG": cv2.imread(rts_strong_image_file, 0)}

    source_image_rgb = cv2.imread(image_file_name)
    source_image_gray = cv2.cvtColor(source_image_rgb, cv2.COLOR_BGR2GRAY)

    rectangle_list = []

    for enemy in enemies:
        template = enemies_files[enemy]
        width, height = template.shape[::-1]
        res = cv2.matchTemplate(
            source_image_gray, template, cv2.TM_CCOEFF_NORMED)
        loc = numpy.where(res >= threshold)

        for pt in list(zip(*loc[::-1])):
            logger.log_pedantic("x,y,w,h = %d,%d,%d,%d" % (
                pt[0], pt[1], template.shape[1], template.shape[0]), tag)
            r = Rectangle(pt[0], pt[1], template.shape[1], template.shape[0])
            rectangle_list.append(r)

            # We are only going to save an image with the result boxes if the debug level is on
            if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
                logger.log_pedantic("Found a match in " + str(r), tag)
                cv2.rectangle(source_image_rgb, pt,
                              (pt[0] + width, pt[1] + height), (0, 0, 255), 2)

        if len(list(zip(*loc[::-1]))) > 0:
            enemies_list.append(enemy)

    if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
        cv2.imwrite(image_file_name[:len(
            image_file_name) - 4] + '-res.png', source_image_rgb)

    return enemies_list


def are_we_home():
    device_folder = device.current_device.devicename

    target_img = os.path.join(
        "data", "images", device_folder, "home_screenshot_target.png")
    capture_img = os.path.join(
        "data", "images", device_folder, "home_screenshot_capture.png")
    device.current_device.capture_screenshot(capture_img)
    rectangle_result = detect_multiple_matches(capture_img, target_img, 0.94)
    if rectangle_result is None:
        return False
    if len(rectangle_result) > 0:
        return True
    return False


def attack_button_present():
    device_folder = device.current_device.devicename

    target_img = os.path.join(
        "data", "images", device_folder, "rtst/attack_button.png")
    capture_img = os.path.join(
        "data", "images", device_folder, "rtst/attack_button_capture.png")
    device.current_device.capture_screenshot(capture_img)
    r = detect_multiple_matches(capture_img, target_img, 0.98)
    if len(r) > 0:
        return True
    return False


def rtsd_locked():
    device_folder = device.current_device.devicename

    target_img = os.path.join(
        "data", "images", device_folder, "rtsd/road_locked.png")
    capture_img = os.path.join(
        "data", "images", device_folder, "rtst/road_locked_capture.png")
    device.current_device.capture_screenshot(capture_img)
    r = detect_multiple_matches(capture_img, target_img, 0.98)
    if len(r) > 0:
        return True
    return False


def get_raid_result(screenshot_file):
    device_folder = device.current_device.devicename

    defeated_img = os.path.join(
        "data", "images", device_folder, "raid/defeated.png")
    winner_img = os.path.join(
        "data", "images", device_folder, "raid/winner.png")

    d = detect_multiple_matches(screenshot_file, defeated_img, 0.98)
    w = detect_multiple_matches(screenshot_file, winner_img, 0.98)

    defeated = False
    winner = False

    if len(d) > 0:
        defeated = True

    if len(w) > 0:
        winner = True

    if defeated and winner:
        logger.log_error(
            "From raid got inconsistent result, we won and lost!!", tag)
        return "ERROR"
    elif not (defeated or winner):
        logger.log_error(
            "From raid got inconsistent result, we neither won or lost!!", tag)
        return "ERROR"
    elif defeated and not winner:
        return "DEFEATED"
    elif winner and not defeated:
        return "WINNER"
    else:
        logger.log_error(
            "From raid got inconsistent result, unexpected situation!!", tag)
        return "ERROR"


def get_raid_energy_value():

    device_folder = device.current_device.devicename
    capture_img = os.path.join(
        "data", "images", device_folder, "raid/capture_for_energy.png")
    device.current_device.capture_screenshot(capture_img)

    value_found = -1
    for energy in [6, 5, 4, 3, 2, 1, 0]:
        energy_str = str(energy)
        target_img = os.path.join(
            "data", "images", device_folder, "raid/" + energy_str + "_energy.png")
        if not os.path.exists(target_img):
            logger.log_warn("File " + target_img + " doesn't exist", tag)
        else:
            r = detect_multiple_matches(capture_img, target_img, 0.98)
            if len(r) > 0:
                if value_found != -1:
                    logger.log_error(
                        "Multiple matches with energy value found... that's a problem! Value=" + value_found, tag)
                    return -1
                else:
                    value_found = energy
                    logger.log_info(
                        "Match found with energy " + energy_str, tag)

    return value_found


def get_rtst_energy_value():

    device_folder = device.current_device.devicename
    capture_img = os.path.join(
        "data", "images", device_folder, "rtst/capture_for_energy.png")
    device.current_device.capture_screenshot(capture_img)

    match_found = -1
    for energy in [8, 7, 6, 5, 4, 3, 2, 1, 0]:
        energy_str = str(energy)
        target_img = os.path.join(
            "data", "images", device_folder, "rtst/" + energy_str + "_energy.png")
        if not os.path.exists(target_img):
            logger.log_warn("File " + target_img + " doesn't exist", tag)
        else:
            r = detect_multiple_matches(capture_img, target_img, 0.99)
            if r is None:
                raise Exception("Scanning raid energy value failed")
            if len(r) > 0:
                if match_found != -1:
                    logger.log_error(
                        "Multiple matches with energy value found... that's a problem! Value=" + match_found, tag)
                    return -1
                else:
                    match_found = energy
                    logger.log_info(
                        "Match found with energy " + energy_str, tag)

    return match_found


def are_all_fighters_dead(image_file_name):
    device_folder = device.current_device.devicename
    logger.log_pedantic("Opening file in " + device_folder, "ImgDetector")
    all_fighters_dead = os.path.join(
        "data", "images", device_folder, "rtsd", "all_dead.png")
    r = detect_multiple_matches(image_file_name, all_fighters_dead, 0.98)
    if len(r) > 0:
        return True


def scan_find_opponent_button(image_file_name):
    device_folder = device.current_device.devicename
    target_img = os.path.join(
        "data", "images", device_folder, "raid/find_opponent_button.png")
    r = detect_multiple_matches(image_file_name, target_img, 0.90)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return r[0]
    return None


def scan_find_bootcamp_button(image_file_name):
    device_folder = device.current_device.devicename
    target_img = os.path.join(
        "data", "images", device_folder, "roadmap/bootcamp.png")
    assert os.path.exists(target_img)
    logger.log_pedantic("Bootcamp file: " + target_img, tag)
    r = detect_multiple_matches(image_file_name, target_img, 0.70)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return r[0]

    target_img = os.path.join(
        "data", "images", device_folder, "roadmap/bootcamp_big.png")
    assert os.path.exists(target_img)
    logger.log_pedantic("Bootcamp file: " + target_img, tag)
    r = detect_multiple_matches(image_file_name, target_img, 0.70)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return r[0]
    return None


def is_refill_needed():
    device_folder = device.current_device.devicename
    target_img = os.path.join(
        "data", "images", device_folder, "world/world_refill.png")
    capture_img = os.path.join(
        "data", "images", device_folder, "roadmap/before_match.png")
    device.current_device.capture_screenshot(capture_img)
    assert os.path.exists(target_img)
    r = detect_multiple_matches(capture_img, target_img, 0.97)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return r[0]
    return None


def scan_next_button(image_file_name, threshold=0.99):

    device_folder = device.current_device.devicename
    if not os.path.exists(image_file_name):
        # Can we fix this by taking a screenshot again?
        raise Exception("Image path " + image_file_name + " doesn't exist")
    source_image_rgb = cv2.imread(image_file_name)
    source_image_gray = cv2.cvtColor(source_image_rgb, cv2.COLOR_BGR2GRAY)

    rectangle_list = []
    next_button = os.path.join(
        "data", "images", device_folder, "world/next_button.png")
    logger.log_pedantic("Opening file " + next_button, tag)
    template = cv2.imread(next_button,  0)
    w, h = template.shape[::-1]
    res = cv2.matchTemplate(source_image_gray, template, cv2.TM_CCOEFF_NORMED)
    loc = numpy.where(res >= threshold)

    for pt in list(zip(*loc[::-1])):
        logger.log_pedantic("x,y,w,h = %d,%d,%d,%d" % (
            pt[0], pt[1], template.shape[1], template.shape[0]), tag)
        r = Rectangle(pt[0], pt[1], template.shape[1], template.shape[0])
        rectangle_list.append(r)

        # We are only going to save an image with the result boxes if the debug level is on
        if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
            logger.log_debug("Found a match in " + str(r), "ImgRecon")
            cv2.rectangle(source_image_rgb, pt,
                          (pt[0] + w, pt[1] + h), (0, 0, 255), 2)

    if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
        cv2.imwrite(image_file_name[:len(
            image_file_name) - 4] + '-res.png', source_image_rgb)

    return rectangle_list


def rtsd_completed():
    device_folder = device.current_device.devicename

    target_img = os.path.join(
        "data", "images", device_folder, "rtsd/road_completed_ok_button.png")
    capture_img = os.path.join(
        "data", "images", device_folder, "rtsd/road_completed_capture.png")
    device.current_device.capture_screenshot(capture_img)
    r = detect_multiple_matches(capture_img, target_img, 0.98)
    if len(r) > 0:
        return True
    return False


def scan_attack_button_rtsd(screenshot_file):
    device_folder = device.current_device.devicename
    target_img = os.path.join(
        "data", "images", device_folder, "rtsd/attack_button.png")

    r = detect_multiple_matches(screenshot_file, target_img, 0.97)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return True
    return False


def scan_attack_button_rtst(screenshot_file):
    device_folder = device.current_device.devicename
    target_img = os.path.join(
        "data", "images", device_folder, "rtst/attack_button.png")

    r = detect_multiple_matches(screenshot_file, target_img, 0.97)
    if len(r) > 0:
        logger.log_pedantic("Found " + str(len(r)) + " matches", tag)
        return True
    return False


def scan_next_button_rts(image_file_name, threshold=0.99):

    device_folder = device.current_device.devicename
    source_image_rgb = cv2.imread(image_file_name)
    source_image_gray = cv2.cvtColor(source_image_rgb, cv2.COLOR_BGR2GRAY)

    rectangle_list = []

    try:
        next_button = os.path.join(
            "data", "images", device_folder, "rts", "next.png")
        logger.log_verbose("Opening file " + next_button, tag)
        template = cv2.imread(next_button,  0)
        w, h = template.shape[::-1]
        res = cv2.matchTemplate(
            source_image_gray, template, cv2.TM_CCOEFF_NORMED)
        loc = numpy.where(res >= threshold)

        for pt in list(zip(*loc[::-1])):
            logger.log_pedantic("x,y,w,h = %d,%d,%d,%d" % (
                pt[0], pt[1], template.shape[1], template.shape[0]), tag)
            r = Rectangle(pt[0], pt[1], template.shape[1], template.shape[0])
            rectangle_list.append(r)

            # We are only going to save an image with the result boxes if the debug level is on
            if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
                logger.log_pedantic("Found a match in " + str(r), tag)
                cv2.rectangle(source_image_rgb, pt,
                              (pt[0] + w, pt[1] + h), (0, 0, 255), 2)

        if logger.get_log_level() >= logger.LOG_LEVEL_DEBUG:
            cv2.imwrite(image_file_name[:len(
                image_file_name) - 4] + '-res.png', source_image_rgb)
    except:
        logger.log_error(
            "Exception caught scanning for next button at the end of match", tag)
        return None

    if len(rectangle_list) > 0:
        return rectangle_list[0]

    return None


def detect_playable_roadmap_matches(image_file_name, threshold=0.90):

    device_folder = device.current_device.devicename
    roadmap_path = os.path.join("data", "images", device_folder, "roadmap")
    playable_maps = None

    # Detecting boot camp
    map_boot_camp = os.path.join(roadmap_path, "map_boot_camp.png")
    rect = detect_multiple_matches(image_file_name, map_boot_camp, threshold)
    if len(rect) > 0:
        if playable_maps is None:
            playable_maps = {}
        playable_maps["map_boot_camp"] = rect

    # Detecting ultimate gear
    map_ultimate_gear = os.path.join(roadmap_path, "map_ultimate_gear.png")
    rect = detect_multiple_matches(
        image_file_name, map_ultimate_gear, threshold)
    if len(rect) > 0:
        if playable_maps is None:
            playable_maps = {}
        playable_maps["map_ultimate_gear"] = rect

    # Detecting road_to_terror
    map_road_to_terror = os.path.join(roadmap_path, "map_road_to_terror.png")
    rect = detect_multiple_matches(
        image_file_name, map_road_to_terror, threshold)
    if len(rect) > 0:
        if playable_maps is None:
            playable_maps = {}
        playable_maps["map_road_to_terror"] = rect

    return playable_maps


def detect_playable_roadmap_areas(image_file_name, threshold=0.95):

    device_folder = device.current_device.devicename
    roadmap_path = os.path.join("data", "images", device_folder, "roadmap")
    playable_maps = None

    # Detecting areas
    area_border = os.path.join(roadmap_path, "roadmap_area_button_border.png")
    rect = detect_multiple_matches(image_file_name, area_border, threshold)
    if len(rect) > 0:
        if playable_maps is None:
            playable_maps = {}
        playable_maps["map_boot_camp"] = rect

    return playable_maps


def raid_produce_key_imgs(image_file_name, threshold=0.99):
    pass


def get_world_energy(image_file):

    if not os.path.exists(image_file):
        logger.log_error(
            "Input file to get the world energy not available " + image_file, tag)
        return -1

    now = datetime.datetime.now()
    time_stamp = now.strftime("%Y-%m-%d_%H-%M-%S")

    dest_cropped_file_name = "tmp/digits/" + \
        time_stamp + "-world-energy-cropped.png"
    dest_negative_file_name = "tmp/digits/" + \
        time_stamp + "world-energy-negative.png"
    dest_black_white_file_name = "tmp/digits/" + \
        time_stamp + "world-energy-blackandwhite.png"

    os.system("convert -crop 41x35+167+84 " +
              image_file + " " + dest_cropped_file_name)
    time.sleep(2)
    os.system(
        "convert " + dest_cropped_file_name + " -channel RGB -negate " + dest_negative_file_name)
    time.sleep(2)
    os.system(
        "convert " + dest_negative_file_name + " -threshold 42% " + dest_black_white_file_name)
    time.sleep(2)
    source_img = Image.open(dest_black_white_file_name)
    #text = pytesseract.image_to_string(source_img, config='--psm 13 --oem 3 ')
    text = pytesseract.image_to_string(
        source_img, config='--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789')
    number = text.replace(',', '').strip().replace('.', '').replace(' ', '')
    logger.log_verbose("Value scanned by tesseract: " + number, tag)

    try:
        number_final = int(number)
        return number_final
    except:
        logger.log_warn("Exception trying to parse the number", tag)
        return -1


def get_reputation(image_file):

    if not os.path.exists(image_file):
        logger.log_error(
            "Input file to get the reputation not available " + image_file, tag)
        return -1

    #os.system("convert -crop 150x50+1540+490 " + image_file + " tmp/raid-cropped.png")
    os.system("convert -crop 155x55+1490+490 " +
              image_file + " tmp/raid-cropped.png")
    time.sleep(2)
    os.system(
        "convert tmp/raid-cropped.png -channel RGB -negate tmp/raid-negated.png")
    time.sleep(2)
    os.system(
        "convert tmp/raid-negated.png -threshold 42% tmp/raid-blackandwhite.png")
    time.sleep(2)
    source_img = Image.open("tmp/raid-blackandwhite.png")
    text = pytesseract.image_to_string(source_img)
    number = text.replace(',', '').strip().replace('.', '').replace(' ', '')
    logger.log_verbose("Value scanned by tesseract: " + number, tag)

    try:
        number_final = int(number)
        return number_final
    except Exception as e:
        logger.log_warn("Exception trying to parse the number: " + str(e), tag)
        return -1


def identify_exception_case(screenshot_file=None):

    device_folder = device.current_device.devicename
    exception_found = "UNIDENTIFIED"

    exception_screens = {
        "WAR RESULTS": "data/images/" + device_folder +
                       "/exceptions/war_results_banner.png",
        "WAR SCORECARD": "data/images/" + device_folder +
                         "/exceptions/war_scorecard_banner.png",
        "NETWORK ERROR": "data/images/" + device_folder +
                         "/exceptions/network_error_banner.png",
        "PRESTIGE REWARDS": "data/images/" + device_folder +
                         "/exceptions/prestige_rewards_banner.png",
        "REGION WAR OVER": "data/images/" + device_folder +
                         "/exceptions/region_war_over_banner.png",
        "REGION WAR HAS ENDED": "data/images/" + device_folder +
                         "/exceptions/region_war_has_ended_banner.png",
        "SWITCH ACCOUNT": "data/images/" + device_folder +
                          "/exceptions/transfer_complete_switch_button.png"}

    if screenshot_file is None:
        screenshot_file = "tmp/exception_caught.png"
        device.current_device.capture_screenshot(screenshot_file)
        time.sleep(1)

    for exception_key in exception_screens:
        logger.log_pedantic("Trying " + exception_key + " with screenshot in " +
                            exception_screens[exception_key], tag)
        target_image_file = exception_screens[exception_key]
        if not os.path.exists(target_image_file):
            logger.log_error("Cannot find file while reconing exception: " +
                             target_image_file, tag)
            continue
        rectangle = detect_multiple_matches(screenshot_file, target_image_file, 0.98)
        if len(rectangle) > 0:
            if exception_found != "UNIDENTIFIED":
                logger.log_error(
                    "Problem identifying the cause of the issue.. too many reported", tag)
            else:
                exception_found = exception_key
                logger.log_info("Identified exception: " +
                                exception_key, tag)

    if exception_found == "UNIDENTIFIED":
        logger.log_warn(
            "Couldn't identify the source of problems in exception", tag)
    else:
        logger.log_pedantic("Confirming exception is " + exception_found, tag)

    return exception_found


def search_world_area(area):
    device_folder = device.current_device.devicename

    capture_img = os.path.join(
        "data", "images", device_folder, "world/search_area_screenshot_capture.png")

    device.current_device.capture_screenshot(capture_img)

    target_img = os.path.join(
        "data", "images", device_folder, "world/area_" + str(area) + "_building.png")
    r = detect_multiple_matches(capture_img, target_img, 0.98)

    if r is None:
        return None

    if len(r) > 0:
        return r

    return None


def from_left_to_right(elem):
    return elem.x


def scan_stages_and_energy(energy_list):

    device_folder = device.current_device.devicename
    capture_img = os.path.join(
        "data", "images", device_folder, "world/area_screenshot_capture.png")
    device.current_device.capture_screenshot(capture_img)

    rect_list = []
    for energy in energy_list:
        target_img = os.path.join(
            "data", "images", device_folder, "world/world_stage_energy_" + str(energy) + ".png")
        rect = detect_multiple_matches(capture_img, target_img, 0.95)

        if len(rect) > 0:
            logger.log_pedantic("                          Found " +
                                str(len(rect)) + " stages for energy " + str(energy), tag)
            rect_list.extend(rect)

    rect_list.sort(key=from_left_to_right)

    return rect_list


def scan_world_area():
    device_folder = device.current_device.devicename

    capture_img = os.path.join(
        "data", "images", device_folder, "world/area_screenshot_capture.png")

    device.current_device.capture_screenshot(capture_img)

    for i in [13, 22, 24, 25]:
        target_img = os.path.join(
            "data", "images", device_folder, "world/area_" + str(i) + "_stages.png")
        r = detect_multiple_matches(capture_img, target_img, 0.98)
        if r is None:
            return None
        if len(r) > 0:
            return i
    return None


def find_play_replay_button():

    device_folder = device.current_device.devicename

    file_name = os.path.join("data/images", device_folder,
                             "roadmap/roadmap_stages.png")
    logger.log_pedantic(file_name, "ButtonFinder")
    device.current_device.capture_screenshot(file_name)
    res = detect_multiple_matches(file_name, os.path.join(
        "data/images", device_folder, "roadmap/play_button.png"), threshold=0.90)
    if res != None and len(res) > 0:
        logger.log_debug("Found play button, pressing it", "ButtonFinder")
        time.sleep(2)
        return ["play", res[0]]

    res = detect_multiple_matches(file_name, os.path.join(
        "data/images", device_folder, "roadmap/replay_button.png"), threshold=0.90)
    if res != None and len(res) > 0:
        logger.log_debug("Found replay button, pressing it", "ButtonFinder")
        time.sleep(2)
        return ["replay", res[0]]

    logger.log_error("Couldn't find play or replay button", "ButtonFinder")
    return None
