import subprocess

def execute(command):
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = p.communicate()[0]
    return result.decode()
